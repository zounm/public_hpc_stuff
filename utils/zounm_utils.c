/*Author: Judicael Zounmevo (zounm@linux.com)
 **/

#ifdef WIN32
#include <process.h>
#include<Winsock2.h>

#pragma comment(lib, "Ws2_32.lib")
#pragma warning(disable:4996)

#else
#define _GNU_SOURCE
#include <sched.h>
#include <pthread.h>
#endif  //WIN32

#include <assert.h>
#include <ctype.h>
#include "zounm_utils.h"


#ifdef Z_REGION_ERROR_REPORTING
#define MAX_ERR_STRING_LEN 251
char err_string[MAX_ERR_STRING_LEN];
#endif //Z_REGION_ERROR_REPORTING

#ifdef Z_REGION_TIMING
#ifdef TIMING_MODE_GETTIMEOFDAY
struct timeval __timeval_temp1={0,0};
struct timeval __timeval_temp2={0,0};
#endif //TIMING_MODE_GETTIMEOFDAY
struct timespec __timespec_temp1={0,0};
struct timespec __timespec_temp2={0,0};
double __wtime_temp1=0.0;
double __wtime_temp2=0.0;
uint64_t  __tsc_temp1;
uint64_t  __tsc_temp2;
double JZTSC_resolution;

#ifndef WIN32
cpu_set_t mask;
#endif


#ifdef Z_WIN_PERFORMANCE_COUNTER
LARGE_INTEGER __perf_frequency;
LARGE_INTEGER __perf_time1;
LARGE_INTEGER __perf_time2;

void init_timing()
{

	 memset(&__perf_frequency, 0, sizeof(__perf_frequency));
	 memset(&__perf_time1, 0, sizeof(__perf_time1));
	 memset(&__perf_time2, 0, sizeof(__perf_time2));
	 QueryPerformanceFrequency(&__perf_frequency);
}
#else

void init_timing()
{

	/*It is the responsibility of the user to:
	 * 1-) pin the process in case of CLOCK_MONOTONIC
	 * .... wait, I removed TSC!
	 * */
	return; 
}
#endif


#endif //Z_REGION_TIMING

#ifdef Z_REGION_DEBUGGING
void Z_wait_for_debugger(const char* hostname, const char* file, int line, BOOL once)
{
	char _hostname[251];
	static BOOL firstTime=TRUE;
	if(!once ||  firstTime)					
	{
		firstTime=FALSE;
		gethostname(_hostname, 250);
		if(hostname && strcmp(_hostname, hostname)!=0)
			return;
		printf("The program (pid = %d) is suspended on %s at %s:%d for debugging. Please attach a debugger to continue\n",
			getpid(), _hostname, file, line);
		fflush(stdout);
		for(;;);		
	}
}

void Z_timed_wait_for_debugger(double time, const char* hostname, const char* file, int line, BOOL once)
{
	char _hostname[251];
	static BOOL firstTime=TRUE;
	if(!once ||  firstTime)					
	{
		firstTime=FALSE;
		gethostname(_hostname, 250);
		if(hostname && strcmp(_hostname, hostname)!=0)
			return;
		printf("The program (pid = %d) is suspended on %s at %s:%d for debugging. Please attach a debugger to continue\n",
			getpid(), _hostname, file, line);
		fflush(stdout);
		Z_DECLARE_TIME_VAR(start);
		Z_DECLARE_TIME_VAR(end);
		Z_STAMP_TIME_VAR(start);
		Z_STAMP_TIME_VAR(end);
		double wait_time = time*MILLION;
		while(Z_GET_DURATION_US_VAR(start,end)<wait_time)
			Z_STAMP_TIME_VAR(end);
	}
}
#endif //Z_REGION_DEBUGGING

#ifdef Z_REGION_DATA_STRUCTURES
void __add_node(void** node_head, void** node_tail, void* node)
{
    Z_node_link_t **head = (Z_node_link_t**)(node_head);
    Z_node_link_t **tail = (Z_node_link_t**)(node_tail);
    if(*tail)
    {
        (*tail)->next = (Z_node_link_t*)node;
    }
    else
    {
        *head = (Z_node_link_t*)node;
    }
    *tail = (Z_node_link_t*)node;
}

void __remove_node(void** node_head, void** node_tail, void* node)
{
    Z_node_link_t *cur = (Z_node_link_t*)(*node_head), *previous = NULL;
    while(cur)
    {
        if(cur == node)
        {
            if(previous)
                previous->next = cur->next;
            else
                *node_head = cur->next;
            if(*node_tail == cur)
                *node_tail = previous;
            break;
        }
        previous = cur;
        cur = cur->next;
    }
}

void* Z_find_node(void* node_head, node_predicate_t predicate, void* predicate_arg)
{
    Z_node_link_t *cur = (Z_node_link_t*)(node_head);
    while(cur)
    {
        if(predicate((void*)cur, predicate_arg))
            return cur;
        cur = cur->next;
    }
    return NULL;
}


void* Z_pop_front(void** node_head, void** node_tail)
{
	void* node = *node_head;
	Z_remove_node(node_head, node_tail, node);
	return node;
}

void* Z_pop_back(void** node_head, void** node_tail)
{
	void* node = *node_tail;
	Z_remove_node(node_head, node_tail, node);
	return node;
}

#endif //Z_REGION_DATA_STRUCTURES


#ifdef Z_REGION_INPUT_MANAGEMENT
static int get_next_token(char* source, char* token, int offset, char key)
{
	size_t length=strlen(source);
	size_t i;
	for(i=offset; i<length; i++)
		if(source[i]==key)
			break;
	strncpy(token, source+offset, i-offset);
	token[i-offset]=0;
	return i==length?-1:(int)(i+1);
}

typedef enum complex_number_distribution_mode_t
{
	COMPLEX_NUM_DISTRO_MODE_UNDEFINED,
	INCREMENT,
	COMPREHENSIVE,
	FACTOR
}complex_number_distribution_mode_t;

static int increment=0;
static int factor=0;
static int max=0;
static int input_data=0;
static char* token=NULL;
static complex_number_distribution_mode_t number_distro_mode = COMPLEX_NUM_DISTRO_MODE_UNDEFINED;
static char token0[100];
static char token1[100];
static int offset0 = 0;
static int offset1 = 0;
char* raw_complex_input = NULL;


void Z_init_complex_input_extraction(char* raw_input)
{
	raw_complex_input = raw_input;
	increment=0;
	factor=0;
	max=0;
	input_data=0;
	token=NULL;
	number_distro_mode = COMPLEX_NUM_DISTRO_MODE_UNDEFINED;
	token0[0] = 0;
	token1[0] = 0;
	offset0 = 0;
	offset1 = 0;
}

BOOL Z_extract_next_from_complex_input(int *extracted_input)
{
	if(number_distro_mode==COMPLEX_NUM_DISTRO_MODE_UNDEFINED)
	{
		offset0=get_next_token(raw_complex_input, token0, offset0, '+');		
		if(token0[0]=='i')
		{			
			offset1=2;
			offset1 = get_next_token(token0, token1, offset1, ',');
			if(input_data==0)
			{
				input_data=atoi(token1);  //initial
				offset1 = get_next_token(token0, token1, offset1, ',');
				increment=atoi(token1);
			}			
			else
			{
				increment=atoi(token1);
				input_data+=increment;
			}

			offset1 = get_next_token(token0, token1, offset1, '+');
			max=atoi(token1);
			number_distro_mode=INCREMENT;
			*extracted_input = input_data;
			return TRUE;
		}
		else if(token0[0]=='f')
		{
			offset1=2;
			offset1 = get_next_token(token0, token1, offset1, ',');
			if(input_data==0)
			{
				input_data=atoi(token1);  //initial
				offset1 = get_next_token(token0, token1, offset1, ',');
				factor=atoi(token1);
			}			
			else
			{
				factor=atoi(token1);
				input_data*=factor;
			}

			offset1 = get_next_token(token0, token1, offset1, '+');
			max=atoi(token1);
			number_distro_mode=FACTOR;
			*extracted_input = input_data;
			return TRUE;
		}
		else
		{
			offset1=0;
			number_distro_mode=COMPREHENSIVE;
			offset1 = get_next_token(token0, token1, offset1, ',');
			input_data= atoi(token1);
			*extracted_input = input_data;
			return TRUE;
		}		
	}
	else
	{
		if(number_distro_mode==INCREMENT)
		{
			input_data+=increment;
			if(input_data>max)
			{
				if(offset0==-1)
					return FALSE;
				number_distro_mode=COMPLEX_NUM_DISTRO_MODE_UNDEFINED;
				input_data-=increment;
				return Z_extract_next_from_complex_input(extracted_input);
			}
			*extracted_input = input_data;
			return TRUE;
		}
		else if(number_distro_mode==FACTOR)
		{
			input_data*=factor;
			if(input_data>max)
			{
				if(offset0==-1)
					return FALSE;
				number_distro_mode=COMPLEX_NUM_DISTRO_MODE_UNDEFINED;
				input_data/=factor;
				return Z_extract_next_from_complex_input(extracted_input);
			}
			*extracted_input = input_data;
			return TRUE;
		}
		else
		{
			int size_backup=input_data;
			offset1 = get_next_token(token0, token1, offset1, ',');
			input_data = atoi(token1);
			if(input_data == 0 )//BUG HERE. Got to check sometime
			{
				if(offset0==-1)
					return FALSE;
				number_distro_mode=COMPLEX_NUM_DISTRO_MODE_UNDEFINED;
				input_data=size_backup;
				return Z_extract_next_from_complex_input(extracted_input);
			}

			*extracted_input = input_data;
			return TRUE;
		}
	}	
}

/*The disto argument can be NULL, in which case it is ignored
*/
void Z_get_complex_metric_distribution(char* complex_input, int **distro, 
		int *max_metric_value, int *nb_metric_values)
{
	*nb_metric_values = 0; 
	int max = 0, i, temp;
	Z_init_complex_input_extraction(complex_input);
	while(Z_extract_next_from_complex_input(&temp))
		++(*nb_metric_values);
	if(distro)
		*distro = (int*)malloc(sizeof(int)*(*nb_metric_values));
	Z_init_complex_input_extraction(complex_input);
	*max_metric_value = 0;
	for(i=0; i<*nb_metric_values; i++)
	{
		(void)Z_extract_next_from_complex_input(&temp);
		*max_metric_value = Z_MAX(max,temp);
		if(distro)
			(*distro)[i] = temp;
	}
}

BOOL Z_is_option_set(int argc, char **argv, char option)
{
	int i;
	for(i=1; i<argc; i++)
	{
		if(strlen(argv[i]) >= 2 && argv[i][0] == '-' && argv[i][1] == option)
			return TRUE;
	}
	return FALSE;
}

BOOL Z_get_int_option_value(int argc, char **argv, char option, int *val)
{
	int i;
	for(i=1; i<argc; i++)
	{
		if(strlen(argv[i]) >= 2 && argv[i][0] == '-' && argv[i][1] == option && (strlen(argv[i]) > 2 || i + 1 < argc))
		{
			*val = atoi((strlen(argv[i]) > 2 ? (argv[i]+2) : argv[i+1]));
			return TRUE;
		}
	}
	return FALSE;
}
BOOL Z_get_char_option_value(int argc, char **argv, char option, char *val)
{
	int i;
	for(i=1; i<argc; i++)
	{
		if(strlen(argv[i]) >= 2 && argv[i][0] == '-' && argv[i][1] == option && (strlen(argv[i]) > 2 || i + 1 < argc))
		{
			*val = (strlen(argv[i]) > 2 ? (argv[i]+2) : argv[i+1])[0];
			return TRUE;
		}
	}
	return FALSE;
}
BOOL Z_get_double_option_value(int argc, char **argv, char option, double *val)
{
	int i;
	for(i=1; i<argc; i++)
	{
		if(strlen(argv[i]) >= 2 && argv[i][0] == '-' && argv[i][1] == option && (strlen(argv[i]) > 2 || i + 1 < argc))
		{
			*val = atof((strlen(argv[i]) > 2 ? (argv[i]+2) : argv[i+1]));
			return TRUE;
		}
	}
	return FALSE;
}

BOOL Z_get_string_option_value(int argc, char **argv, char option, char val[], 
		int max_val_length, int* min_required_length)
{
	int i;
	for(i=1; i<argc; i++)
	{
		if(strlen(argv[i]) >= 2 && argv[i][0] == '-' && argv[i][1] == option && (strlen(argv[i]) > 2 || i + 1 < argc))
		{
			*min_required_length = (int)strlen((strlen(argv[i]) > 2 ? (argv[i]+2) : argv[i+1]));
			if(*min_required_length + 1 > max_val_length)
				return FALSE;
			strcpy(val, (strlen(argv[i]) > 2 ? (argv[i]+2) : argv[i+1]));
			return TRUE;
		}
	}
	return FALSE;
}
#endif //Z_REGION_INPUT_MANAGEMENT

#ifdef Z_REGION_TRACING

FILE* Z_trace_file = NULL;
char Z_trace_filename[201];


void __Z_open_trace_file(char* file_name)
{
    char hostname[50];
	if(file_name)
		sprintf(Z_trace_filename, "%s", file_name);
	else
	{
		gethostname(hostname, 50);
		sprintf(Z_trace_filename, "z_trace_host-%s_pid-%d", hostname, getpid());
	}
    Z_trace_file = fopen(Z_trace_filename, "w");
}

void __Z_update_file_name()
{
#ifdef __Z_SUPPORT_MPI
	char temp[201];
    char hostname[50];
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    gethostname(hostname, 50);
    fclose(Z_trace_file);
    sprintf(temp, "jz_trace_host-%s_pid-%d_rank-%d", hostname, getpid(), rank);
    rename(Z_trace_filename, temp);
    Z_trace_file = fopen(temp, "a");
    strcpy(Z_trace_filename, temp);
#endif
}

void __Z_close_trace_file()
{
      fclose(Z_trace_file);
}
#endif //Z_REGION_TRACING

#ifdef Z_REGION_RESOURCE_MANAGEMENT
//the allocation unit always return an object with the layout
typedef struct obj_t
{
    Z_object_pool_t* pool;   //manipulated with size_t
    size_t is_unavailable;
    char obj_content[Z_UNDEFINED_SIZE];
}obj_t;
//Then obj_t can be casted to obj_content to be used. The goal is to allocate object right as link nodes to avoid separate mallocs

Z_pool_head_t* Z_create_pool_head(int item_size, int allocation_granularity)
{
	Z_pool_head_t* head = (Z_pool_head_t*)malloc(sizeof(Z_pool_head_t));
	head->first = NULL;
	
	//force pointer-boundary alignment
	head->sizeof_useful_content = (item_size/sizeof(size_t))*sizeof(size_t);  
	if((int)head->sizeof_useful_content < item_size)
		head->sizeof_useful_content += sizeof(size_t);

	head->outer_object_size = head->sizeof_useful_content + sizeof(void*) + sizeof(size_t);
	head->allocation_granularity = allocation_granularity;

	return head;
}
 
static Z_object_pool_t* allocate_pool(Z_pool_head_t* head)
{
    int pool_size = head->outer_object_size * head->allocation_granularity + sizeof(Z_object_pool_t); 
    Z_object_pool_t *unit = (Z_object_pool_t*)malloc(pool_size);
	unit->next = NULL;

#ifdef DEBUG_ALLOCATOR
	unit->num = head->nb_pools++; 
#endif
    return unit;
}
 
#define	ITEM_TO_OBJ_T(item) ((obj_t*)((size_t)(item) - sizeof(Z_object_pool_t*) - sizeof(size_t)))
#define	OBJ_T_TO_ITEM(obj) (void*)((size_t)(obj) + sizeof(Z_object_pool_t*) + sizeof(size_t))

#define	POOL_OBJ_ARRAY(pool) (void*)((size_t)(pool) + sizeof(Z_object_pool_t))
 
void* Z_allocate_object(Z_pool_head_t* head)
{
	//Z_WAIT_ONCE_FOR_DEBUGGER_IF(Z_RANK==2);
    Z_object_pool_t *pool = head->first;
    obj_t* obj = NULL;
    int item_size = head->sizeof_useful_content;
    int outer_object_size = head->outer_object_size;
    int next_object_offset = 0;
	Z_object_pool_t *new_pool;
	int allocation_granularity = head->allocation_granularity;
    int i;
 
#ifdef NO_CUSTOM_ALLOCATOR
	return calloc(item_size, 1);
#endif

    if(pool)
    {
		next_object_offset = 0;
        while(1)
        {
            if(pool->nb_free>0)
            {
                int i;
                for(i=0; i < allocation_granularity; i++)
                {
                    obj = (obj_t*)(&pool->object_array[next_object_offset]);
                    if(!obj->is_unavailable)
                    {
                        --pool->nb_free;
						obj->is_unavailable = TRUE;
                        memset(obj->obj_content, 0, item_size);
	
                        return (void*)obj->obj_content;
                    }
                    next_object_offset += outer_object_size;
                }
            }
            if(!pool->next)
                break;
            pool = pool->next;
			next_object_offset = 0;
        }
    }
 
    new_pool = allocate_pool(head);
    if(!head->first)
        head->first = new_pool;
    else 
    {
        pool->next = new_pool;
    }
    pool = new_pool;
    next_object_offset = 0;
    for(i = 0; i < allocation_granularity; i++)
    {
		obj = (obj_t*)(&pool->object_array[next_object_offset]);
		obj->pool = pool;
		obj->is_unavailable = FALSE;
		next_object_offset += outer_object_size;
    }
	obj = (obj_t*)(&pool->object_array[0]);
	pool->nb_free = allocation_granularity - 1;
	obj->is_unavailable = TRUE;
	memset(obj->obj_content, 0, item_size);

	return (void*)obj->obj_content;
}
 

void Z_free_object(Z_pool_head_t* head, void* object)
{
	(void)head;
#ifdef NO_CUSTOM_ALLOCATOR
	free(object);
	return;
#endif
	obj_t* obj = ITEM_TO_OBJ_T(object);
	obj->is_unavailable = FALSE;
	++obj->pool->nb_free;

	/*For now (and for simplicity), the pool number does not shrink to adapt to freed objects*/

	return;
}


BOOL Z_free_ref_counted_object(Z_ref_counted_obj_t* obj) //return TRUE if the object has been effectively deallocated
{
	if(--obj->ref_count == 0)
	{
		free(obj);
		return TRUE;
	}
	return FALSE;
}

#endif //Z_REGION_RESOURCE_MANAGEMENT


#ifdef Z_REGION_STRING_MANIPULATION
char* Z_trim_left(char *string)
{
    while(*string == ' ' || *string == '\t')
            string++;
    return string;
}

char* Z_trim_right(char *string)
{
    int end = strlen(string) - 1;
    while(string[end] == ' ' || string[end] == '\t' || string[end] == '\r' || string[end] == '\n')
        --end;
    string[end+1]=0;
    return string;
}

char* Z_trim(char *string)
{
    return Z_trim_right(Z_trim_left(string));
}

BOOL Z_str_begin_with(char *base_string, const char* begin_with_string)
{
    int nb_comp =  strlen(begin_with_string);
    if(nb_comp > (int)strlen(base_string))
        return FALSE;
    return strncmp(base_string, begin_with_string, nb_comp) == 0;
}

#endif //Z_REGION_STRING_MANIPULATION

#ifdef Z_REGION_STREAM_MANIPULATION
#ifdef __Z_SUPPORT_MPI
void Z_serialized_stream(void* data, uint32_t data_size, 
	void* additional_arg, MPI_Comm comm, 
	Z_stream_display_func_t display_func)
{
	int rank;
	int size;
	uint32_t remote_data_size = 0, new_remote_data_size;
	void* buf_for_remote_data = NULL;  //in proc 0
	MPI_Comm comm_dup;
	MPI_Comm_dup(comm, &comm_dup);
	MPI_Comm_size(comm_dup, &size);
	MPI_Comm_rank(comm_dup, &rank);

	int i;
	for(i=0; i<size; i++)
	{
		MPI_Barrier(comm_dup);
		if(rank == 0)
		{
			if(i == 0) 
			{
				display_func(data, data_size, 0, additional_arg);
			}
			else
			{
				MPI_Recv(&new_remote_data_size, 1, MPI_UNSIGNED, i, 0, comm_dup, MPI_STATUS_IGNORE);
				if(new_remote_data_size > remote_data_size)
				{
					if(buf_for_remote_data)
						free(buf_for_remote_data);
					buf_for_remote_data = malloc(new_remote_data_size);
					remote_data_size = new_remote_data_size;
				}
				MPI_Recv(buf_for_remote_data, new_remote_data_size, MPI_CHAR, i, 0, comm_dup, MPI_STATUS_IGNORE);
				display_func(buf_for_remote_data, new_remote_data_size, i, additional_arg);
			}
		}
		else if(i == rank)
		{
			MPI_Send(&data_size, 1, MPI_UNSIGNED, 0, 0, comm_dup);
			MPI_Send(data, data_size, MPI_CHAR, 0, 0, comm_dup);
		}
	}
	if(rank == 0 && buf_for_remote_data)
		free(buf_for_remote_data);
	MPI_Comm_free(&comm_dup);
}

#endif //__Z_SUPPORT_MPI
#endif //Z_REGION_STREAM_MANIPULATION
