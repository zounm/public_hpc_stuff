
# Prelude
This folder contains a few tools to diagnose and debug distributed programs.
It contains:

* pallstack (read P-ALL-STACK)
* zounm_utils

## pallstack
### Description:
A python tool that displays a snapshot of the backtrace of all the MPI processes
belonging to a given job on a node. I wrote it to debug deadlocks and other 
obscures waits at both application and middleware levels when working with MPI. 

Usage is `pallstack job_name` with `job_name` being the name of the processes
(or application)  as displayed by `ps` on the node.

pallstack is not just for MPI use; it can be useful for any multi* process
job.

### Prerequisites

_pstack_ is required for this to work

## zounm_utils
### Description:
It's a C "library". It is actually a C header and source to be more accurate.
The user can either compile it directly in his application or link against its
binary. zounm\_utils is just a collection of functions and other goodies that 
I  wrote to solve some issues and then found myself using frequently.

zounm\_utils is not meant for MPI programs only; although it has been used mostly
for that purpose. 

The library is clumsily organized in categories of features.

### Prerequisites
None if not using MPI features.
If MPI features are being used, MPI is expected to be installed.


### Features or categories of features

#### TIMING
Performance measurement is a recurrent activity in HPC. zounm_utils provides an
"instrument"-agnostic way of measuring time.

The supported instruments are:

* `MPI_Wtime`
 * When compiled with -D\_\_Z\_SUPPORT\_MPI
* `clock_gettime(CLOCK_MONOTONIC,...)`
 * When compiled with -DZ\_TIMING\_MODE\_CLOCK\_MONOTONIC
 * The user is expected to pin the process to a single core.
 * This "instrument" is the default if not running on Windows.
* `gettimeofday`
 * When compiled with -DZ\_TIMING\_MODE\_GETTIMEOFDAY
* `QueryPerformanceCounter`:
 * When compiled with -DZ\_WIN\_PERFORMANCE\_COUNTER
 * This instrument is (of course) valid only on Windows.

The timing API comes in _nameless_ and _named_ flavors. The nameless version 
behaves as having a SINGLE unnamed stop watch for the whole process. The named 
version behaves like having a named stopwatch meant for a specific use. Many 
of these named stopwatches can exist simultaneously in the same address space.

##### _Nameless_ stopwatch API
* `void Z_STAMP_START_TIME()` 
 * Start timing. 
 * If `Z_STAMP_START_TIME()` was previously called, any subsequent call 
 has the same effect as resetting and starting anew.
* `void Z_STAMP_END_TIME()`
 * Each call to this routine sets a split time (or lap time).
* `double Z_GET_DURATION_S()`
 * Returns the duration in seconds between the last call to 
 `Z_STAMP_START_TIME()` and `Z_STAMP_END_TIME()`
* `double Z_GET_DURATION_MS()`
 * Returns the duration in milliseconds between the last call to
`Z_STAMP_START_TIME()` and `Z_STAMP_END_TIME()`
* `double Z_GET_DURATION_US()`
 * Returns the duration in microseconds between the last call to
`Z_STAMP_START_TIME()` and `Z_STAMP_END_TIME()`
* `double Z_GET_DURATION_NS()`
 * Returns the duration in nanoseconds between the last call to
`Z_STAMP_START_TIME()` and `Z_STAMP_END_TIME()`

e.g.

```
Z_STAMP_START_TIME(); //start measuring
do_stuff_0();
Z_STAMP_END_TIME();  //set checkpoint for duration
double d0 = Z_GET_DURATION_S();
do_stuff_1();
Z_STAMP_END_TIME(); 
double d0_and_d1 = Z_GET_DURATION_S();
Z_STAMP_START_TIME(); //reset and start measuring
do_stuff_2();
Z_STAMP_END_TIME(); 
double d2 = Z_GET_DURATION_S();
```


##### _Named_ stopwatch API
* `Z_TIME(n)`
 * Declares a stopwatch identified by `n`. Two stopwatches cannot have the 
 same name in the same scope. Another variable can be named `n` in the same 
 scope though; `n` is not a programmatic variable name.
* `void Z_TIME_START(n)`
 * Behaves like `Z_STAMP_START_TIME()`; but for the stopwatch n.
* `void Z_TIME_END(n)`
 * Behaves like `Z_STAMP_END_TIME()`; but for the stopwatch n.
* `double Z_ELAPSED(n)`
 * Behaves like `Z_GET_DURATION_S()`; but for the stopwatch n.


#### TRACING	
For those tracing instructions that nobody wants to track and delete once
debugging is over.

Compiling with -D\_\_Z\_TRACE enables the tracing; otherwise, all the tracing
instructions are empty preprocessor directives.

##### API
* `Z_TRACE(...)`
 * Behaves like `printf(...)`
* `Z_TRACE_IF(cond,...)`
 * Behaves like `if(cond) printf(...)`


Sometimes, especially in distributed jobs, STDOUT does not allow clean
easy diagnoses. In those situations, it is easier to be able to distinguish
the streams per process; in which case tracing in files can come in handy.

zounm_utils allows tracing in file if compiled with 
-D\_\_Z\_TRACE and -D\_\_Z\_TRACE\_IN\_FILE.

##### API
* `Z_INIT_TRACE_IN_FILE()`
 * Must be called before any tracing in file.
* `Z_TRACE_IN_FILE(...)`
    * Like `Z_TRACE` but in a file.
* `Z_TRACE_IN_FILE_IF(cond,...)`
    * Like `Z_TRACE_IF` but in a file.
* `Z_END_TRACE_IN_FILE()`
    * Ends the tracing. This behaves like tracing teardown.


### DEBUGGING
These features provide breakpoint mechanisms that allows the debugging
of distributed computing middleware. The approach is a bit raw; but it
helped me countless of times.
I initially wrote these "tools" to be placed only inside the source code of 
middlewares.  Later on, I started using them from application level; but still 
mostly to debug middlewares. 

Compile with -D\_\_Z\_DEBUG to enable the breakpoint mechanisms.
During development and debugging time, a code base can end up being full of 
breakpoint mechanisms. Then, when the time comes to do performance testing or 
for release, compiling without \_\_Z\_DEBUG turns all the breakpoints mechanisms
into empty instructions.

##### API:
* `Z_WAIT_FOR_DEBUGGER()`
    * Execution stops here until a debugger is attached. In an SPMD program,
this instruction is likely to suspend all the processes in the job at this
point.
* `Z_WAIT_FOR_DEBUGGER_IF(cond)`
    * Same as `Z_WAIT_FOR_DEBUGGER()` but stops progression only if cond is true.
In an MPI job, cond could be a condition on process(es) rank(s) for instance.
 * e.g. `Z_WAIT_FOR_DEBUGGER_IF(rank % 2 == 0)`
* `Z_WAIT_ONCE_FOR_DEBUGGER()`
    * Same as `Z_WAIT_FOR_DEBUGGER()`; but occurs only once. After this instruction,
any subsequent encounter to any flavor of `Z_WAIT_FOR_DEBUGGER` will not suspend
the execution of the process. This instruction is very useful when the starting
point of the issue is hypothesized to be in a loop for instance.
* `Z_WAIT_ONCE_FOR_DEBUGGER_IF(cond)`
    * Conditional `Z_WAIT_ONCE_FOR_DEBUGGER`
* `Z_WAIT_FOR_DEBUGGER_ON_HOST(host)`
    * Behaves like `Z_WAIT_FOR_DEBUGGER()` but only processes running on the node
named _host_ will be suspended. host is a string that represents the name of
the host as provided by gethostname.
* `Z_WAIT_FOR_DEBUGGER_ON_HOST_IF(cond, host)`
* `Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST()`
* `Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(cond,host)` 
 * Provided to behave like their aforementioned counterparts; but only if the 
processes that encounter them are on the specified host. 
* `Z_TIMED_WAIT_FOR_DEBUGGER(timeout_in_microseconds)`
    * behaves like `Z_WAIT_FOR_DEBUGGER()`; but if after _timeout\_in\_microseconds_ a 
debugger is not attached to the process, the process just moves on.
* `Z_TIMED_WAIT_FOR_DEBUGGER_IF(timeout,cond)`
* `Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST(timeout,host)`
* `Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond)`
* `Z_TIMED_WAIT_ONCE_FOR_DEBUGGER(timeout)`
* `Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_IF(timeout,cond)`
* `Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST(timeout,host)`
* `Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond)`
    * Behave like their aforementioned counterparts but with the timeout constraint.


### MPI DIAGNOSTIC AND FAILURE MANAGEMENT
Compile with -D\_\_Z\_SUPPORT\_MPI. 
Not compiling with \_\_Z\_SUPPORT\_MPI does not transform the instructions below
into empty ones. The compiler will complain about undefined symbols. These
instructions are not meant for temporary use during dev and debugging; they are
meant to be part of the legitimate instruction flow of the program.

##### API
* `Z_PRINT_FAILURE_STATUS(ret)`
    * Does the equivalent of `perror` for MPI routines return statuses. ret is
the return value of the MPI routine of interest.
* `Z_ABORT_ON_FAILURE(ret)`
    * Abort an MPI job if the MPI call that returned _ret_ failed.
* `Z_EXIT(...)`
    * Display the printf-formatted arguments passed to it and terminates
the MPI job. The exit status is always EXIT_SUCCESS.
* `Z_EXIT_IF(cond, ...)`
    * conditional `Z_EXIT(...)`
* `Z_FATAL(...)`
    * Display the printf-formatted arguments passed to it and terminates
the MPI job. The exit status is EXIT_FAILURE.
* `Z_FATAL_IF(cond, ...)`
    * Like `Z_FATAL(...)` but conditional

### GENERAL-PURPOSE DIAGNOSTIC AND FAILURE MANAGEMENT
Useful for both MPI and non-MPI programs or jobs.

##### API
* `Z_TERMINATE_ON_FAILURE(healthy_cond, error, msg)`
    * if _healthy\_cond_ evaluates to FALSE, then the custom message _msg_ is 
displayed along with the string error message of _error_; and the program
is terminated. _error_ is expected to be a valid `errno`.
* `Z_PRINT_ERROR(...)`
    * Behaves like `fprintf(stderr)` but with the error message in red. Useful when
the program has abundant display; and one wants to easily visually distinguish
the error messages.
* `Z_PRINT_ERROR_IF(cond,...)`
    * Same as above; but conditional.

### FORCING THE GENERATION OF CORE FILES
##### API
* `Z_CRASH()`
    * Force termination of the program or job with the intent of generating a core dump.
* `Z_CRASH_IF(cond)`
    * conditional `Z_CRASH()`


### OUTPUT STREAM MANIPULATION FOR MPI JOB
If your MPI job displays stuff in the shell from multiple processes, you will have
"_to puz solve zle a to read output the in order the_" ... well, you know what I mean.
zounm\_utils provides `Z_serialized_stream` to force a desired order in the display.
I used this facility mostly for tracing and debugging.

See examples/serialized\_stream\_test.c for an example using this function.
 
Compile with -D\_\_Z\_SUPPORT\_MPI

##### API
*  `void Z_serialized_stream(void* data, uint32_t data_size, 
void* additional_arg, MPI_Comm comm, Z_stream_display_func_t display_func)`
    * Each process in comm calls the function with the data to display. 
The display order is defined by the rank ordering in _comm_. The output of each
process in _comm_ appears to be displayed atomically with respect to the otheri
peers in _comm_. The parameters are as follows :
 	 * data: The data to display.
 	 * data_size: Size of data in bytes
 	 * additional_arg: whatever other obscure argument `Z_stream_display_func_t`
might need. In many cases, it would just be NULL
 	 * comm: The communicator that defines the processes whose output must be
serialized
 	 * display_func: The type of the custom display function. It is defined as
`void (*Z_stream_display_func_t)(void* data, uint32_t size, int rank, 
void* additional_arg)`

### COMPLEX INTEGER INPUT PARSING
I wrote a lot of (micro)benchmarks that output some performance metric in terms
of some increasing integer-based X axis value. The X axis value could be time,
message size, etc. I wrote the complex input parser to allow flexible and compact
specification of the aforementioned kinds of X axis values.

See examples/complex\_input\_test.c for an example using this mechanism.

The parser operates in two modes:

* Stream mode: the parsed values are returned one at a time
* All at once mode: All the parsing is done at once before the values could be
consummed.

##### API
* `Z_init_complex_input_extraction(char* complex_input)`
    * Initializes the parser for stream parsing. 
* `BOOL Z_extract_next_from_complex_input(int *extracted_input)`
    * If there is at least one value left to be parsed, extract the next
one, put it in extracted_input and return TRUE; otherwise return FALSE.
This function is typically called in a (while) loop.
* `void Z_get_complex_metric_distribution(char* complex_input, int **distro,
int *max_metric_value, int *nb_metric_values)`
    * Does an all-at-once parsing. The parameters are as follows:
     * complex\_input: The complex input to parse
     * distro: the array of parsed values to return. This is allocated internally
by the function and must be deallocated after use by the caller.
     * max\_metric\_value: The maximum of the parsed values
     * nb\_metric\_values: the count of distro

### MISC
##### API
* `Z_WORK(duration_in_microseconds)`
    * Synthetic CPU-bound work. This call will occupy the CPU for a specified duration.
