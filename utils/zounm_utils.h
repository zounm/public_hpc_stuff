/*Author: Judicael Zounmevo (zounm@linux.com)
 * */

#ifndef __ZOUNM_UTILS__
#define __ZOUNM_UTILS__

//#define __Z_SUPPORT_MPI

#include <stdlib.h>

#ifdef WIN32
#include <windows.h> 
#define Z_WIN_PERFORMANCE_COUNTER
#else
#include <unistd.h>
#endif

#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#ifdef __Z_SUPPORT_MPI
#include "mpi.h"
#endif
#include <stdint.h>
#include <assert.h>

#define Z_REGION_CONSTANTS
#define Z_REGION_TIMING
#define Z_REGION_DEBUGGING
#define Z_REGION_TRACING
#define Z_REGION_ERROR_REPORTING
#define Z_REGION_DATA_STRUCTURES
#define Z_REGION_INPUT_MANAGEMENT
#define Z_REGION_MISC_ROUTINES
#define Z_REGION_MATHS
#define Z_REGION_RESOURCE_MANAGEMENT
#define Z_REGION_STRING_MANIPULATION
#define Z_REGION_STREAM_MANIPULATION


#define VARIABLE_SIZE_DATA char
#define Z_UNDEFINED_SIZE 0
#define Z_UNKNOWN_SIZE_OBJECT

#ifndef WIN32
typedef enum BOOL
{
	FALSE,
	TRUE = !FALSE
}BOOL;
#endif

#ifdef Z_REGION_CONSTANTS
#define THOUSAND	1000.0
#define MILLION	1000000.0
#define BILLION	1000000000.0

#define GREEN_TEXT	"\x1b[32m"
#define RED_TEXT     "\x1b[31m"
#define NEUTRAL_TEXT   "\x1b[0m"


#define B	1.0
#define KB	1024.0
#define MB	1048576.0
#define GB	1073741824.0
#endif //Z_REGION_CONSTANTS


#ifdef Z_REGION_TIMING
void init_timing();
#ifndef Z_WIN_PERFORMANCE_COUNTER
//#define Z_TIMING_MODE_GETTIMEOFDAY	
#ifdef __Z_SUPPORT_MPI
#define Z_TIMING_MODE_WTIME	
#endif 
#define Z_TIMING_MODE_CLOCK_MONOTONIC
#endif //Z_WIN_PERFORMANCE_COUNTER

#if defined(Z_TIMING_MODE_GETTIMEOFDAY)
#ifndef WIN32
#include <sys/time.h>
#endif
	#define INIT_TIMING()	
	#define DIFF_TIME_US(end,start) \
		((end).tv_sec*MILLION+(end).tv_usec-(start).tv_sec*MILLION-(start).tv_usec)
	#define DIFF_TIME_NS(end,start) (DIFF_TIME_US((end),(start))*THOUSAND)
	#define DIFF_TIME_MS(end,start) (DIFF_TIME_US((end),(start))/THOUSAND)
	#define DIFF_TIME_S(end,start) (DIFF_TIME_US((end),(start))/MILLION)

	extern struct timeval __timeval_temp1;
	extern struct timeval __timeval_temp2;

	#define Z_STAMP_START_TIME() gettimeofday(&__timeval_temp1, NULL)
	#define Z_STAMP_END_TIME() gettimeofday(&__timeval_temp2, NULL)

	#define Z_GET_DURATION_NS() DIFF_TIME_NS(__timeval_temp2, __timeval_temp1)
	#define Z_GET_DURATION_US() DIFF_TIME_US(__timeval_temp2, __timeval_temp1)
	#define Z_GET_DURATION_MS() DIFF_TIME_MS(__timeval_temp2, __timeval_temp1)
	#define Z_GET_DURATION_S() DIFF_TIME_S(__timeval_temp2, __timeval_temp1)

	#define Z_DECLARE_TIME_VAR(__var_name)	struct timeval __var_name
	#define Z_STAMP_TIME_VAR(__var_name) gettimeofday(&__var_name, NULL)
	#define Z_GET_DURATION_US_VAR(__time_start,__time_end) DIFF_TIME_US((__time_end), (__time_start))

#elif defined(Z_TIMING_MODE_WTIME)
	extern double __wtime_temp1;
	extern double __wtime_temp2;

	#define INIT_TIMING()	

	#define Z_STAMP_START_TIME() __wtime_temp1=MPI_Wtime()
	#define Z_STAMP_END_TIME() __wtime_temp2=MPI_Wtime()
		
	#define Z_GET_DURATION_S() (__wtime_temp2 - __wtime_temp1)
	#define Z_GET_DURATION_NS() (Z_GET_DURATION_S()*BILLION)
	#define Z_GET_DURATION_US() (Z_GET_DURATION_S()*MILLION)
	#define Z_GET_DURATION_MS() (Z_GET_DURATION_S()*THOUSAND)

	#define Z_DECLARE_TIME_VAR(__var_name)	double __var_name
	#define Z_STAMP_TIME_VAR(__var_name) __var_name=MPI_Wtime()
	#define Z_GET_DURATION_US_VAR(__time_start,__time_end) (((__time_end)-(__time_start))*MILLION)

#elif defined(Z_TIMING_MODE_CLOCK_MONOTONIC)
	#define INIT_TIMING() init_timing()

	#define DIFF_TIME_NS(end,start) (end.tv_sec*BILLION+end.tv_nsec-\
	start.tv_sec*BILLION-start.tv_nsec)
	#define DIFF_TIME_US(end,start) (DIFF_TIME_NS((end),(start))/THOUSAND)
	#define DIFF_TIME_MS(end,start) (DIFF_TIME_NS((end),(start))/MILLION)
	#define DIFF_TIME_S(end,start) (DIFF_TIME_NS((end),(start))/BILLION)


	extern struct timespec __timespec_temp1;
	extern struct timespec __timespec_temp2;

	#define Z_STAMP_START_TIME() clock_gettime(CLOCK_MONOTONIC, &__timespec_temp1)
	#define Z_STAMP_END_TIME() clock_gettime(CLOCK_MONOTONIC, &__timespec_temp2)

	#define Z_GET_DURATION_NS() DIFF_TIME_NS(__timespec_temp2, __timespec_temp1)
	#define Z_GET_DURATION_US() DIFF_TIME_US(__timespec_temp2, __timespec_temp1)
	#define Z_GET_DURATION_MS() DIFF_TIME_MS(__timespec_temp2, __timespec_temp1)
	#define Z_GET_DURATION_S() DIFF_TIME_S(__timespec_temp2, __timespec_temp1)

	#define Z_DECLARE_TIME_VAR(__var_name)	struct timespec __var_name
	#define Z_STAMP_TIME_VAR(__var_name) clock_gettime(CLOCK_MONOTONIC, &__var_name)
	#define Z_GET_DURATION_US_VAR(__time_start,__time_end) DIFF_TIME_US((__time_end), (__time_start))
#elif defined(Z_WIN_PERFORMANCE_COUNTER)
	
#define INIT_TIMING() init_timing()
	extern LARGE_INTEGER __perf_frequency;
	extern LARGE_INTEGER __perf_time1;
	extern LARGE_INTEGER __perf_time2;
	
	#define DIFF_TIME_S(end,start) ((1.0*(__perf_time2.QuadPart-__perf_time1.QuadPart))/__perf_frequency.QuadPart)
	#define DIFF_TIME_NS(end,start) (DIFF_TIME_S(end,start)*BILLION)
	#define DIFF_TIME_US(end,start) (DIFF_TIME_S(end,start)*MILLION)
	#define DIFF_TIME_MS(end,start) (DIFF_TIME_S(end,start)*THOUSAND)

	#define Z_STAMP_START_TIME() QueryPerformanceCounter(&__perf_time1);
	#define Z_STAMP_END_TIME() QueryPerformanceCounter(&__perf_time2);

	#define Z_GET_DURATION_NS() DIFF_TIME_NS(__perf_time2, __perf_time1)
	#define Z_GET_DURATION_US() DIFF_TIME_US(__perf_time2, __perf_time1)
	#define Z_GET_DURATION_MS() DIFF_TIME_MS(__perf_time2, __perf_time1)
	#define Z_GET_DURATION_S() DIFF_TIME_S(__perf_time2, __perf_time1)

	#define Z_DECLARE_TIME_VAR(__var_name)	LARGE_INTEGER __var_name
	#define Z_STAMP_TIME_VAR(__var_name) QueryPerformanceCounter(&__var_name);
	#define Z_GET_DURATION_US_VAR(__time_start,__time_end) DIFF_TIME_US((__time_end), (__time_start))
#endif

#define Z_TIME(n)	Z_DECLARE_TIME_VAR(__z_time_start_##n); Z_DECLARE_TIME_VAR(__z_time_end_##n)
#define Z_TIME_START(n) Z_STAMP_TIME_VAR(__z_time_start_##n)
#define Z_TIME_END(n) Z_STAMP_TIME_VAR(__z_time_end_##n)
#define Z_ELAPSED(n)	Z_GET_DURATION_US_VAR(__z_time_start_##n, __z_time_end_##n)

#endif //Z_REGION_TIMING

#ifdef Z_REGION_TRACING
#define Z_PRINT_RAW(channel,...)	do{fprintf((channel), __VA_ARGS__); fflush((channel));} while(0)
#define Z_PRINT_RAW_IF(cond,channel,...)	do{if(cond){Z_PRINT_RAW((channel), __VA_ARGS__);}} while(0)

#define Z_PRINT(...)	Z_PRINT_RAW(stdout, __VA_ARGS__)
#define Z_PRINT_IF(cond,...)	Z_PRINT_RAW_IF((cond), stdout, __VA_ARGS__)
#define Z_WARN(...)	Z_PRINT_RAW(stderr, __VA_ARGS__)
#define Z_WARN_IF(cond,...)	Z_PRINT_RAW_IF((cond), stderr, __VA_ARGS__)

/*TRACING*/
//#define __Z_TRACE

#ifdef __Z_TRACE
	#define Z_TRACE(...)	Z_PRINT(__VA_ARGS__)
	#define Z_TRACE_IF(cond,...)	Z_PRINT_IF((cond), __VA_ARGS__)
#else
	#define Z_TRACE(...)
	#define Z_TRACE_IF(cond,...)
#endif

	#ifdef __Z_TRACE_IN_FILE
        extern FILE* Z_trace_file;
        void __Z_open_trace_file(char* file_name);
        void __Z_close_trace_file();
        void __Z_update_file_name();
        #define Z_INIT_TRACE_IN_FILE() __Z_open_trace_file(NULL)
		#define Z_INIT_TRACE_IN_FILE_WITH_NAME(file_name) __Z_open_trace_file(file_name)
        #define Z_UPDATE_TRACE_FILE_NAME() __Z_update_file_name()
        #define Z_END_TRACE_IN_FILE()  __Z_close_trace_file()
        #define Z_TRACE_IN_FILE(...) do{fprintf(Z_trace_file, __VA_ARGS__); fflush(Z_trace_file);}while(0)
        #define Z_TRACE_IN_FILE_IF(cond,...) do{if((cond)){Z_TRACE_IN_FILE(__VA_ARGS__);}}while(0)
    #else
        #define Z_INIT_TRACE_IN_FILE()
        #define Z_UPDATE_TRACE_FILE_NAME()
        #define Z_END_TRACE_IN_FILE()
        #define Z_TRACE_IN_FILE(...)
        #define Z_TRACE_IN_FILE_IF(cond,...)
    #endif

#endif //Z_REGION_TRACING


#ifdef Z_REGION_DEBUGGING
void Z_wait_for_debugger(const char* hostname, const char* file, int line, BOOL once);
void Z_timed_wait_for_debugger(double time, const char* hostname, const char* file, int line, BOOL once);
#define __Z_DEBUG
#ifdef __Z_DEBUG
	#define	Z_WAIT_FOR_DEBUGGER()	Z_wait_for_debugger(NULL, __FILE__, __LINE__, FALSE)
	#define	Z_WAIT_FOR_DEBUGGER_IF(cond) do{if((cond)){Z_WAIT_FOR_DEBUGGER();}}while(0)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER() Z_wait_for_debugger(NULL, __FILE__, __LINE__, TRUE)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_IF(cond) do{if((cond)){Z_WAIT_ONCE_FOR_DEBUGGER();}}while(0)
	#define	Z_WAIT_FOR_DEBUGGER_ON_HOST(host)	Z_wait_for_debugger((host), __FILE__, __LINE__, FALSE)
	#define	Z_WAIT_FOR_DEBUGGER_ON_HOST_IF(cond,host) do{if((cond)){Z_WAIT_FOR_DEBUGGER_ON_HOST((host));}}while(0)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST(host)	Z_wait_for_debugger((host), __FILE__, __LINE__, TRUE)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(cond,host) do{if((cond)){Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST((host));}}while(0)

	#define Z_TIMED_WAIT_FOR_DEBUGGER(timeout) Z_timed_wait_for_debugger((timeout), NULL, __FILE__, __LINE__, FALSE)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_IF(timeout,cond) do{if((cond)){Z_TIMED_WAIT_FOR_DEBUGGER((timeout));}}while(0)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST(timeout,host) Z_timed_wait_for_debugger((timeout), (host), __FILE__, __LINE__, FALSE)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond) do{if((cond)){Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST((timeout), (host));}}while(0)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER(timeout)  Z_timed_wait_for_debugger((timeout), NULL, __FILE__, __LINE__, TRUE)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_IF(timeout,cond) do{if((cond)){Z_TIMED_WAIT_ONCE_FOR_DEBUGGER((timeout));}}while(0)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST(timeout,host) Z_timed_wait_for_debugger((timeout), (host), __FILE__, __LINE__, TRUE)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond) do{if((cond)){Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST((timeout),(host));}}while(0)
#else
	#define	Z_WAIT_FOR_DEBUGGER()
	#define	Z_WAIT_FOR_DEBUGGER_IF(cond)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER()
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_IF(cond)
	#define	Z_WAIT_FOR_DEBUGGER_ON_HOST(host)
	#define	Z_WAIT_FOR_DEBUGGER_ON_HOST_IF(cond,host)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST(host)
	#define	Z_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(cond,host)

	#define Z_TIMED_WAIT_FOR_DEBUGGER(timeout)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_IF(timeout,cond)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST(timeout,host)
	#define Z_TIMED_WAIT_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER(timeout)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_IF(timeout,cond)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST(timeout,host)
	#define Z_TIMED_WAIT_ONCE_FOR_DEBUGGER_ON_HOST_IF(timeout,host,cond)
#endif
#endif //Z_REGION_DEBUGGING

#ifdef Z_REGION_ERROR_REPORTING
#ifdef __Z_SUPPORT_MPI
#define MAX_ERR_STRING_LEN 251
extern char err_string[];
#define Z_PRINT_FAILURE_STATUS(_ret)\
	    do{char err_string[MAX_ERR_STRING_LEN];\
			if((_ret)!= MPI_SUCCESS){int err_string_len, rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank);			\
			MPI_Error_string((_ret), err_string, &err_string_len);          								\
			Z_PRINT("line %d|", __LINE__ -1); Z_PRINT("%s[%d]: %s %s\n", RED_TEXT,       						\
							                    rank, err_string, NEUTRAL_TEXT);}}while(0)

#define Z_ABORT_ON_FAILURE(_ret) \
	do{if((_ret) != MPI_SUCCESS) {Z_PRINT_FAILURE_STATUS((_ret)); MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);}}while(0)
#define Z_EXIT(...) \
	do{int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank); Z_PRINT_IF(rank==0, __VA_ARGS__); MPI_Finalize(); return 0;}while(0)
#define Z_EXIT_IF(cond,...) do{if((cond))Z_EXIT(__VA_ARGS__);}while(0)
#define Z_FATAL(...) \
	do{int rank; MPI_Comm_rank(MPI_COMM_WORLD, &rank); Z_PRINT_IF(rank==0, __VA_ARGS__); \
		MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);}while(0)
#define Z_FATAL_IF(cond,...) do{if((cond))Z_FATAL(__VA_ARGS__);}while(0)
#else
#define Z_ABORT_ON_FAILURE(healthy_cond, error, msg)\
	do{																			\
		int err = (error);														\
		if(!(healthy_cond))														\
		{																		\
			fprintf(stderr, "%s[%s]\n", ((msg)), strerror((err))); 				\
			int* i = NULL; *i = 0;												\
		}																		\
	}while(0)

#endif //__Z_SUPPORT_MPI
#define Z_PRINT_ERROR(...) do{printf("%s", RED_TEXT); printf(__VA_ARGS__); Z_PRINT("%s", NEUTRAL_TEXT);}while(0)
#define Z_PRINT_ERROR_IF(cond,...) do{if((cond))Z_PRINT_ERROR(__VA_ARGS__);}while(0)
#endif //Z_REGION_ERROR_REPORTING


#ifdef Z_REGION_MISC_ROUTINES
#define Z_WORK(duration/*in us*/)											\
do {																	\
	if((duration)>0)													\
	{	Z_DECLARE_TIME_VAR(__WE_start);									\
		Z_DECLARE_TIME_VAR(__WE_end);										\
		Z_STAMP_TIME_VAR(__WE_start);										\
		Z_STAMP_TIME_VAR(__WE_end);										\
		while(Z_GET_DURATION_US_VAR(__WE_start, __WE_end)<(duration))		\
			Z_STAMP_TIME_VAR(__WE_end);									\
	}																	\
}while(0)

#define Z_CRASH() do{char* p =NULL; Z_PRINT("Crashing at %s:%d\n", __FILE__, __LINE__); *p = 'a';}while(0)
#define Z_CRASH_IF(cond) do{if((cond)){Z_CRASH();}}while(0)
#endif //Z_REGION_MISC_ROUTINES

#ifdef Z_REGION_DATA_STRUCTURES
typedef struct Z_node_link_t
{
    struct Z_node_link_t* next;
}Z_node_link_t;
void __add_node(void** node_head, void** node_tail, void* node);
void __remove_node(void** node_head, void** node_tail, void* node);
typedef BOOL (*node_predicate_t)(void* node, void* arg); 
void* Z_find_node(void* node_head, node_predicate_t predicate, void* predicate_arg);
void* Z_pop_front(void** node_head, void** node_tail);
void* Z_pop_back(void** node_head, void** node_tail);

#define Z_add_node(head,tail,node) __add_node((void**)(head), (void**)(tail), (void*)(node))
#define Z_remove_node(head,tail,node) __remove_node((void**)(head), (void**)(tail), (void*)(node))
#endif //Z_REGION_DATA_STRUCTURES

#ifdef Z_REGION_INPUT_MANAGEMENT
void Z_init_complex_input_extraction(char* raw_input);
BOOL Z_extract_next_from_complex_input(int *extracted_input);
void Z_get_complex_metric_distribution(char* complex_input, int **distro, 
		int *max_metric_value, int *nb_metric_values);

BOOL Z_is_option_set(int argc, char **argv, char option);
BOOL Z_get_char_option_value(int argc, char **argv, char option, char *val);
BOOL Z_get_int_option_value(int argc, char **argv, char option, int *val);
BOOL Z_get_double_option_value(int argc, char **argv, char option, double *val);

//Fails if min_required_length +1 > max_val_length
BOOL Z_get_string_option_value(int argc, char **argv, char option, char val[], 
		int max_val_length, int* min_required_length);
#endif //Z_REGION_INPUT_MANAGEMENT

#ifdef Z_REGION_RESOURCE_MANAGEMENT

typedef struct Z_object_pool_t
{
    long nb_free;
    struct Z_object_pool_t* next;
	VARIABLE_SIZE_DATA object_array[Z_UNDEFINED_SIZE];
      //void* array_of_objects;

}Z_object_pool_t;

typedef struct Z_pool_head_t
{
    unsigned int sizeof_useful_content;
    unsigned int outer_object_size;
	int allocation_granularity;
    Z_object_pool_t *first;
}Z_pool_head_t;

struct Z_pool_head_t* Z_create_pool_head(int item_size, int allocation_granularity);
void* Z_allocate_object(struct Z_pool_head_t* head);
void Z_free_object(struct Z_pool_head_t* head, void* object);

typedef struct Z_ref_counted_obj_t
{
	int ref_count;
}Z_ref_counted_obj_t;
BOOL Z_free_ref_counted_object(Z_ref_counted_obj_t* obj); //return TRUE if the object has been effectively deallocated

#define Z_ACQUIRE_OBJECT(obj) (++((Z_ref_counted_obj_t*)(obj))->ref_count)
#define Z_RELEASE_OBJECT(obj) (free_ref_counted_object((Z_ref_counted_obj_t*)(obj)))
#endif //Z_REGION_RESOURCE_MANAGEMENT

#ifdef Z_REGION_MATHS
#define Z_MAX(a,b) \
	({__typeof__((a)) __a = (a); __typeof__((b)) __b = (b); __a > __b ? __a : __b;})

#define Z_MIN(a,b) \
	({__typeof__((a)) __a = (a); __typeof__((b)) __b = (b); __a <= __b ? __a : __b;})
#endif //Z_REGION_MATHS

#ifdef Z_REGION_STRING_MANIPULATION
char* Z_trim_left(char *string);
char* Z_trim_right(char *string);
char* Z_trim(char *string);
BOOL Z_str_begin_with(char *base_string, const char* begin_with_string);
#endif //Z_REGION_STRING_MANIPULATION
#endif //__ZOUNM_UTILS__

#ifdef Z_REGION_STREAM_MANIPULATION
#ifdef __Z_SUPPORT_MPI
typedef void(*Z_stream_display_func_t)(void* data, uint32_t size, int rank,
	 void* additional_arg);
void Z_serialized_stream(void* data, uint32_t data_size, 
	void* additional_arg, MPI_Comm comm, 
	Z_stream_display_func_t display_func);
#endif //__Z_SUPPORT_MPI
#endif //Z_REGION_STREAM_MANIPULATION
