#include <unistd.h>
#include "mpi.h"
#include <stdio.h>
#include "zounm_utils.h"


void print_usage()
{
	printf("Description: Example of complex_input parsing. \n"
			"\tThere are two ways of parsing complex inputs: \n"
			"\t\t-As a stream: The parsed values are consumed as they are returned\n"
			"\t\t    by the parser.\n"
			"\t\t-All parsed at once: The parser produces an array of the parsed values at once.\n"
			"\n"
			"\tOptions are: \n"
			"\t -h  --Display this help\n"
			"\t -s complex_input_spec --Read on for explanations\n"
			"\t -t once|stream  --if stream, the parser will operate in stream mode. If once, \n"
			"\t     it will operate in the other mode. Both modes will produce the same output;\n"
			"\t     all this is for the sake of showing the difference in the code.\n"
		"\n=================COMPLEX INPUTS==================== \n"
            "Complex input are a way of specifying a large number of option arguments for a numeric option such as size or time.\n"
            "Let's describe it using the size for instance (unit is assumed to be in KB for instance). "
            "The size option can be specified as follows:"
            "\n\t\t1) single value(e.g. -s 100)\n\t\t2) a list of value (e.g. -s 100,200,300,400 ...)\n\t\t3) start,"
            "increment,end (e.g. -s i:100,100:1000) =>100,200,300,...1000\n\t\t4) start,factor,end "
            "(e.g. -s f:100,2,1000) => 100,200,400,...,1000\n"
            "\t\t5) compound_form;compound_form... where the first compound_form (CF) can be any of the previous forms"
            " and each of the subsequent CF assumes its start to be the max of the previous CF."
            "\n\t\t  (e.g. -s f:8,2,128+i:128,1024+i:1024,4096) would be sizes from 8KB doubling until 128KB; \n"
			"then increasing by 128KB until 1MB; then increasing 1MB at a time until 4MB\n\n");
}


int main(int argc, char** argv)
{
	enum parse_type_t
	{
		PT_STREAM,
		PT_ONCE
	}parse_type = PT_STREAM;

	char* complex_input = NULL;

	int c;
	while( ( c = getopt(argc, argv, "s:t:h")) != -1)
	{
		switch(c)
		{
			case 's': 
				complex_input = malloc(strlen(optarg));
				strcpy(complex_input, optarg);
				break;
			case 't':
				if(strcmp(optarg, "once") == 0)
					parse_type = PT_ONCE;
				else if(strcmp(optarg, "stream") != 0)
				{
					fprintf(stderr, "Unsupported parse type\n");
					print_usage();
					exit(EXIT_FAILURE);
				}
				break;
			case 'h':
				print_usage();
				return 0;
			default:
				print_usage();
				exit(EXIT_FAILURE);
		}
	}
	if(parse_type == PT_STREAM)
	{
		printf("Stream parsing:\n");
		int val;
		Z_init_complex_input_extraction(complex_input);
		while(Z_extract_next_from_complex_input(&val))
			printf("%d\n", val);
	}
	else
	{
		printf("All at once parsing:\n");
		int* all_vals = NULL;
		int nb_vals;
		int max_val;	
		Z_get_complex_metric_distribution(complex_input, &all_vals, &max_val, &nb_vals);

		int i;
		for(i=0; i<nb_vals; i++)
			printf("%d\n", all_vals[i]);
		free(all_vals);
	}
	return 0;
}
