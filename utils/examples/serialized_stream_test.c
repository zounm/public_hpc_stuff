#include <unistd.h>
#include "mpi.h"
#include <stdio.h>
#include "zounm_utils.h"

#define MAX_CHAR_BUF_LEN	250

void print_usage()
{
	printf(" Description: Test of Z_serialized_stream.\n"
			"Options are: \n"
			"\t -h  --Display this help\n"
			"\t -i  --Display process identities (in order)\n"
			"\t -s  --Generate a dummy sorted data and display it.\n"
			"\t         This test pretends to sort a distributed array of uint64_t\n"
			"\t         At the end of the sort, each rank from 0 to job_size-1 holds\n"
			"\t         a partition of the array in such a way that the concatenation\n"
			"\t         from rank 0 to job_size-1 would yield the sorted array.\n"
			"\t         While the sorted array can be displayed with other means,\n"
			"\t         this dummy test is meant to show how how it could be done with\n"
			"\t         Z_serialized_stream.\n"
		  );
}

void identify_self(void* data, uint32_t data_size, int rank, void* additional_arg)
{
	(void) rank; //ignored
	(void) additional_arg; // ignored

	printf("I'm rank %d on host \"%s\"\n", rank, (char*)data);
}

void display_sorted(void* data, uint32_t data_size, int rank, void* additional_arg)
{
	(void)rank; //ignored
	(void)additional_arg; //ignored
	uint64_t *array = (uint64_t*)data;
	int count = data_size / sizeof(uint64_t);
	int i;
	for(i=0; i<count; i++)
		printf("%lu\n", array[i]);
}

int main(int argc, char** argv)
{
	int rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	char hostname [MAX_CHAR_BUF_LEN];

	BOOL display_id = FALSE;
	BOOL sort = FALSE;

	int c;
	while( (c = getopt(argc, argv, "ish")) != -1)
	{
		switch(c)
		{
			case 'i': display_id = TRUE; break;
					  break;
			case 's': sort = TRUE; break;
			case 'h': 
			default:
					  if(rank == 0)
						  print_usage();
					  Z_EXIT(" ");
		}
	}
	if(display_id)
	{
		gethostname(hostname, MAX_CHAR_BUF_LEN);
		Z_serialized_stream((void*)hostname, strlen(hostname), NULL, MPI_COMM_WORLD, 
				identify_self);
	}
	if(sort)
	{
		uint64_t array[10];
		int i;
		uint64_t start = rank*10;
		for(i=0; i<10; i++)
			array[i] = start + i;
		Z_serialized_stream((void*)array, sizeof(array), NULL, MPI_COMM_WORLD, 
				display_sorted);
	}
	MPI_Finalize();
	return 0;
}

