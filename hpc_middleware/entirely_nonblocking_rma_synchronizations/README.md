# Prelude

To understand this work (**and before using it**), you are encouraged to read
this [paper](http://post.queensu.ca/~pprl/papers/SC-2014.pdf)

[MVAPICH](http://mvapich.cse.ohio-state.edu/) is a highly-tuned InfiniBand and 
RoCE-based MPI implementation provided
and maintained by the NOWLAB of Dr. Panda in OSU. It is based on the popular
[MPICH](https://www.mpich.org/) of Argonne National Laboratory. MVAPICH is used,
among other things, by the 10th most powerful supercomputer in the world 
(as of November 2015).

[MPI-3.0](http://www.mpi-forum.org/docs/mpi-3.0/mpi30-report.pdf) RMA is
fundamentally nonblocking; but its epoch-closing synchronizations are blocking
so as to provide a release-consistency-like memory model. This folder contains
the source code implementation of the first attempt to make these
synchronizations **entirely nonblocking**; allowing the HPC programmer to:

* Mitigate late peer delays by overlapping them with post-epoch activities 
(data dependency permitting).
* Achieve a higher level of communication/computation overlapping.
* Achieve communication/communication overlapping by:
	* Overlapping the communications of an epoch with those of subsequent ones.
	* Overlapping the communications of an epoch with subsequent 2-sided or
	  collectives.
* Instruct the progress engine for safe and aggressive optimizations based on
  the knowledge of his application.
* Enable or facilitate new algorithmic approaches in those moments where the HPC
  programmer finds himself:
	* Creating multiple RMA windows only for the sake of having the same kind 
	of epoch over exactly the same process group and the same local memory
	  region (Both error-prone and not scalable).
	* Creating multiple threads for the sake achieving concurrency over the same
	  RMA window object (Both error-prone and not scalable).

In situations where peer delays can propagate, an application written with
nonblocking epoch-closing synchronizations will usually outperform its
equivalent using pure MPI-3.0 RMA (even on the most finely tuned MPI 
implementation over the same network technology).

The implementation is aggressively-optimized and fast; it outperforms
production-quality implementations. However, bear in mind that it remains
a proof-of-concept implementation of the concepts introduced in the
[paper](http://post.queensu.ca/~pprl/papers/SC-2014.pdf); so it most
definitely lacks all the rigor (e.g. error checking) expected from a 
production-quality work. 

# Prerequisites

This work _replaces_ MPI-3.0 RMA inside MVAPICH-2-1.9. The user is assumed to be
familiar with MPI-3.x  RMA (and to a lesser extent MVAPICH-2).

The MPI-3.x epoch-closing synchronizations are blocking to guarantee memory 
consistency (among other things). With entirely nonblocking RMA
synchronizations, there are new rules for those guarantees to still hold. The
user is strongly encouraged to read the
[paper](http://post.queensu.ca/~pprl/papers/SC-2014.pdf) of the work to
understdand the new rules and semantics ... because (yes) you can definitely
shoot yourself in the foot otherwise.


# Building

This is a patch to be applied specifically to MVAPICH-2.1.9. I have not been able 
to anonymously checkout MVAPICH-2-1.9 from the NOWLAB repo; so I'm providing
a tarball right here in this folder. To apply the patch and build it, proceed 
**exactly** as follows:

* Assume this current folder is designated as `ROOT_PATH`
* extract mvapich2-1.9.tgz. Let's assume that it is extracted in
  `path_to_vanilla_mvapich`
* `cd patch`
* `./applyPatch -d path_to_vanilla_mvapich -t before\_configure -r`
* `cd path_to_vanilla_mvapich`
* `./configure YOUR_OPTIONS_IF_ANY`
* `cd ROOT_PATH`
* ./applyPatch -d `path_to_vanilla_mvapich` -r
* `cd path_to_vanilla_mvapich`
* `make && make install`  #Assuming you configured with a prefix that is not
  system-wide

 Done!

# New API
The emphasis of this work is on the synchronizations. However, it proved
extremely difficult to approach the synchronizations without rethinking the
communications as well. So RMA-3 has been (almost) entirely reimplemented.
**Almost** because MPI-3 RMA is big; and I had to decide to leave out or 
postpone some of its aspects that are not required to show the benefits of 
nonblocking synchronizations.

## MPI-3.0 API

### Window management
* `MPI_Win_allocate`
* `MPI_Win_free`

### Epoch synchronizations
* `MPI_Win_start`
* `MPI_Win_post`
* `MPI_Win_complete`
* `MPI_Win_wait`
* `MPI_Win_test`
* `MPI_Win_fence`
* `MPI_Win_lock`
* `MPI_Win_unlock`
* `MPI_Win_lock_all`
* `MPI_Win_unlock_all`
* `MPI_Win_flush`
* `MPI_Win_flush_local`
* `MPI_Win_flush_all`
* `MPI_Win_flush_local_all`

### RMA communications
* `MPI_Put`
* `MPI_Get`
* `MPI_Accumulate`
	* Known bug for large payloads
* `MPI_Compare_and_swap`
	* Partially tested
* `MPI_Fetch_and_op`
	* Partially tested

## Newly introduced API and flags
### Nonblocking epoch syncrhonizations
* `MPI_Win_istart`
* `MPI_Win_ipost`
* `MPI_Win_icomplete`
* `MPI_Win_iwait`
* `MPI_Win_ifence`
* `MPI_Win_ilock`
* `MPI_Win_iunlock`
* `MPI_Win_ilock_all
* `MPI_Win_iunlock_all`
* `MPI_Win_iflush`
* `MPI_Win_iflush_local`
* `MPI_Win_iflush_all`
* `MPI_Win_iflush_local_all`

### Aggressive message progression flags
I encourage the user to read Section VI.B in the
[paper](http://post.queensu.ca/~pprl/papers/SC-2014.pdf) 
for a better understanding of these flags. The flags are provided via info
objects. They are:

* `MPI_WIN_ACCESS_AFTER_ACCESS_REORDER`
* `MPI_WIN_ACCESS_AFTER_EXPOSURE_ REORDER`
* `MPI_WIN_EXPOSURE_AFTER_EXPOSURE_REORDER`
* `MPI_WIN_EXPOSURE_AFTER_ACCESS_REORDER`

# Missing aspects
* Derived datatypes are supported; but non-contiguous one are not. If derived
  datatypes are used, they must be contiguous.
* As shown by the API above, `MPI_Win_create` and `MPI_Win_allocate_shared` are
  not implemented.
* `MPI_rput` and `MPI_rget` are not implemented; they are very easy to add
  though.
* MPI RMA functions that not related to window creation/destruction, RMA
  communications or RMA synchronizations are not implemented either. An example
  of such functions is `MPI_Win_get_name`.

# Advice
The nonblocking synchronization API provisions for epoch-opening routines as
well. Those routines are actually nonblocking already (either as per the
specification; or by implementation by most MPI implementations). I provided
them for the sake of completeness. A good advice for the performance freaks is
to prefer the vanilla version of those routines over their ostentatiously
nonblocking counterparts ... because `MPI_Win_post` for instance could be a 
microsecond or two faster than `MPI_Win_ipost` because the later must build and
return a request object.
