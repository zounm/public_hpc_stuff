/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */
/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include "mpiimpl.h"

MPIU_SUPPRESS_OSX_HAS_NO_SYMBOLS_WARNING;

#ifdef USE_MPIU_INSTR

static int MPIU_INSTR_Printf( FILE *fp );
static int MPIU_INSTR_Finalize( void *p );

/* */
/*
 * Basic but general support for instrumentation hooks in MPICH
 *
 * Most actions are handled by MPIU_INSTR_xxx macros (to permit both lowest
 * overhead and to allow instrumentation to be selected at compile time.
 */
static struct MPIU_INSTR_Generic_t *instrHead = 0, *instrTail = 0;

int MPIU_INSTR_AddHandle( void *handlePtr )
{
    struct MPIU_INSTR_Generic_t *gPtr = 
	(struct MPIU_INSTR_Generic_t *)handlePtr;

    /* Note that Addhandle must be within a thread-safe initialization */
    if (!instrHead) {
	/* Make sure that this call back occurs early (before MPID_Finalize) */
	MPIR_Add_finalize( MPIU_INSTR_Finalize, stdout,
			   MPIR_FINALIZE_CALLBACK_PRIO + 2 );
    }

    if (instrHead) {
	instrTail->next = gPtr;
    }
    else {
	instrHead       = gPtr;
    }
    instrTail       = gPtr;
    return 0;
}

#define MAX_INSTR_BUF 1024
static int MPIU_INSTR_Printf( FILE *fp )
{
    struct MPIU_INSTR_Generic_t *gPtr = instrHead;
    char instrBuf[MAX_INSTR_BUF];
    
    while (gPtr) {
	/* We only output information on events that occured */
	if (gPtr->count) {
	    if (gPtr->toStr) {
		(*gPtr->toStr)( instrBuf, sizeof(instrBuf), gPtr );
	    }
	    else {
		if (gPtr->desc) {
		    MPIU_Strncpy( instrBuf, gPtr->desc, sizeof(instrBuf) );
		}
		else {
		    /* This should not happen */
		    MPIU_Strncpy( instrBuf, "", sizeof(instrBuf) );
		}
	    }
	    fputs( instrBuf, fp );
	    fputc( '\n', fp );
	}
	gPtr = gPtr->next;
    }
    fflush( fp );
    return 0;
}

static int MPIU_INSTR_Finalize( void *p )
{
    int rc;
    struct MPIU_INSTR_Generic_t *gPtr = instrHead;
    /* FIXME: This should at least issue the writes in process order */
    /* Allow whether output is generated to be controlled */
    if (!MPL_env2bool( "MPICH_INSTR_AT_FINALIZE", &rc )) 
	rc = 0;

    if (rc) {
	MPIU_INSTR_Printf( stdout );
    }

    /* Free any memory allocated for the descriptions */
    while (gPtr) {
	if (gPtr->desc) {
	    MPIU_Free( (char *)gPtr->desc );
	    gPtr->desc = 0;
	}
	gPtr = gPtr->next;
    }
    
    return 0;
}

/*
 * Standard print routines for the instrumentation objects
 */

/* 
 * Print a duration, which may have extra integer fields.  Those fields
 * are printed as integers, in order, separate by tabs
 */
int MPIU_INSTR_ToStr_Duration_Count( char *buf, size_t maxBuf, void *ptr )
{
    double ttime;
    struct MPIU_INSTR_Duration_count_t *dPtr = 
	(struct MPIU_INSTR_Duration_count_t *)ptr;
    MPID_Wtime_todouble( &dPtr->ttime, &ttime );
    snprintf( buf, maxBuf, "%-40s:\t%d\t%e", dPtr->desc, dPtr->count, ttime );
    if (dPtr->nitems) {
	char *p;
	int  len = strlen(buf);
	int  i;
	/* Add each integer value, separated by a tab. */
	maxBuf -= len;
	p       = buf + len;
	for (i=0; i<dPtr->nitems; i++) {
	    snprintf( p, maxBuf, "\t%d", dPtr->data[i] );
	    len     = strlen(p);
	    maxBuf -= len;
	    p      += len;
	}
    }
    return 0;
}

/* Print the max counter value and the total counter value. */
int MPIU_INSTR_ToStr_Counter( char * buf, size_t maxBuf, void *ptr )
{
    struct MPIU_INSTR_Counter_t *dPtr = 
	(struct MPIU_INSTR_Counter_t *)ptr;
    snprintf( buf, maxBuf, "%-40s:\t%d\t%d", 
	      dPtr->desc, dPtr->maxcount, dPtr->totalcount );
    return 0;
}

#else
/* No routines required if instrumentation is not selected */
#endif


/*JZ_STUFF*/
double JZTimeOrigin=0.0;
int JZ_isInitialized = FALSE;
BOOL JZ_isFinalizedInternal = FALSE;
double __JZ_wtimeTemp1 = 0.0;
double __JZ_wtimeTemp2 = 0.0;
double __JZ_queueOperationTime = 0.0;
uint64_t JZ_wqe_trace_count_when_finalizing = 0;

char traceBuf[1024];

void __JZPrintStackTrace(FILE* stream, char* fileName, int line)
{
    void* callstack[128];
	char hostname[251];
	gethostname(hostname, 250);
    fprintf(stream, "\n\n[JZ][Time: %lf] Process (rank,pid)=(%d,%d) of %s backtracing from %s:%d\n", JZ_TIME(), JZ_RANK, getpid(), hostname, fileName, line);
    int i, nb_frames = backtrace(callstack, 128);
    char** symbols = backtrace_symbols(callstack, nb_frames);
	for(i=0; i< nb_frames; i++)
		fprintf(stream, "%s\n", symbols[i]);
	free(symbols);
	fprintf(stream, "\n\n");
    fflush(stream);
}

void __JZwaitForDebugger(const char* file, int line, int once)
{
	char hostname[251];
	static BOOL firstTime = TRUE;
	if(!once ||  firstTime)					
	{
		gethostname(hostname, 250);
		printf("The program (rank, pid)=(%d, %d) is suspended on %s at %s:%d for debugging. Please attach a debugger to continue\n",
			JZ_RANK, getpid(), hostname, file, line);
		fflush(stdout);
		if(once)
			firstTime=0;
		for(;;);		
	}	
}

void __JZtimedWaitForDebugger(double time, const char* file, int line, BOOL once)
{
	char _hostname[251];
	static BOOL firstTime=TRUE;
	if(!once ||  firstTime)					
	{
		firstTime=FALSE;
		gethostname(_hostname, 250);
		printf("The program (pid = %d) is suspended on %s at %s:%d for debugging. Please attach a debugger to continue\n",
			getpid(), _hostname, file, line);
		fflush(stdout);
		JZ_DECLARE_TIME_VAR(start);
		JZ_DECLARE_TIME_VAR(end);
		JZ_STAMP_TIME_VAR(start);
		JZ_STAMP_TIME_VAR(end);
		double wait_time = time*MILLION;
		while(JZ_GET_DURATION_US_VAR(start,end)<wait_time)
			JZ_STAMP_TIME_VAR(end);
	}
}

JZ_bitmask_t	__JZTraceMask = 0;
JZ_bitmask_t	__JZMask = 0;
void __JZSetMask(JZ_bitmask_t* mask, JZ_bitmask_t maskPos, int activate)
{
	if(activate)
		(*mask)|=maskPos;
	else
		(*mask)&=(~maskPos);
}

FILE* JZ_traceFile = NULL;
char JZ_traceFilename[201];


void __JZ_open_trace_file()
{
    char hostname[50];
    gethostname(hostname, 50);

    sprintf(JZ_traceFilename, "jz_trace_host-%s_pid-%d", hostname, getpid());
    JZ_traceFile = fopen(JZ_traceFilename, "w");
}

void __JZ_update_file_name()
{
	char temp[201];
    char hostname[50];
    gethostname(hostname, 50);
    fclose(JZ_traceFile);
    sprintf(temp, "jz_trace_host-%s_pid-%d_rank-%d", hostname, getpid(), MPIR_Process.comm_world->rank);
    rename(JZ_traceFilename, temp);
	JZ_traceFile = fopen(temp, "a");
	strcpy(JZ_traceFilename, temp);
}

void __JZ_close_trace_file()
{
    /*char temp[201];
    char hostname[50];
    gethostname(hostname, 50);*/
    fclose(JZ_traceFile);
    /*sprintf(temp, "jz_trace_host-%s_rank-%d", hostname, MPIR_Process.comm_world->rank);
    rename(JZ_traceFilename, temp);*/
}

#define NB_MAX_VBUF	500

int JZ_vbuf_array_count = 0;
vbuf* JZ_vbuf_array[NB_MAX_VBUF];

void __JZ_push_vbuf(vbuf* vb)
{
	JZ_WAIT_FOR_DEBUGGER_IF(!vb->vc);
	JZ_vbuf_array[JZ_vbuf_array_count++] = vb;
}

void __jz_remove_vbuf(vbuf* vb)
{
	int i;
	BOOL found = FALSE;
	for(i=0; i<JZ_vbuf_array_count; i++)
	{
		if(JZ_vbuf_array[i] == vb)
		{
			JZ_vbuf_array[i] = NULL;
			if(i != JZ_vbuf_array_count-1)
				JZ_vbuf_array[i] = JZ_vbuf_array[JZ_vbuf_array_count-1];
			JZ_vbuf_array_count--;
			found =TRUE;
			break;
		}
	}
	JZ_WAIT_FOR_DEBUGGER_IF(!found);
	JZ_WAIT_FOR_DEBUGGER_IF(!vb->vc);
}

BOOL JZ_wc_equal(const struct ibv_wc* wc0, const struct ibv_wc* wc1)
{
	return wc0->wr_id == wc1->wr_id && wc0->status ==  wc1->status && wc0->opcode == wc1->opcode && wc0->vendor_err == wc1->vendor_err
		&& wc0->byte_len == wc1->byte_len && wc0->imm_data == wc1->imm_data && wc0->qp_num == wc1->qp_num  && wc0->src_qp == wc1->src_qp
		&& wc0->wc_flags == wc1->wc_flags && wc0->pkey_index == wc1->pkey_index && wc0->slid == wc1->slid && wc0->sl == wc1->sl
		&& wc0->dlid_path_bits == wc1->dlid_path_bits;
}



#define JZ_THRESHOLD_1_FOR_WQES_WHILE_FINALIZING    0
#define JZ_THRESHOLD_2_FOR_WQES_WHILE_FINALIZING    100
void __JZTraceWqesWhileFinalizing(struct MPIDI_VC* vc, int remote_rank, int nb_wqes)
{
	if(!JZ_isFinalizedInternal)
		return;
	++JZ_wqe_trace_count_when_finalizing;
	if(JZ_wqe_trace_count_when_finalizing>JZ_THRESHOLD_1_FOR_WQES_WHILE_FINALIZING && 
		JZ_wqe_trace_count_when_finalizing<JZ_THRESHOLD_2_FOR_WQES_WHILE_FINALIZING)
		JZ_DEEP_TRACE("JZ_TRACE_WQES_WHILE_FINALIZING: {remote_rank=%d, vc=%p, send_wqes=%d}\n", remote_rank, vc, nb_wqes); 
}
