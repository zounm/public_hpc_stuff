#include "mpiimpl.h"
#include "rdma_impl.h"
#include "dreg.h"

#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */

//#define NO_CUSTOM_ALLOCATOR
//#define DEBUG_ALLOCATOR

#ifdef REGION_MISC
typedef struct node_link_t
{
	struct node_link_t* next;
}node_link_t;

typedef struct d_node_link_t
{
	struct d_node_link_t* next;
	struct d_node_link_t* prev;
}d_node_link_t;


void d_add_node(void** node_head, void** node_tail, void* node_cur)
{
	d_node_link_t** head = (d_node_link_t**)(node_head);
	d_node_link_t** tail = (d_node_link_t**)(node_tail);
	d_node_link_t* node = (d_node_link_t*)(node_cur);
	if(! (*head) )
	{
		(*head) = node;
		node->next = NULL;
		node->prev = NULL;
	}
	else
	{
		node->prev = (*tail);
		(*tail)->next = node;
		node->next = NULL;
	}
	(*tail) = node;
}

void d_remove_node(void** node_head, void** node_tail, void* node_cur)
{
	d_node_link_t** head = (d_node_link_t**)(node_head);
	d_node_link_t** tail = (d_node_link_t**)(node_tail);
	d_node_link_t* node = (d_node_link_t*)(node_cur);
	if(node->prev)
		node->prev->next = node->next;
	if(node->next)
		node->next->prev = node->prev;
	if(node == *head)
		(*head) = node->next;
	if(node == *tail)
		(*tail) = node->prev;
	node->prev = node->next = NULL;
}

void d_append_node(void** node_head, void** node_tail, void* append_head, void* append_tail)
{
	d_node_link_t** head = (d_node_link_t**)(node_head);
	d_node_link_t** tail = (d_node_link_t**)(node_tail);
	d_node_link_t* nhead = (d_node_link_t*)(append_head);
	d_node_link_t* ntail = (d_node_link_t*)(append_tail);
	if(! (*head) )
	{
		(*head) = nhead;
		nhead->prev = NULL; /*double safety; it is supposed to be this way
						before entering the function*/
	}
	else
	{
		(*tail)->next = nhead;
		nhead->prev = (*tail);
	}
	(*tail) = ntail;
	ntail->next = NULL;  /*double safety; it is supposed to be this way
					 before entering the function*/
}
void add_node(void** node_head, void** node_tail, void* node)
{
	node_link_t **head = (node_link_t**)(node_head);
	node_link_t **tail = (node_link_t**)(node_tail);
	if(*tail)
	{
		(*tail)->next = (node_link_t*)node;
	}
	else
	{
		*head = (node_link_t*)node;
	}
	*tail = (node_link_t*)node;
}

void add_node_to_front(void** node_head, void** node_tail, void* node)
{
	node_link_t **head = (node_link_t**)(node_head);
	node_link_t **tail = (node_link_t**)(node_tail);
    node_link_t* the_node = (node_link_t*)node;
    if(*head == NULL)
    {
        *head = *tail = the_node;
    }
    else
    {
        the_node->next = *head;
        *head = the_node;
    }
}

void remove_node(void** node_head, void** node_tail, void* node)
{
	node_link_t *cur = (node_link_t*)(*node_head), *previous = NULL;
	while(cur)
	{
		if(cur == node)
		{
			if(previous)
				previous->next = cur->next;
			else
				*node_head = cur->next;
			if(*node_tail == cur)
				*node_tail = previous;
			cur->next = NULL;
			break;
		}
		previous = cur;
		cur = cur->next;
	}
}

void* find_node(void* node_head, node_predicate_t predicate, void* predicate_arg)
{
	node_link_t *cur = (node_link_t*)(node_head);
	while(cur)
	{
		if(predicate((void*)cur, predicate_arg))
			return cur;
		cur = cur->next;
	}
	return NULL;
}

void add_req(MPID_Request** head, MPID_Request** tail, MPID_Request* node)
{
	if(*tail)
		(*tail)->JZ_next = node;
	else
		*head = node;
	*tail = node;
}

void remove_req(MPID_Request** head, MPID_Request** tail, MPID_Request* node)
{
	MPID_Request *cur = *head, *previous = NULL;
	while(cur)
	{
		if(cur == node)
		{
			if(previous)
				previous->JZ_next = cur->JZ_next;
			else
				*head = cur->JZ_next;
			if(*tail == cur)
				*tail = previous;
			cur->JZ_next = NULL;
			break;
		}
		previous = cur;
		cur = cur->JZ_next;
	}
}

#endif //REGION_MISC

#ifdef REGION_BUFFER_POOL


#define UNKNOWN_SIZE_OBJECT 


typedef struct JZ_object_pool_t
{
    long nb_free;
    struct JZ_object_pool_t* next;
#ifdef DEBUG_ALLOCATOR
	int num;
#endif
	VARIABLE_SIZE_DATA object_array[JZ_UNDEFINED_SIZE];
      //void* array_of_objects;

}JZ_object_pool_t;
 
 
typedef struct JZ_pool_head_t
{
    unsigned int sizeof_useful_content;
    unsigned int outer_object_size;
	int allocation_granularity;
    JZ_object_pool_t *first;

#ifdef DEBUG_ALLOCATOR
	int nb_pools;
#endif
}JZ_pool_head_t;

//the allocation unit always return an object with the layout
typedef struct obj_t
{
    JZ_object_pool_t* pool;   //manipulated with size_t
    size_t is_unavailable;
    char obj_content[JZ_UNDEFINED_SIZE];
}obj_t;
//Then obj_t can be casted to obj_content to be used. The goal is to allocate object right as link nodes to avoid separate mallocs

JZ_pool_head_t* create_pool_head(int item_size, int allocation_granularity)
{
	JZ_pool_head_t* head = (JZ_pool_head_t*)malloc(sizeof(JZ_pool_head_t));
	head->first = NULL;
	
	//force pointer-boundary alignment
	head->sizeof_useful_content = (item_size/sizeof(size_t))*sizeof(size_t);  
	if(head->sizeof_useful_content < item_size)
		head->sizeof_useful_content += sizeof(size_t);

	head->outer_object_size = head->sizeof_useful_content + sizeof(void*) + sizeof(size_t);
	head->allocation_granularity = allocation_granularity;

#ifdef DEBUG_ALLOCATOR
	head->nb_pools = 0;
#endif
	return head;
}
 
static JZ_object_pool_t* allocate_pool(JZ_pool_head_t* head)
{
    int pool_size = head->outer_object_size * head->allocation_granularity + sizeof(JZ_object_pool_t); 
    JZ_object_pool_t *unit = (JZ_object_pool_t*)malloc(pool_size);
	unit->next = NULL;

#ifdef DEBUG_ALLOCATOR
	unit->num = head->nb_pools++; 
#endif
    return unit;
}
 
#define	ITEM_TO_OBJ_T(item) ((obj_t*)((size_t)(item) - sizeof(JZ_object_pool_t*) - sizeof(size_t)))
#define	OBJ_T_TO_ITEM(obj) (void*)((size_t)(obj) + sizeof(JZ_object_pool_t*) + sizeof(size_t))

#define	POOL_OBJ_ARRAY(pool) (void*)((size_t)(pool) + sizeof(JZ_object_pool_t))
 
JZ_object_pool_t* JZ_some_UOA_head = NULL;
 
#ifdef DEBUG_ALLOCATOR

//JZ_OBJECT_ALLOCATION_MASK
static void DEBUG_ONLY_DECLARATION trace_allocators(JZ_pool_head_t* head, obj_t* obj, 
	int position_in_pool, const char* msg, int line_number)
{
	if(position_in_pool>=0)
	{
		JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK,
			"%s: obj = {size = %d, obj_content=%p, pool=%p, is_available=%s, pool_number=%d, position_in_pool=%d}[INVOKED FROM LINE %d]\n",
			msg, head->sizeof_useful_content, obj->obj_content, obj->pool,
				 obj->is_unavailable?"FALSE":"TRUE", obj->pool->num, position_in_pool, line_number);
	}
	else
	{
		JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK,
			"%s: obj = {size = %d, obj_content=%p, pool=%p, is_available=%s, pool_number=%d}[INVOKED FROM LINE %d]\n",
			msg, head->sizeof_useful_content, obj->obj_content, obj->pool,
				 obj->is_unavailable?"FALSE":"TRUE", obj->pool->num, line_number);
	}

}
#else
#define trace_allocators(...)
#endif 

void* allocate_object(JZ_pool_head_t* head)
{
	//JZ_WAIT_ONCE_FOR_DEBUGGER_IF(JZ_RANK==2);
    JZ_object_pool_t *pool = head->first;
    obj_t* obj = NULL;
    int item_size = head->sizeof_useful_content;
    int outer_object_size = head->outer_object_size;
    int next_object_offset = 0;
	JZ_object_pool_t *new_pool;
	int inner_object_size = head->sizeof_useful_content;
	int allocation_granularity = head->allocation_granularity;
    int i;
 
#ifdef NO_CUSTOM_ALLOCATOR
	return calloc(item_size, 1);
#endif

    if(pool)
    {
		next_object_offset = 0;
        while(1)
        {
			JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK,"\n\nTrying to consume from pool %p ...\n", pool); 
            if(pool->nb_free>0)
            {
                int i;
                for(i=0; i < allocation_granularity; i++)
                {
                    obj = (obj_t*)(&pool->object_array[next_object_offset]);
                    {
                        --pool->nb_free;
						obj->is_unavailable = TRUE;
                        memset(obj->obj_content, 0, item_size);
	
#ifdef DEBUG_ALLOCATOR
						trace_allocators(head, obj, i, "Just allocated", __LINE__);
#endif
						JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK, "Done trying to consume from  pool %p ...\n\n", pool);
                        return (void*)obj->obj_content;
                    }
                    next_object_offset += outer_object_size;
                }
            }
			JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK, "Done trying to consume from  pool %p ...\n\n", pool);
            if(!pool->next)
                break;
            pool = pool->next;
			next_object_offset = 0;
        }
    }
	JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK, "Done trying to consume from  existing pools ...\n\n");
	//JZ_WAIT_FOR_DEBUGGER_IF(pool);
 
    new_pool = allocate_pool(head);
    if(!head->first)
        head->first = new_pool;
    else 
    {
        pool->next = new_pool;
    }
    pool = new_pool;
    next_object_offset = 0;
	JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK,"\n\nSetting up objs for pool %p ...\n", pool);
    for(i = 0; i < allocation_granularity; i++)
    {
		obj = (obj_t*)(&pool->object_array[next_object_offset]);
		obj->pool = pool;
		obj->is_unavailable = FALSE;
		next_object_offset += outer_object_size;
    }
	JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_OBJECT_ALLOCATION_MASK, "Done setting up objs for pool %p ...\n\n", pool);
	obj = (obj_t*)(&pool->object_array[0]);
	pool->nb_free = allocation_granularity - 1;
	obj->is_unavailable = TRUE;
	memset(obj->obj_content, 0, item_size);

	return (void*)obj->obj_content;
}
 

void free_object(JZ_pool_head_t* head, void* object)
{
#ifdef NO_CUSTOM_ALLOCATOR
	free(object);
	return;
#endif
	obj_t* obj = ITEM_TO_OBJ_T(object);
#ifdef DEBUG_ALLOCATOR
	trace_allocators(head, obj, -1, "About to be freed", __LINE__);
#endif
	obj->is_unavailable = FALSE;
	++obj->pool->nb_free;

	/*For now (and for simplicity), the pool number does not shrink to adapt to freed objects*/

	return;
}

#endif //REGION_BUFFER_POOL

#ifdef REGION_PINNED_BUFFER

JZ_pinned_pool_head_t pinned_pool_head = {NULL, NULL, 0};


#if 1
#define PIN_POOL(_pool, size) do{(_pool).dreg = dreg_register((_pool).pool_buf, (size));} while(0)
#define UNPIN_POOL(pool) dreg_unregister((pool).dreg)
#else
#define PIN_POOL(pool, size)
#define UNPIN_POOL(pool)
#endif

static inline uint32_t align(uint32_t size)
{
	uint32_t mod = (size % sizeof(void*));
	return size + (sizeof(void*) - mod);
}

void get_pinned_buffer(uint32_t size, JZ_pinned_buf_t** _pinned_buf)
{
	JZ_DEEP_TRACE_IF(JZ_isFinalizedInternal, "Getting pinned buf after all release\n");
	JZ_pinned_buf_t* pinned_buf = NULL;
	//JZ_WAIT_ONCE_FOR_DEBUGGER_IF(JZ_RANK == 0);
	size = align(size + sizeof(JZ_pinned_buf_t));
	uint32_t allocation_size = PINNED_POOL_ALLOCATION_GRANULARITY;
	JZ_pinned_pool_t* pool = pinned_pool_head.first;
	while(pool)
	{
		if(pool->size - pool->start_of_free_space >= size)
			goto get_buf;
		pool = pool->next;
	}

	while(allocation_size < size)
		allocation_size += PINNED_POOL_ALLOCATION_GRANULARITY;		

	if(posix_memalign((void**)(&pool), sizeof(void*), sizeof(JZ_pinned_pool_t) + allocation_size) != 0 )
	{
		JZ_FATAL("posix_memalign failed! strerror(errno) is %s\n", strerror(errno));
	}
	memset(pool, 0, sizeof(JZ_pinned_pool_t));
	pinned_pool_head.nb_pools++;
	pool->size = pool->free_size = allocation_size;		 
	PIN_POOL(*pool, allocation_size);

	if(!pinned_pool_head.last)
	{
		pinned_pool_head.last = pinned_pool_head.first = pool;
	}
	else
	{
		pinned_pool_head.last->next = pool;
		pinned_pool_head.last = pool;
	}
get_buf:
	pinned_buf = (JZ_pinned_buf_t*)(&pool->pool_buf[pool->start_of_free_space]);	
	pinned_buf->next = NULL;
	pinned_buf->buf = (void*)((uintptr_t)pinned_buf + sizeof(JZ_pinned_buf_t));
	pinned_buf->dreg = pool->dreg;
	pinned_buf->originating_pool = pool;
	pinned_buf->size = size;

	pool->start_of_free_space += size;
	pool->free_size -= size;

    JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_PINNED_BUF_MASK, 
		"Getting pinned_buf (%p) = {buf=%p, size=%d, dreg=%p, originating_pool=%p}\n",
        pinned_buf, pinned_buf->buf, pinned_buf->size, pinned_buf->dreg, pinned_buf->originating_pool);
	*_pinned_buf = pinned_buf;
}

void free_pinned_buffer(JZ_pinned_buf_t* pinned_buf)
{
    JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_PINNED_BUF_MASK, 
		"Freeing pinned_buf (%p) = {buf=%p, size=%d, dreg=%p, originating_pool=%p}\n",
        pinned_buf, pinned_buf->buf, pinned_buf->size, pinned_buf->dreg, pinned_buf->originating_pool);
	JZ_pinned_pool_t* pool = pinned_buf->originating_pool;
	pool->free_size += pinned_buf->size;
	//memset(pinned_buf, 0, sizeof(JZ_pinned_buf_t));
	if(pool->free_size == pool->size)
		pool->start_of_free_space = 0;
}

void reset_pinned_buffer_pool()
{
	//NOOP
}

void release_pinned_buffer_pool()
{
	JZ_pinned_pool_t* pool = pinned_pool_head.first;
	JZ_pinned_pool_t* next;

#if 0
	g_i = 0;
	g_j = 0;
	g_nb_pool = 0;
	g_nb_pools_on_free_list = 0;
	g_pool = pinned_pool_head.first;
	while(g_pool)
	{
		g_nb_pool++;
		g_pool = g_pool->next;
	}

	g_pool = pinned_pool_head.free_list_head;
    while(g_pool)
    {   
        g_nb_pools_on_free_list++;
        g_pool = g_pool->next;
    }
#endif
	
	while(pool)
	{
		next = pool->next;
		UNPIN_POOL(*pool);
		free(pool);
		pool = next;
	}
	pinned_pool_head.last = pinned_pool_head.first = NULL;
	pinned_pool_head.nb_pools = 0;
}
#endif //REGION_PINNED_BUFFER


#ifdef REGION_EXTENDED_CQ
struct JZ_pool_head_t* JZ_wc_pool_head = NULL;
JZ_extended_completionQ_t* JZ_regular_extended_wc_queue_head = NULL;
JZ_extended_completionQ_t* JZ_RMA_extended_wc_queue_head = NULL;

void JZ_allocate_extended_wc_queues_if_required()
{
	if(!JZ_regular_extended_wc_queue_head)  //both queues are NUUL or not NULL at the same time, we can thus check any one of both
	{
		JZ_regular_extended_wc_queue_head = calloc(1, sizeof(JZ_extended_completionQ_t));
		JZ_RMA_extended_wc_queue_head = calloc(1, sizeof(JZ_extended_completionQ_t));
	}
}


void JZ_enqueue_wc(JZ_extended_completionQ_t* wc_queue, const struct ibv_wc* const wc)  //wc is the work completion returned by ibv_poll_cq; it is copied by this routine
{
	JZ_ibv_wc_item_t* wc_item = (JZ_ibv_wc_item_t*)malloc(sizeof(JZ_ibv_wc_item_t));
	wc_item->next = NULL;
	memcpy(&wc_item->wc, wc, sizeof(struct ibv_wc));
	if(wc_queue->tail)
	{
		wc_queue->tail->next = wc_item;
		wc_queue->tail = wc_item;
	}
	else
	{
		wc_queue->head = wc_queue->tail = wc_item;
	}
}

JZ_ibv_wc_item_t* JZ_dequeue_wc(JZ_extended_completionQ_t* wc_queue) //return null if nothing to consume
{
	JZ_ibv_wc_item_t* ret = NULL;
	if(wc_queue->head)
	{
		ret = wc_queue->head;
		wc_queue->head = wc_queue->head->next;
		if(wc_queue->tail == ret)
			wc_queue->tail = NULL;
	}
	return ret;
}

#endif //REGION_EXTENDED_CQ

/* Copyright (c) 2001-2013, The Ohio State University. All rights
 * reserved.
 *
 * This file is part of the MVAPICH2 software package developed by the
 * team members of The Ohio State University's Network-Based Computing
 * Laboratory (NBCL), headed by Professor Dhabaleswar K. (DK) Panda.
 *
 * For detailed copyright and licensing information, please refer to the
 * copyright file COPYRIGHT in the top level MVAPICH2 directory.
 *
 */

#include <mpichconf.h>

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <malloc.h>
#include <netinet/in.h>
#include <byteswap.h>
#include <inttypes.h>

#include <infiniband/verbs.h>

char *u_ibv_wr_opcode_string(int opcode)
{
    switch (opcode) {
    case IBV_WR_RDMA_WRITE:
        return "IBV_WR_RDMA_WRITE";
    case IBV_WR_RDMA_WRITE_WITH_IMM:
        return "IBV_WR_RDMA_WRITE_WITH_IMM";
    case IBV_WR_SEND:
        return "IBV_WR_SEND";
    case IBV_WR_SEND_WITH_IMM:
        return "IBV_WR_SEND_WITH_IMM";
    case IBV_WR_RDMA_READ:
        return "IBV_WR_RDMA_READ";
    case IBV_WR_ATOMIC_CMP_AND_SWP:
        return "IBV_WR_ATOMIC_CMP_AND_SWP";
    case IBV_WR_ATOMIC_FETCH_AND_ADD:
        return "IBV_WR_ATOMIC_FETCH_AND_ADD";
    default:
        return "Unknown-wr-opcode";
    }

}

char *u_ibv_wc_opcode_string(int opcode)
{
    switch (opcode) {
    case IBV_WC_SEND:
        return "IBV_WC_SEND";
    case IBV_WC_RDMA_WRITE:
        return "IBV_WC_RDMA_WRITE";
    case IBV_WC_RDMA_READ:
        return "IBV_WC_RDMA_READ";
    case IBV_WC_COMP_SWAP:
        return "IBV_WC_COMP_SWAP";
    case IBV_WC_FETCH_ADD:
        return "IBV_WC_FETCH_ADD";
    case IBV_WC_BIND_MW:
        return "IBV_WC_BIND_MW";

        //// recv-side: inbound completion
    case IBV_WC_RECV:
        return "IBV_WC_RECV";
    case IBV_WC_RECV_RDMA_WITH_IMM:
        return "IBV_WC_RECV_RDMA_WITH_IMM";
    default:
        return "Unknow-wc-opcode";
    }
}

const char *u_ibv_mtu_string(enum ibv_mtu mtu)
{
    switch (mtu) {
    case IBV_MTU_256:
        return "IBV_MTU_256";
    case IBV_MTU_512:
        return "IBV_MTU_512";
    case IBV_MTU_1024:
        return "IBV_MTU_1024";
    case IBV_MTU_2048:
        return "IBV_MTU_2048";
    case IBV_MTU_4096:
        return "IBV_MTU_4096";
    default:
        return "unknown mut";
    }
/* 147 enum ibv_mtu {
 148     IBV_MTU_256  = 1,
 149     IBV_MTU_512  = 2,
 150     IBV_MTU_1024 = 3,
 151     IBV_MTU_2048 = 4,
 152     IBV_MTU_4096 = 5
 153 };    */
}

const char *u_ibv_port_state_string(enum ibv_port_state state)
{
    return ibv_port_state_str(state);

    switch (state) {
    case IBV_PORT_NOP:
        return "IBV_PORT_NOP";
    case IBV_PORT_DOWN:
        return "IBV_PORT_DOWN";
    case IBV_PORT_INIT:
        return "IBV_PORT_INIT";
    case IBV_PORT_ARMED:
        return "IBV_PORT_ARMED";
    case IBV_PORT_ACTIVE:
        return "IBV_PORT_ACTIVE";
    case IBV_PORT_ACTIVE_DEFER:
        return "IBV_PORT_ACTIVE_DEFER";
    default:
        return "unknown port state";
    }
}

const char *u_ibv_port_phy_state_string(uint8_t phys_state)
{
    switch (phys_state) {
    case 1:
        return "SLEEP";
    case 2:
        return "POLLING";
    case 3:
        return "DISABLED";
    case 4:
        return "PORT_CONFIGURATION TRAINNING";
    case 5:
        return "LINK_UP";
    case 6:
        return "LINK_ERROR_RECOVERY";
    case 7:
        return "PHY TEST";
    default:
        return "invalid physical state";
    }
}

const char *u_ibv_atomic_cap_string(enum ibv_atomic_cap atom_cap)
{
    switch (atom_cap) {
    case IBV_ATOMIC_NONE:
        return "ATOMIC_NONE";
    case IBV_ATOMIC_HCA:
        return "ATOMIC_HCA";
    case IBV_ATOMIC_GLOB:
        return "ATOMIC_GLOB";
    default:
        return "invalid atomic capability";
    }
}

const char *u_ibv_width_string(uint8_t width)
{
    switch (width) {
    case 1:
        return "1";
    case 2:
        return "4";
    case 4:
        return "8";
    case 8:
        return "12";
    default:
        return "invalid width";
    }
}

const char *u_ibv_speed_string(uint8_t speed)
{
    switch (speed) {
    case 1:
        return "2.5 Gbps";
    case 2:
        return "5.0 Gbps";
    case 4:
        return "10.0 Gbps";
    default:
        return "invalid speed";
    }
}

const char *u_ibv_vl_string(uint8_t vl_num)
{
    switch (vl_num) {
    case 1:
        return "1";
    case 2:
        return "2";
    case 3:
        return "4";
    case 4:
        return "8";
    case 5:
        return "15";
    default:
        return "invalid value";
    }
}

const char *u_ibv_wc_status_string(int status)
{
    return ibv_wc_status_str(status);
}

void u_dump_wc(struct ibv_wc *wc)
{
    printf("CQ::  wr_id=0x%lx, wc_opcode=%s, wc_status=%s, wc_flag=0x%x\n", wc->wr_id, 
		u_ibv_wc_opcode_string(wc->opcode), u_ibv_wc_status_string(wc->status), wc->wc_flags);
    printf("      byte_len=%u, immdata=%u, qp_num=0x%x, src_qp=%u\n", 
		wc->byte_len, wc->imm_data, wc->qp_num, wc->src_qp);
}

void u_dump_send_wr(struct ibv_send_wr *wr)
{
    printf("SQ WR::  wr_id=0x%lx, opcode=%s, num_sge=%d, send_flags=%u, immdata=%u\n", wr->wr_id, 
		u_ibv_wr_opcode_string(wr->opcode), wr->num_sge, wr->send_flags, wr->imm_data);

    if (wr->opcode == IBV_WR_RDMA_WRITE) {

    } else if (wr->opcode == IBV_WR_RDMA_WRITE_WITH_IMM) {

    } else if (wr->opcode == IBV_WR_RDMA_READ) {

    } else if (wr->opcode == IBV_WR_ATOMIC_CMP_AND_SWP) {

    } else if (wr->opcode == IBV_WR_ATOMIC_FETCH_AND_ADD) {

    }

}

void u_dump_ibv_device_attr(struct ibv_device_attr *attr)
{
    printf("\n==== device attr ====\n");
    printf("\tfw_ver: %s\n", attr->fw_ver);
    printf("\tnode_guid: 0x%lx\n", attr->node_guid);
    printf("\tsys_image_guid: 0x%lx\n", attr->sys_image_guid);
    printf("\tmax_mr_size: 0x%lx\n", attr->max_mr_size);    /* Largest contiguous block that can be registered */
    printf("\tpage_size_cap: %lu\n", attr->page_size_cap);  /* Supported memory shift sizes */
    printf("\thw_ver:  %u\n", attr->hw_ver);
    printf("\tmax_qp:  %d\n", attr->max_qp);
    printf("\tmax_qp_wr:  %d\n", attr->max_qp_wr);
    printf("\tmax_sge:  %d\n", attr->max_sge);
    printf("\tmax_cq:  %d\n", attr->max_cq);
    printf("\tmax_cqe:  %d\n", attr->max_cqe);
    printf("\tmax_mr:  %d\n", attr->max_mr);
    printf("\tmax_pd:  %d\n", attr->max_pd);
    printf("\tmax_qp_rd_atom:  %d\n", attr->max_qp_rd_atom);    /* Maximum number of RDMA Read & Atomic operations that can be outstanding per QP */
    printf("\tmax_res_rd_atom:  %d\n", attr->max_res_rd_atom);  /* Maximum number of resources used for RDMA Read & Atomic operations by this HCA as the Target */
    printf("\tmax_qp_init_rd_atom:  %d\n", attr->max_qp_init_rd_atom);  /* Maximum depth per QP for initiation of RDMA Read & Atomic operations */
    printf("\tphys_port_cnt:  %d\n", attr->phys_port_cnt);
    printf("\t\n");
    printf("==============\n");
    //////////

}

void u_dump_ibv_port_attr(struct ibv_port_attr *attr)
{
    printf("\n==========  port attr ==========\n");
    printf("\tstate: %d  (%s)\n", attr->state, u_ibv_port_state_string(attr->state));
    printf("\tmax_mtu:  %s\n", u_ibv_mtu_string(attr->max_mtu));
    printf("\tactive_mtu:  %s\n", u_ibv_mtu_string(attr->active_mtu));
    printf("\tgid_tbl_len:  %d\n", attr->gid_tbl_len);  /* Length of source GID table */
    printf("\tport_cap_flags:  0x %x\n", attr->port_cap_flags);
    printf("\tmax_msg_sz:  %u\n", attr->max_msg_sz);
    printf("\tlid:  %d\n", attr->lid);  /* Base port LID */
    printf("\tsm_lid:  %d\n", attr->sm_lid);
    printf("\tlmc:  %d\n", attr->lmc);  /* LMC of LID */
    printf("\tmax_vl_num:  %d (%s)\n", attr->max_vl_num, u_ibv_vl_string(attr->max_vl_num));  /* Maximum number of VLs */
    printf("\tsm_sl:  %d\n", attr->sm_sl);  /* SM service level */
    printf("\tactive_width:  %d (%s)\n", attr->active_width, u_ibv_width_string(attr->active_width)); /* Currently active link width */
    printf("\tactive_speed:  %d (%s)\n", attr->active_speed, u_ibv_speed_string(attr->active_speed));
    printf("\tphys_state:  %d (%s)\n", attr->phys_state, u_ibv_port_phy_state_string(attr->phys_state));
    printf("==============\n");

}

double u_tv2sec(struct timeval *start, struct timeval *end)
{
    int us = (int) (end->tv_usec - start->tv_usec);

    double sec = (end->tv_sec - start->tv_sec);

    return sec + us / 1000000.0;

}

const char *u_int_to_binary(int x)
{
    static char b[32] = { 0 };

    int z;
    b[0] = 0;
    for (z = 31; z >= 0; z--) {
        strcat(b, (x & (1 << z)) ? "1" : "0");
    }

    return b;
}

char str_wc[501];
char* JZ_get_str_wc(struct ibv_wc* wc) 
{
    sprintf(str_wc,
        "wc = {wr_id = %p, status = %s, opcode = %s, vendor_err = %u, byte_len = %u, imm_data = 0x%X, qp_num = %u, src_qp = %u, wc_flags = %d, pkey_index = %hu, slid = %hu, sl = %d, dlid_path_bits = %d}",
        wc->wr_id, u_ibv_wc_status_string(wc->status), 
		u_ibv_wc_opcode_string(wc->opcode), wc->vendor_err, wc->byte_len, wc->imm_data, wc->qp_num, wc->src_qp, wc->wc_flags, wc->pkey_index, wc->slid, (int)wc->sl, (int)wc->dlid_path_bits);
    return str_wc;
}


char str_op_code[151];
const char* JZ_str_rma_op_code(uint64_t op_code)
{
	uint64_t lower_48 = op_code&JZ_MASK_ADDRESS;
	switch(op_code&JZ_MASK_RMA_WORK)
	{
		case JZ_NOT_AN_RMA_WORK:
			sprintf(str_op_code, "%s|0X%016X", "JZ_NOT_AN_RMA_WORK", lower_48); break;
		case JZ_RMA_WORK_LOCK_GRANT_STATUS:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_LOCK_GRANT_STATUS", lower_48); break;
		case JZ_RMA_WORK_EPOCH_POST_STATUS:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_EPOCH_POST_STATUS", lower_48); break;
		case JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS", lower_48); break;
		case JZ_RMA_WORK_PUT:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_PUT", lower_48); break;
		case JZ_RMA_WORK_GET:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_GET", lower_48); break;
		case JZ_RMA_WORK_BUF_REQUEST:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_BUF_REQUEST", lower_48); break;
		case JZ_RMA_WORK_BUF_RESPONSE:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_BUF_RESPONSE", lower_48); break;
		case JZ_RMA_WORK_1ST_OF_2_STEPS_ACC_DATA_ONLY:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_1ST_OF_2_STEPS_ACC_DATA_ONLY", lower_48); break;
		case JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY", lower_48); break;
		case JZ_RMA_WORK_SINGLE_STEP_ACC:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_SINGLE_STEP_ACC", lower_48); break;
		case JZ_RMA_WORK_ACC:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_ACC", lower_48); break;
		case JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST", lower_48); break;
		case JZ_RMA_WORK_SHARED_LOCK_REQUEST:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_SHARED_LOCK_REQUEST", lower_48); break;
		case JZ_RMA_WORK_LOCK_RELEASE:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_LOCK_RELEASE", lower_48); break;
		case JZ_RMA_WORK_WAITING_FOR_SHM_LOCK:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_WORK_WAITING_FOR_SHM_LOCK", lower_48); break;
		case JZ_RMA_DEBUG_WORK:
			sprintf(str_op_code, "%s|0X%016X", "JZ_RMA_DEBUG_WORK", lower_48); break;
		case JZ_IBV_SEND_SIDE_UNSPECIFIED_WORK_TYPE:
			sprintf(str_op_code, "%s|0X%016X", "JZ_IBV_SEND_SIDE_UNSPECIFIED_WORK_TYPE", lower_48); break;
		default:
			sprintf(str_op_code, "%s|0X%016X", "UNKNOWN", lower_48);
	}
	return str_op_code;
}

char str_fast_notif[251];
const char* JZ_str_fast_notif(JZ_fast_notification_t* fast_notif)
{
	sprintf(str_fast_notif, "fast_notif = {\n"
		"dest_win_ptr = %p,\n"
		" notification = %s,\n"
		"exposure_id = %u,\n"
		"src_rank = %d }\n", 
		fast_notif->dest_win_ptr, 
		JZ_str_rma_op_code(fast_notif->notification),
		fast_notif->exposure_id,
		fast_notif->src_rank);
	return str_fast_notif;
}
