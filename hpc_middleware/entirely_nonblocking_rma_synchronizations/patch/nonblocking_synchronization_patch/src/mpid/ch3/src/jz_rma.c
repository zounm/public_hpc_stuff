#include "mpidimpl.h"
#include "mpidrma.h"
#include "mpiimpl.h"
#include "rdma_impl.h"
#include "dreg.h"
#include "ibutil.h"

#include <string.h>

#include "cm.h"


#include <unistd.h>
#include <sys/syscall.h>

uint32_t nb_delayed_shm = 0;
uint32_t nb_late_remote_targets = 0;
uint32_t nb_no_wqes = 0;


BOOL _ms = FALSE; //_monitoring_scheduling

#if defined( __JZ_TRACE) && defined(__JZ_SCHEDULING_MONITORING)
#define _UP_DELAYED_SHM()	do{ if(_ms) ++nb_delayed_shm;}while(0)
#define _UP_LATE_REMOTE_TARGET() do{ if(_ms) ++nb_late_remote_targets ;}while(0)
#define _UP_NO_WQE() do{ if(_ms) ++nb_no_wqes ;}while(0) 
#else
#define _UP_DELAYED_SHM()
#define _UP_LATE_REMOTE_TARGET()
#define _UP_NO_WQE()
#endif

#if defined( __JZ_TRACE) && defined(__JZ_SCHEDULING_MONITORING)
void JZ_activate_scheduling_monitoring()
{
	_ms = TRUE;
}

void JZ_deactivate_scheduling_monitoring()
{
	_ms = FALSE;
}


void JZ_dump_scheduling_stats()
{
	JZ_TRACE_IN_FILE("\nnb_delayed_shm = %u\n "
		"nb_late_remote_targets = %u\n"
		"nb_no_wqes = %u\n\n\n",
		nb_delayed_shm, nb_late_remote_targets, nb_no_wqes);
}


void JZ_clear_scheduling_monitor()
{
	nb_delayed_shm = 0;
	nb_late_remote_targets = 0;
	nb_no_wqes = 0;
}
#endif

int cpu_numa_map[] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};

static inline int getnumanode() 
{
#if 0
    #ifdef SYS_getcpu
    int node;
    int status;
    status = syscall(SYS_getcpu, NULL, &node, NULL);
    return (status == -1) ? status : node;
    #else
	JZ_FATAL("SYS_getcpu not defined\n");
    return -1; // unavailable
    #endif
#endif
	int pid = getpid();
	char filename[251];
	sprintf(filename, "/proc/%d/stat", getpid());
	FILE* f = fopen(filename, "r");
	char buf[251];
	int i;
	for(i=0; i<39; i++)
		fscanf(f, "%s", buf);
	fclose(f);
	int cpu = atoi(buf);
	//JZ_DEEP_PRINT("(CPU, NUMA_NODE) = (%d, %d)\n", cpu, cpu_numa_map[cpu]);
	return cpu_numa_map[cpu];
}

#if 0
static inline int get_numa_of_address(void* ptr)
{
	int numa_node = -1;
	get_mempolicy(&numa_node, NULL, 0, ptr, MPOL_F_NODE | MPOL_F_ADDR);
	return numa_node;
}
#endif


//#define PLAIN_ALLOC

#define INVALID_EPOCH_ID    0x0
#define CAS_LISTENER_TAG	0x7FFFFFFF
#define CAS_REPLY_TAG		(0x7FFFFFFF -1)
#define FETCH_AND_OP_LISTENER_TAG (0x7FFFFFFF - 2)
#define FETCH_AND_OP_REPLY_TAG (0x7FFFFFFF - 3)
#define MPI_ACC_MAX_OP	MPI_NO_OP

#define SHM_NAME_ROOT	"244a1a05-3eb5-45fc-8461-e5dcf5d16df1"

#define REGION_INTRA_NODE_STUFF

struct ibv_comp_channel* JZ_comp_channel = NULL; //


#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b) ((a) < (b) ? (a) : (b))
#endif

#define IS_PASSIVE_TARGET(epoch) ((epoch)->epoch_type >= JZ_EPOCH_TYPE_LOCK_ALL)

/*Datatype manipulation stuff*/
/**/

JZ_MPIDI_win_t* JZ_active_win_head = NULL;
JZ_MPIDI_win_t* JZ_active_win_tail = NULL;

uint32_t local_numa_node;

extern mv2_MPIDI_CH3I_RDMA_Process_t mv2_MPIDI_CH3I_RDMA_Process;
struct ibv_cq* JZ_cq = NULL;
uint32_t JZ_max_inline_size = 0;
double JZ_param_poll_time_for_post = -1.0;

int JZ_is_rma_in_progress = 0;
BOOL JZ_progressing_only_for_wqe_avail = FALSE;;
BOOL JZ_ignore_rma_progression = FALSE;

uint32_t JZ_shm_intra_numa_delayed_copy_threshold = SHM_DEFAULT_DELAYED_COPY_THRESHOLD;
uint32_t JZ_shm_inter_numa_delayed_copy_threshold = SHM_DEFAULT_DELAYED_COPY_THRESHOLD;
uint32_t JZ_shm_delayed_copy_threshold = JZ_INFINITE; //no scheduling by default
uint32_t JZ_ds_size_threshold_for_poll = 0;
uint32_t JZ_ds_failed_scan_size = 1;
BOOL JZ_double_scheduling = FALSE;

#define GET_SHM_DELAYED_COPY_THRESHOLD() JZ_shm_delayed_copy_threshold
#if 0
	((win_trait)->local_numa_node == local_numa_node ? \
	  JZ_shm_intra_numa_delayed_copy_threshold : \
	  JZ_shm_inter_numa_delayed_copy_threshold)
#endif

int remote_progress_nest_level = 0; //used as a boolean >0 ==> TRUE
BOOL progress_test_recursing = FALSE;
BOOL posting_remote_done = FALSE;

#if __WORDSIZE == 64
#define JZ_MPI_PTR	MPI_LONG
#else
#define JZ_MPI_PTR	MPI_INT
#endif

#include <limits.h>

JZ_rma_op_t* rma_op_cache_head = NULL;
JZ_lock_request_t* lock_request_cache_head = NULL;
JZ_rma_epoch_t* epoch_cache_head = NULL;
JZ_rma_epoch_t* single_lock_epoch_cache_head = NULL;

/*ALLOCATION MANAGEMENT*/

static inline JZ_rma_op_t* allocate_rma_op()
{
#ifdef PLAIN_ALLOC
	JZ_rma_op_t* rma_op = (JZ_rma_op_t*)malloc(sizeof(JZ_rma_op_t));
	return rma_op;
#else
	JZ_rma_op_t* rma_op = rma_op_cache_head;
	if(rma_op)
	{
		rma_op_cache_head = rma_op_cache_head->next;
		rma_op->next = NULL;
		rma_op->prev = NULL;
		rma_op->request = NULL;
		return rma_op;
	}
	rma_op =  malloc(sizeof(JZ_rma_op_t));
	rma_op->next = NULL;
	rma_op->prev = NULL;
	rma_op->request = NULL;
	return rma_op;
#endif
}

static inline void free_rma_op(JZ_rma_op_t* rma_op)
{
#ifdef PLAIN_ALLOC
	free(rma_op);
	return;
#else
	rma_op->next = rma_op_cache_head;
	rma_op_cache_head = rma_op;
#endif
}

static inline JZ_lock_request_t* allocate_lock_request()
{
 #ifdef PLAIN_ALLOC
	 return (JZ_lock_request_t*)malloc(sizeof(JZ_lock_request_t));
#else
	JZ_lock_request_t* lock_request = lock_request_cache_head;
	if(lock_request)
	{
		lock_request_cache_head = lock_request_cache_head->next;
		lock_request->next = NULL;
		return lock_request;
	}
	lock_request = malloc(sizeof(JZ_lock_request_t));
	lock_request->next = NULL;
	return lock_request;
#endif
}

static inline void free_lock_request(JZ_lock_request_t* lock_request)
{
#ifdef PLAIN_ALLOC
	free(lock_request);
	return;
#else
	lock_request->next = lock_request_cache_head;
	lock_request_cache_head = lock_request;
#endif
}

#ifndef offsetof
#define offsetof(st, m) ((size_t)(&((st *)0)->m))
#endif

static inline JZ_rma_epoch_t* allocate_epoch(JZ_MPIDI_win_t* win_ptr)
{
#ifdef PLAIN_ALLOC
	JZ_rma_epoch_t* epoch = calloc(sizeof(JZ_rma_epoch_t) + sizeof(JZ_epoch_trait_t)*win_ptr->comm_size, 1);
	epoch->epoch_traits = (JZ_epoch_trait_t*)((uintptr_t)epoch + sizeof(JZ_rma_epoch_t));
	epoch->deleted = FALSE;
	return epoch;
#else
	JZ_rma_epoch_t* epoch = epoch_cache_head;
	if(epoch)
	{
		epoch_cache_head = epoch_cache_head->next;
		epoch->next = NULL;
		memset(&epoch->close_request, 0, 
			offsetof(JZ_rma_epoch_t, empty) - offsetof(JZ_rma_epoch_t, close_request));
	}
	else
	{
		epoch = calloc(sizeof(JZ_rma_epoch_t), 1);
		epoch->epoch_traits = malloc(sizeof(JZ_epoch_trait_t)*MPIR_Process.comm_world->local_size);

		if(JZ_cq)
		{
			get_pinned_buffer(sizeof(JZ_fast_notification_t) * MPIR_Process.comm_world->local_size,
					&epoch->access_open_pinned_buf);
			get_pinned_buffer(sizeof(JZ_fast_notification_t) * MPIR_Process.comm_world->local_size,
					&epoch->access_close_pinned_buf);
		}
	}
	epoch->deleted = FALSE;
	return epoch;
#endif
}

static inline void free_epoch(JZ_rma_epoch_t* epoch)
{
	epoch->just_closed_rma_batch = FALSE; //This is important
	epoch->deleted = TRUE;
#ifdef PLAIN_ALLOC
	free(epoch);
	return;
#else
	epoch->next = epoch_cache_head;
	epoch_cache_head = epoch;
#endif
}

static inline JZ_rma_epoch_t* allocate_single_lock_epoch(JZ_MPIDI_win_t* win_ptr)
{
#ifdef PLAIN_ALLOC
	JZ_rma_epoch_t* epoch = calloc(sizeof(JZ_rma_epoch_t) + sizeof(JZ_epoch_trait_t), 1);
	epoch->epoch_traits = (JZ_epoch_trait_t*)((uintptr_t)epoch + sizeof(JZ_rma_epoch_t));
	return epoch;
#else
	JZ_rma_epoch_t* epoch = single_lock_epoch_cache_head;
	if(epoch)
	{
		single_lock_epoch_cache_head = single_lock_epoch_cache_head->next;
		epoch->next = NULL;
		memset(&epoch->close_request, 0, 
			offsetof(JZ_rma_epoch_t, empty) - offsetof(JZ_rma_epoch_t, close_request));
	}
	else
	{
		epoch = calloc(sizeof(JZ_rma_epoch_t), 1);
		epoch->epoch_traits = malloc(sizeof(JZ_epoch_trait_t));

		if(JZ_cq)
		{
			get_pinned_buffer(sizeof(JZ_fast_notification_t), &epoch->access_open_pinned_buf);
			get_pinned_buffer(sizeof(JZ_fast_notification_t), &epoch->access_close_pinned_buf);
		}
	}
	return epoch;
#endif
}

static inline void free_single_lock_epoch(JZ_rma_epoch_t* epoch)
{
#ifdef PLAIN_ALLOC
	free(epoch);
	return;
#else
	epoch->next = single_lock_epoch_cache_head;
	single_lock_epoch_cache_head = epoch;
#endif
}

/*ALLOCATION MANAGEMENT*/


static MPI_Win win_id_counter = INT_MIN;
static inline JZ_MPIDI_win_t* create_win_object()
{
    JZ_MPIDI_win_t *win_ptr = NULL; 
    win_ptr = calloc(1, sizeof(JZ_MPIDI_win_t));
    win_ptr->unique_win_id = win_id_counter++;
    if(win_id_counter == MPI_WIN_NULL)  //skip MPI_WIN_NULL
        win_id_counter++;
    return win_ptr;
}


static BOOL can_activate_epoch(JZ_MPIDI_win_t* win_ptr, JZ_epoch_type_t type, 
	int dest /*only for win_lock*/)
{
	JZ_rma_epoch_t* last_active_epoch = win_ptr->active_epoch_tail;
	if(!last_active_epoch)
		return TRUE;

	switch(type)
	{
		case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
		{
			switch(last_active_epoch->epoch_type)
			{
				case JZ_EPOCH_TYPE_START:
				case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
				case JZ_EPOCH_TYPE_LOCK_SHARED:
					return win_ptr->access_after_access_reorder &&
						(win_ptr->win_traits[dest].is_locked || /*target by lock or not a target at all*/
						  win_ptr->win_traits[dest].is_target == FALSE);
				case JZ_EPOCH_TYPE_POST:
					return  win_ptr->access_after_exposure_reorder &&
						(win_ptr->win_traits[dest].is_locked || /*target by lock or not a target at all*/
						  win_ptr->win_traits[dest].is_target == FALSE);
				default:
					return FALSE;
			}
		}
		case JZ_EPOCH_TYPE_LOCK_SHARED:
		{
			switch(last_active_epoch->epoch_type)
			{
				case JZ_EPOCH_TYPE_START:
				case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
				case JZ_EPOCH_TYPE_LOCK_SHARED:
					return win_ptr->access_after_access_reorder &&
						  win_ptr->win_traits[dest].is_target == FALSE;
				case JZ_EPOCH_TYPE_POST:
					return  win_ptr->access_after_exposure_reorder &&
						  win_ptr->win_traits[dest].is_target == FALSE;
				default:
					return FALSE;
			}
		}
		case JZ_EPOCH_TYPE_START:
		{
			switch(last_active_epoch->epoch_type)
			{
				case JZ_EPOCH_TYPE_START:
				case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
				case JZ_EPOCH_TYPE_LOCK_SHARED:
					return win_ptr->access_after_access_reorder;
				case JZ_EPOCH_TYPE_POST:
					return  win_ptr->access_after_exposure_reorder || 
						last_active_epoch->rma_done == RMA_DONE_STATUS_NONE;
				default:
					return FALSE;
			}
		}
		case JZ_EPOCH_TYPE_POST:
		{
			switch(last_active_epoch->epoch_type)
			{
				case JZ_EPOCH_TYPE_START:
					return win_ptr->exposure_after_access_reorder || 
						last_active_epoch->rma_done == RMA_DONE_STATUS_NONE;
				case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
				case JZ_EPOCH_TYPE_LOCK_SHARED:
					return win_ptr->exposure_after_access_reorder;
				case JZ_EPOCH_TYPE_POST:
					return  win_ptr->exposure_after_exposure_reorder;
				default:
					return FALSE;
			}
		}
		default:
			return FALSE;  //JZ_EPOCH_TYPE_FENCE and JZ_EPOCH_TYPE_LOCK_ALL
	}
}

static inline BOOL can_activate_new_epoch(JZ_MPIDI_win_t* win_ptr, 
	JZ_epoch_type_t type, int dest /*only for win_lock*/)
{
	if(win_ptr->deferred_epoch_head)
		return FALSE;
	return can_activate_epoch(win_ptr, type, dest);
}

static inline BOOL can_activate_deferred_epoch(JZ_MPIDI_win_t* win_ptr)
{
	JZ_rma_epoch_t* deferred_epoch = win_ptr->deferred_epoch_head;
	if(!deferred_epoch)
		return FALSE;
	int dest = -1;
	if(deferred_epoch->epoch_type >= JZ_EPOCH_TYPE_LOCK_SHARED)
		dest = (int)(uintptr_t)deferred_epoch->epoch_parameters[1];
	return can_activate_epoch(win_ptr, win_ptr->deferred_epoch_head->epoch_type, dest);
}

static void notify_done_with_targets_right_away(JZ_rma_epoch_t* epoch);
static void post_win_rma(JZ_MPIDI_win_t *win_ptr);
static void open_all_possible_epochs(JZ_MPIDI_win_t* win_ptr);

static inline BOOL done_flushing(JZ_rma_epoch_t* epoch) 
{
	return epoch->rma_op_list.head == NULL &&
			epoch->ss_cur_rma_op == NULL &&
			epoch->nb_expected_remote_completions == 0 &&
			epoch->flush_all_request_head == NULL;
}

static inline void complete_flush_all_request_if_possible(JZ_rma_epoch_t* epoch)
{
	JZ_FATAL("Not implemented\n");
	int incomplete;
	MPID_cc_decr(epoch->flush_all_request_head->cc_ptr, &incomplete);
	if(!incomplete)
	{
		//JZ_PRINT_STACK_TRACE();
		epoch->flush_all_request_head = epoch->flush_all_request_head->JZ_next;
		if(epoch->flush_all_request_head == NULL)
			epoch->flush_all_request_tail = NULL;
		MPIDI_CH3_Progress_signal_completion();
	}
}

static JZ_UGLY_FIX void complete_all_flush_requests(JZ_epoch_trait_t* epoch_trait)
{
	MPID_Request* cur = epoch_trait->flush_request_head;
	while(cur)
	{
		MPID_cc_set(cur->cc_ptr, 0);
		cur = cur->JZ_next;
		MPIDI_CH3_Progress_signal_completion();
	}
	epoch_trait->flush_request_head = epoch_trait->flush_request_tail = NULL;
	JZ_DEEP_TRACE_ON_ALL_CHANNELS("Applying UGLY FIX! \n");
}

static inline void update_flush_status(JZ_epoch_trait_t* epoch_trait, 
	uint32_t rma_id)
{
	int incomplete;
	MPID_Request* req = epoch_trait->flush_request_head;
	MPID_Request* prev_req = NULL;
	while(req)
	{
		if((uint32_t)(uintptr_t)req->JZ_data < rma_id)
			break;
		MPID_cc_decr(req->cc_ptr, &incomplete);
		if(!incomplete)
		{
			if(prev_req)
				prev_req->JZ_next = req->JZ_next;
			else
				epoch_trait->flush_request_head = req->JZ_next;
			if(epoch_trait->flush_request_tail == req)
				epoch_trait->flush_request_tail = prev_req;
			MPIDI_CH3_Progress_signal_completion();
		}
		prev_req = req;
		req = req->JZ_next;
	}
}

static inline void update_flush_all_status(JZ_rma_epoch_t* epoch, JZ_rma_op_t* rma_op) 
{
	//TODO
}


static inline BOOL can_close_access_epoch(JZ_rma_epoch_t* epoch) 
{
	if(epoch->cas_request_tail)
	{
		MPID_Request* req = epoch->cas_request_head;
		MPID_Request* temp_req = NULL;
		MPID_Request* prev_req = NULL;
		while(req)
		{
			if(MPID_Request_is_complete(req))
			{
				JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
					epoch->epoch_traits : &epoch->epoch_traits[(int)req->JZ_data_1];
				--epoch_trait->nb_rma_ops_to_complete;
				if(epoch_trait->flush_request_head) 
					update_flush_status(epoch_trait, (uint32_t)(uintptr_t)req->JZ_data);

				if(prev_req)
					prev_req->JZ_next = req->JZ_next;
				else
					epoch->cas_request_head = req->JZ_next;
				if(epoch->cas_request_tail == req)
					epoch->cas_request_tail = prev_req;
				temp_req = req;
				req = req->JZ_next;
				MPID_Request_release(temp_req);

				continue;
			}
			prev_req = req;
			req = req->JZ_next;
		}
	}

	if(epoch->epoch_type >= JZ_EPOCH_TYPE_LOCK_ALL) /*passive target must wait for all RDMA completions*/
		return epoch->rma_op_list.head == NULL &&
				epoch->ss_cur_rma_op == NULL &&
				epoch->nb_expected_remote_completions == 0 &&
				epoch->nb_done_packet_completions_to_get == 0 &&
				epoch->flush_all_request_head == NULL && 
				epoch->cas_request_tail == NULL;
	else
		return epoch->rma_op_list.head == NULL &&
				epoch->ss_cur_rma_op == NULL &&
				epoch->nb_expected_remote_get_completions == 0 &&
				epoch->nb_done_packet_completions_to_get == 0 &&
				epoch->flush_all_request_head == NULL && 
				epoch->cas_request_tail == NULL;
}

static inline BOOL can_close_exposure_epoch(JZ_rma_epoch_t* epoch)
{
	return epoch->nb_expected_done == 0 && epoch->nb_large_acc_to_complete == 0;
}

static inline BOOL can_close_fence_epoch(JZ_rma_epoch_t* epoch)
{
	return epoch->rma_op_list.head == NULL &&
			epoch->ss_cur_rma_op == NULL &&
			epoch->nb_expected_remote_get_completions == 0 &&
			epoch->nb_done_packet_completions_to_get == 0 &&
			epoch->nb_expected_done == 0 &&
			epoch->nb_large_acc_to_complete == 0 &&
			epoch->cas_request_tail == NULL;
}

static inline BOOL has_this_active_epoch(JZ_MPIDI_win_t* win_ptr, JZ_rma_epoch_t* epoch)
{
	JZ_rma_epoch_t* cur_epoch = win_ptr->active_epoch_head;
	while(cur_epoch)
	{
		if(cur_epoch == epoch)
			return TRUE;
		cur_epoch = cur_epoch->next;
	}
	return FALSE;
}

static inline void post_intra_node_epoch_rma(JZ_rma_op_t* rma_op);
static inline BOOL can_close_epoch(JZ_rma_epoch_t* epoch);
static inline void close_current_epoch_and_open_next_ones(JZ_rma_epoch_t* epoch);
static inline void set_done_flags(JZ_rma_epoch_t* epoch)
{
	assert(epoch->rma_done != RMA_DONE_STATUS_ISSUED);
	if(!epoch->is_active)
	{
		epoch->rma_done = RMA_DONE_STATUS_REQUESTED;
		return;
	}
	epoch->rma_done = RMA_DONE_STATUS_ISSUED;
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	uint32_t i;
	for(i=0; i<epoch->target_group_size; i++)
	{
		if(epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL)
		{
			if(epoch->epoch_traits[0].rma_done_status == RMA_DONE_STATUS_NONE)
				epoch->epoch_traits[0].rma_done_status = RMA_DONE_STATUS_REQUESTED;
		}
		else
		{
			if(epoch->epoch_traits[epoch->target_group_ranks[i]].rma_done_status == RMA_DONE_STATUS_NONE)
				epoch->epoch_traits[epoch->target_group_ranks[i]].rma_done_status = RMA_DONE_STATUS_REQUESTED;
		}
	}
	notify_done_with_targets_right_away(epoch);

#if 0
	if(JZ_cq)
	{
		if(epoch->nb_inter_node_rma_ops)
		{
			double start = MPI_Wtime();
			do
			{
				JZ_RMA_process_remote_communications(NULL);
			}while(epoch->nb_inter_node_rma_ops && 
				(MPI_Wtime() - start < JZ_param_poll_time_for_post));
		}
	}
#endif
	JZ_RMA_progress_test();
	if(!has_this_active_epoch(win_ptr, epoch))
		return; //epoch was completed, closed and deleted
#if 0
	if(epoch->nb_intra_node_rma_ops)
		post_intra_node_epoch_rma(epoch);
#endif

	if(can_close_epoch(epoch))
		close_current_epoch_and_open_next_ones(epoch);
}

#define REGION_REQ_STUFF
#ifdef REGION_REQ_STUFF

void JZ_RMA_req_query(MPID_Request* request, MPI_Status *status)
{
    MPIU_UNREFERENCED_ARG(request);

    /* All status fields, except the error code, are undefined */
	if(status == MPI_STATUS_IGNORE)
		return;
    status->count = 0;
    status->cancelled = FALSE;
    status->MPI_SOURCE = MPI_UNDEFINED;
    status->MPI_TAG = MPI_UNDEFINED;

    status->MPI_ERROR = MPI_SUCCESS;
}

static MPID_Request* make_rma_epoch_opening_request()
{
    MPID_Request *request_ptr = MPID_Request_create(); //thery are created with cc = 1
    request_ptr->kind = JZ_MPID_REQUEST_RMA_DUMMY; //opening requests are completed right away
    MPID_cc_set(request_ptr->cc_ptr, 0);  //I set it as completed right away.
    return request_ptr;
}


static inline MPID_Request* make_rma_epoch_closing_request(JZ_MPIDI_win_t* win_ptr, JZ_rma_epoch_t* epoch)
{
    MPID_Request *request_ptr = MPID_Request_create(); //thery are created with cc = 1
    request_ptr->kind = JZ_MPID_REQUEST_RMA_CLOSING_EPOCH;
	if(epoch)
	{
		epoch->close_request = request_ptr;
		epoch->just_closed_rma_batch = TRUE;
	}
	else //This could happen for MPI_Win_post epochs
	{
		MPID_cc_set(request_ptr->cc_ptr, 0);
		MPIDI_CH3_Progress_signal_completion();
	}
    return request_ptr;
}

static MPID_Request* make_rma_win_flush_request(JZ_rma_epoch_t* epoch, int dest)
{
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[dest];

    MPID_Request *request_ptr = MPID_Request_create(); //thery are created with cc = 1
	MPID_cc_set(request_ptr->cc_ptr, epoch_trait->nb_rma_ops_to_complete);
    request_ptr->kind = JZ_MPID_REQUEST_RMA_WIN_FLUSH;
	request_ptr->JZ_next = NULL;
	request_ptr->JZ_data = (void*)(uintptr_t)(epoch->rma_id - 1); /*anything younger than this and 
		meant for this destination is concerned by the flush*/

	int dummy;
	BOOL complete = TRUE;
	if(!epoch_trait->nb_rma_ops_to_complete)
		return request_ptr;

	if(epoch_trait->flush_request_tail)
	{
		epoch_trait->flush_request_tail->JZ_next = request_ptr;
		epoch_trait->flush_request_tail = request_ptr;
	}
	else
	{
		epoch_trait->flush_request_head = epoch_trait->flush_request_tail = request_ptr;
	}
    return request_ptr;
}

/*YET TO BE FIXED*/
static inline MPID_Request* make_rma_win_flush_all_request(JZ_rma_epoch_t* epoch)
{
	JZ_FATAL("Not implemented :) \n");
    MPID_Request *request_ptr = MPID_Request_create(); //thery are created with cc = 1
    request_ptr->kind = JZ_MPID_REQUEST_RMA_WIN_FLUSH;
	request_ptr->JZ_next = NULL;

	MPID_cc_set(request_ptr->cc_ptr, 0);
	int dummy;
	BOOL complete = TRUE;
	if(epoch->rma_op_list.tail)
	{
		MPID_cc_incr(request_ptr->cc_ptr, &dummy);
		complete = FALSE;
	}
	if(epoch->cas_request_tail)
	{
		epoch->cas_request_tail->JZ_data = request_ptr;
		MPID_cc_incr(request_ptr->cc_ptr, &dummy);
		complete = FALSE;
	}
	if(complete)
		return request_ptr;
	

	if(epoch->flush_all_request_tail)
	{
		epoch->flush_all_request_tail->JZ_next = request_ptr;
		epoch->flush_all_request_tail = request_ptr;
	}
	else
	{
		epoch->flush_all_request_head = epoch->flush_all_request_tail = request_ptr;
	}
    return request_ptr;
}
#endif //REGION_REQ_STUFF

static inline JZ_rma_epoch_t* create_epoch(JZ_MPIDI_win_t* win_ptr, JZ_epoch_type_t epoch_type)
{
	JZ_rma_epoch_t* epoch;
	if(epoch_type == JZ_EPOCH_TYPE_LOCK_SHARED ||
		epoch_type == JZ_EPOCH_TYPE_LOCK_EXCLUSIVE)
		epoch = allocate_single_lock_epoch(win_ptr);
	else
		epoch = allocate_epoch(win_ptr);
	epoch->win_ptr = win_ptr;
	epoch->epoch_type = epoch_type;
	win_ptr->most_recent_epoch = epoch;
	switch(epoch_type)
	{
		case JZ_EPOCH_TYPE_START:
		case JZ_EPOCH_TYPE_LOCK_ALL:
		case JZ_EPOCH_TYPE_LOCK_SHARED:
		case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
			win_ptr->most_recent_origin_epoch = epoch; 
			break;
		case JZ_EPOCH_TYPE_POST:
			win_ptr->most_recent_target_epoch = epoch; 
			break;
		case JZ_EPOCH_TYPE_FENCE:
			win_ptr->most_recent_origin_epoch = epoch;
			win_ptr->most_recent_target_epoch = epoch; 
			break;
		default:
			JZ_FATAL("Unsupported epoch type\n");
	}
	return epoch;
}


static void defer_epoch(JZ_MPIDI_win_t *win_ptr, JZ_epoch_type_t epoch_type,
        void* arg0, void* arg1, void* arg2)
{
    JZ_rma_epoch_t *epoch = create_epoch(win_ptr, epoch_type);
	if(epoch_type > JZ_EPOCH_TYPE_LOCK_ALL)
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t));
	else
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t)*win_ptr->comm_size);

    epoch->epoch_parameters[0] = arg0;
    epoch->epoch_parameters[1] = arg1;
    epoch->epoch_parameters[2] = arg2;

	add_node((void**)(&win_ptr->deferred_epoch_head), (void**)(&win_ptr->deferred_epoch_tail), epoch);
}


static int internal_win_start_impl(MPID_Group *group_ptr, int assert, JZ_rma_epoch_t* epoch);
static int internal_win_post_impl(MPID_Group *post_grp_ptr, int assert, JZ_rma_epoch_t* epoch);
static int internal_win_lock_impl(int lock_type, int dest, int assert, JZ_rma_epoch_t* epoch);
static int internal_win_lock_all_impl(int assert, JZ_rma_epoch_t* epoch);

static BOOL activate_next_deferred_epoch(JZ_MPIDI_win_t *win_ptr)
{
    int mpi_errno = MPI_SUCCESS;
    JZ_rma_epoch_t *epoch = win_ptr->deferred_epoch_head;
    remove_node((void**)(&win_ptr->deferred_epoch_head), (void**)(&win_ptr->deferred_epoch_tail), epoch);

	JZ_DEEP_TRACE_ON_ALL_CHANNELS_ON_MASK(JZ_DEFERRED_EPOCH_MASK, "Activating deferred epoch\n");

    switch(epoch->epoch_type)
    {
        case JZ_EPOCH_TYPE_START:
            mpi_errno = internal_win_start_impl((MPID_Group*)epoch->epoch_parameters[0],
                    (int)(uintptr_t)epoch->epoch_parameters[1], epoch);
            assert(mpi_errno == MPI_SUCCESS); // I must think of a way of propagating this instead of simply asserting! 
            MPIR_Group_release((MPID_Group*)epoch->epoch_parameters[0]);
            break;
        case JZ_EPOCH_TYPE_POST:
            mpi_errno = internal_win_post_impl((MPID_Group*)epoch->epoch_parameters[0],
                    (int)(uintptr_t)epoch->epoch_parameters[1], epoch);
            assert(mpi_errno == MPI_SUCCESS); // I must think of a way of propagating this instead of asserting! 
            MPIR_Group_release((MPID_Group*)epoch->epoch_parameters[0]);
            break;
        case JZ_EPOCH_TYPE_FENCE:
            mpi_errno = internal_win_post_impl(win_ptr->grp_ptr, 0, epoch);
            assert(mpi_errno == MPI_SUCCESS);
            mpi_errno = internal_win_start_impl(win_ptr->grp_ptr, 0, epoch);
            assert(mpi_errno == MPI_SUCCESS);
			epoch->epoch_side = JZ_EPOCH_SIDE_ORIGIN_AND_TARGET;
            break;
        case JZ_EPOCH_TYPE_LOCK_SHARED:
        case JZ_EPOCH_TYPE_LOCK_EXCLUSIVE:
            mpi_errno = internal_win_lock_impl((int)(uintptr_t)epoch->epoch_parameters[0],
                    (int)(uintptr_t)epoch->epoch_parameters[1],
                    (int)(uintptr_t)epoch->epoch_parameters[2],
                    epoch);
            assert(mpi_errno == MPI_SUCCESS);
            break;
        case JZ_EPOCH_TYPE_LOCK_ALL:
            mpi_errno = internal_win_lock_all_impl((int)(uintptr_t)epoch->epoch_parameters[0], epoch);
            assert(mpi_errno == MPI_SUCCESS);
            break;
        default:
			JZ_WAIT_FOR_DEBUGGER();
            JZ_FATAL("What the heck! ...");
    }
	add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), epoch);
	if(epoch->rma_done == RMA_DONE_STATUS_REQUESTED)
		set_done_flags(epoch);

    JZ_RMA_progress_test();
	return TRUE;
}


static inline void open_all_possible_epochs(JZ_MPIDI_win_t* win_ptr)
{
	while(can_activate_deferred_epoch(win_ptr))
		activate_next_deferred_epoch(win_ptr);
}


static inline void delete_epoch(JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	if(win_ptr->most_recent_epoch == epoch)
		win_ptr->most_recent_epoch = NULL;
	if(win_ptr->most_recent_origin_epoch == epoch)
		 win_ptr->most_recent_origin_epoch = NULL;
	if(win_ptr->most_recent_target_epoch == epoch)
		 win_ptr->most_recent_target_epoch = NULL;

	if(epoch->epoch_type == JZ_EPOCH_TYPE_LOCK_SHARED ||
		epoch->epoch_type == JZ_EPOCH_TYPE_LOCK_EXCLUSIVE)
		free_single_lock_epoch(epoch);
	else
		free_epoch(epoch);
}

static void close_epoch(JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
    remove_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), epoch);
    if(epoch->epoch_type == JZ_EPOCH_TYPE_FENCE)
		--JZ_is_rma_in_progress;  //fence is double epoch
    --JZ_is_rma_in_progress;

    if(epoch->close_request)
    {
        MPID_cc_set(epoch->close_request->cc_ptr, 0);
		MPIDI_CH3_Progress_signal_completion();
    }

    if(epoch->flush_all_request_head)
    {
        MPID_Request* req = epoch->flush_all_request_head;
		MPID_Request* next;
        while(req)
        {
			next = req->JZ_next;
            MPID_cc_set(req->cc_ptr, 0);
			MPIDI_CH3_Progress_signal_completion();
            req = next;
        }
    }

    if(epoch->target_group_ranks)
        free(epoch->target_group_ranks);
	
	delete_epoch(epoch);
}

static inline void close_current_epoch_and_open_next_ones(JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t *win_ptr = epoch->win_ptr;
	close_epoch(epoch);
	open_all_possible_epochs(win_ptr);
}


#ifdef REGION_SHM
int JZ_create_shared_mem(JZ_shared_mem_t* __JZ_IN_OUT shm)
{
    int status;
    errno = 0;
    int desc = shm_open(shm->name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
	JZ_FATAL_IF(desc == -1, "shm_open failed! strerror(errno) is %s\n", strerror(errno));
    
	errno = 0;
    status = ftruncate(desc, shm->size);
    JZ_FATAL_IF(status != 0, "ftruncate failed on fd %d for size %lf MB! strerror(errno) is %s\n",
		 desc, 1.0*shm->size/MB, strerror(errno));

    errno = 0;
    shm->buffer = mmap(NULL, shm->size, PROT_WRITE | PROT_READ, MAP_SHARED, desc, 0); 
	JZ_FATAL_IF(shm->buffer ==  MAP_FAILED, "mmap failed! strerror(errno) is %s\n", strerror(errno));
    return 0;
}


static inline int JZ_delete_shared_mem(JZ_shared_mem_t* __JZ_IN_OUT shm)
{
    int status = munmap(shm->buffer, shm->size);
	JZ_FATAL_IF(status != 0,  "munmap failed! strerror(errno) is %s\n", strerror(errno));
    shm->buffer = NULL;

    status = shm_unlink(shm->name);
	JZ_FATAL_IF(status != 0 && errno != ENOENT,  
		"shm_unlink failed! errno = %d; strerror(errno) is %s\n", errno, strerror(errno));
	return status;
}

static inline void reset_shm_control(JZ_pair_of_shm_control_queue_t* shm_control)
{
    shm_control->in->next_production_id = 
        shm_control->out->next_consumption_id = 0;
}

static inline BOOL can_get_shm_control(JZ_pair_of_shm_control_queue_t* shm_control)
{
    return shm_control->out->next_consumption_id < shm_control->out->next_production_id;
}

static inline void get_shm_control(JZ_pair_of_shm_control_queue_t* shm_control, uintptr_t* value)
{
    uint32_t consumption_id = shm_control->out->next_consumption_id;
    while(consumption_id == shm_control->out->next_production_id);
    *value = shm_control->out->buf[consumption_id%SHM_CIRCULAR_BUF_SIZE];
    ++shm_control->out->next_consumption_id;
}

static inline BOOL try_get_shm_control(JZ_pair_of_shm_control_queue_t* shm_control, uintptr_t* value)
{
	uint32_t consumption_id = shm_control->out->next_consumption_id;
    if(consumption_id == shm_control->out->next_production_id)
        return FALSE;
    *value = shm_control->out->buf[consumption_id%SHM_CIRCULAR_BUF_SIZE];
    ++shm_control->out->next_consumption_id;
    return TRUE;
}

static inline BOOL can_set_shm_control(JZ_pair_of_shm_control_queue_t* shm_control)
{
    return shm_control->in->next_production_id - shm_control->in->next_consumption_id <
        SHM_CIRCULAR_BUF_SIZE;
}

static inline void set_shm_control(JZ_pair_of_shm_control_queue_t* shm_control, uintptr_t value)
{
    uint32_t production_id = shm_control->in->next_production_id;
    while(production_id - shm_control->in->next_consumption_id == SHM_CIRCULAR_BUF_SIZE);
    shm_control->in->buf[production_id%SHM_CIRCULAR_BUF_SIZE] = value;
    ++shm_control->in->next_production_id;
}

static inline BOOL try_set_shm_control(JZ_pair_of_shm_control_queue_t* shm_control, uintptr_t value)
{
	uint32_t production_id = shm_control->in->next_production_id;
    if(production_id - shm_control->in->next_consumption_id == SHM_CIRCULAR_BUF_SIZE)
        return FALSE;
    shm_control->in->buf[production_id%SHM_CIRCULAR_BUF_SIZE] = value;
    ++shm_control->in->next_production_id;
    return TRUE;
}


#endif //REGION_SHM

static inline build_type_info(MPI_Datatype type, int count, JZ_type_info_t* type_info)
{
    MPI_Aint dt_true_lb;
    MPIDI_Datatype_get_info(count, type,
            type_info->is_contig, type_info->data_size, type_info->dtp, dt_true_lb);

    if(!type_info->dtp)
    {
        type_info->eltype = type;
        type_info->elsize = MPID_Datatype_get_basic_size(type);
        type_info->is_contig = TRUE;
        type_info->data_size = type_info->elsize*count;
    }
	else
	{
		type_info->eltype = type_info->dtp->eltype;
		type_info->elsize = type_info->dtp->element_size;
	}
}

static inline void delete_win_object(JZ_MPIDI_win_t* win_ptr)
{
    remove_node((void**)(&JZ_active_win_head), (void**)(&JZ_active_win_tail), win_ptr);
    free(win_ptr);
}

static BOOL lock_request_predicate(void* lock_request, void* requester_rank)
{
    return ((JZ_lock_request_t*)lock_request)->requester_rank == (int)(uintptr_t)requester_rank;
}

static inline void free_ibv_send_q_slot(JZ_win_trait_t* win_trait, int nb_slots_to_free)
{
	JZ_EXEC(BOOL had_to_free_wqes = FALSE);
	JZ_EXEC(int count = 0);
    //JZ_progressing_only_for_wqe_avail = TRUE;
	struct MPIDI_VC* vc = win_trait->vc;
    while(vc->mrail.rails[0].send_wqes_avail < nb_slots_to_free)
    {
        MPIDI_CH3I_Progress_test();
        JZ_DEEP_TRACE_IN_FILE("Trying to free wqes\n");
		JZ_EXEC(had_to_free_wqes = TRUE);
		//JZ_EXEC(JZ_WAIT_FOR_DEBUGGER_IF(++count == 20));
    }
    //JZ_progressing_only_for_wqe_avail = FALSE;
	JZ_EXEC(JZ_DEEP_TRACE_IN_FILE_IF(had_to_free_wqes, "Done freeing wqes\n"));
}

static inline int JZ_ibv_post_send(
        JZ_MPIDI_win_t *win_ptr,
        struct ibv_qp* qp,
        uint64_t wr_id,
        void* laddr,
        uint32_t lkey,
        void* raddr,
        uint32_t rkey,
        enum ibv_wr_opcode opcode,
        int send_flags,
        int size,
        uint32_t imm_data)
{
    struct ibv_send_wr wr;
    struct ibv_sge sge;
    struct ibv_send_wr *bad_wr = NULL;

    //JZ_DEEP_TRACE_ON_MASK(JZ_WR_ID_MASK, "wr_id = %p\n", wr_id);

    memset(&sge, 0, sizeof(sge));
    memset(&wr, 0, sizeof(wr));

    wr.wr_id = wr_id;
    sge.addr = (uint64_t)laddr;
    sge.lkey = lkey;
    sge.length = size;
    wr.sg_list = &sge;
    wr.num_sge = 1;
    if(raddr)
    {
        wr.wr.rdma.remote_addr = (uint64_t)raddr;
        wr.wr.rdma.rkey = rkey;
    }
	wr.imm_data = imm_data;
    wr.opcode = opcode;
    wr.send_flags = IBV_SEND_SIGNALED;
    wr.send_flags |= send_flags;
    ++mv2_MPIDI_CH3I_RDMA_Process.global_used_send_cq;
    ++win_ptr->nb_consummed_wqes;
    return ibv_post_send(qp, &wr, &bad_wr);
}

static inline void post_remote_done_packet(JZ_rma_epoch_t* epoch, int target_rank)
{
	if(posting_remote_done)
		return;
	posting_remote_done = TRUE;
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[target_rank];
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[target_rank];

	JZ_WAIT_FOR_DEBUGGER_IF(!epoch_trait->has_access);

	/*Careful, the test below is not redundant. It fixes a very subtle recrusion bug*/
	if(epoch_trait->rma_done_status == RMA_DONE_STATUS_ISSUED) 
		return;

	free_ibv_send_q_slot(win_trait, 1);

	uint64_t notif = JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS;
	if(epoch->epoch_type >= JZ_EPOCH_TYPE_LOCK_ALL)
	{
		notif = JZ_RMA_WORK_LOCK_RELEASE;
		--win_trait->is_locked;
		JZ_DEEP_TRACE_IN_FILE("Sending lock release to %d\n", target_rank);
	}

	JZ_fast_notification_t *fast_notif = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		&(((JZ_fast_notification_t*)epoch->access_close_pinned_buf->buf)[0]):
		&(((JZ_fast_notification_t*)epoch->access_close_pinned_buf->buf)[target_rank]);
	
	fast_notif->dest_win_ptr = win_trait->remote_win_ptr;
	fast_notif->notification = notif;
	fast_notif->exposure_id = epoch_trait->access_id;
	fast_notif->src_rank = win_ptr->rank;
	
    int mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
            notif|(uintptr_t)epoch_trait,
            fast_notif, 
            epoch->access_close_pinned_buf->dreg->memhandle[0]->lkey, 
            NULL,
            0,
            IBV_WR_SEND_WITH_IMM, //IBV_WR_RDMA_WRITE_WITH_IMM,
            IBV_SEND_FENCE,  //send_flags
            sizeof(JZ_fast_notification_t), //size of data to send
            JZ_RMA_FAST_NOTIF_IMM_VALUE);
    JZ_FATAL_IF(mpi_errno, "ibv_post_send failed with error 0x%X", mpi_errno);
    --win_trait->vc->mrail.rails[0].send_wqes_avail;  
	JZ_FATAL_IF(win_trait->is_locked < 0, "win_trait->is_locked is negative\n");
	//--win_trait->is_target is done upon receiving the corresponding CQE

	--epoch->nb_done_packets_to_send;
    epoch_trait->rma_done_status = RMA_DONE_STATUS_ISSUED; 
	posting_remote_done = FALSE;

#if 0 
    struct ibv_qp_attr attr;
    struct ibv_qp_init_attr init_attr;
    mpi_errno = ibv_query_qp(win_trait->qp, &attr, IBV_QP_CAP, &init_attr);
    JZ_FATAL_IF(mpi_errno, "ibv_query_qp failed with error 0x%X", mpi_errno);
    JZ_DEEP_TRACE("init_attr.cap.max_inline_data = %lu\n", init_attr.cap.max_inline_data);
    JZ_DEEP_TRACE("attr.cap.max_inline_data = %lu\n", attr.cap.max_inline_data);


	struct ibv_device_attr dev_attr;
	ibv_query_device(win_trait->qp->context, &dev_attr);

	JZ_WAIT_ONCE_FOR_DEBUGGER();
#endif
}

static inline void post_shm_done_packet(JZ_rma_epoch_t* epoch, int target)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[target];
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[target];

	if(epoch->epoch_type >= JZ_EPOCH_TYPE_LOCK_ALL)
	{
		--win_trait->is_locked;
		set_shm_control(&win_trait->shm_queues, JZ_RMA_WORK_LOCK_RELEASE);
	}
	else
		set_shm_control(&win_trait->shm_queues, 
			JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS|(uint64_t)epoch_trait->access_id);

	epoch_trait->rma_done_status = RMA_DONE_STATUS_ISSUED;
	--epoch->nb_done_packets_to_send;
	--epoch->nb_done_packet_completions_to_get;
	--win_trait->is_target;
	JZ_FATAL_IF(win_trait->is_locked < 0, "win_trait->is_locked is negative\n");
}

static int post_remote_accumulate(JZ_rma_op_t* rma_op)
{
    /*CAUTION! send_wqes_avail decrementation happened outside of this call.
     * This was meant to uniformize certain practices and ease the logical reading of 
     * the code
     * */
	JZ_epoch_trait_t* epoch_trait = rma_op->epoch_trait;
	JZ_rma_epoch_t* epoch = epoch_trait->epoch;
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[rma_op->target_rank];
    int mpi_errno = MPI_SUCCESS;
    JZ_unknown_packet_info_t* unknown_packet_info = NULL;
    JZ_acc_data_copy_t* acc_data_copy = NULL;

    int total_size = sizeof(JZ_unknown_packet_info_t) + sizeof(JZ_acc_data_copy_t) +
        rma_op->origin_type_info.data_size;

    if(total_size < rdma_put_fallback_threshold)
    {
		get_pinned_buffer(total_size, &rma_op->pinned_buf);
		rma_op->status = JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER;
		unknown_packet_info = (JZ_unknown_packet_info_t*)rma_op->pinned_buf->buf;

        unknown_packet_info->temp_buf_type = JZ_RMA_WORK_SINGLE_STEP_ACC;
        unknown_packet_info->src_win_ptr = win_ptr;
        unknown_packet_info->dest_win_ptr = win_trait->remote_win_ptr;
        unknown_packet_info->src_rank = win_ptr->rank;
        unknown_packet_info->dest_rank = rma_op->target_rank;

        acc_data_copy = (JZ_acc_data_copy_t*)unknown_packet_info->content;
        acc_data_copy->data_destination_address = (void*)((uintptr_t)win_trait->base.addr + 
                rma_op->target_disp*win_trait->disp_unit);
        acc_data_copy->op = rma_op->acc_op;
        acc_data_copy->builtin_type = rma_op->origin_type_info.eltype;
        acc_data_copy->builtin_type_size = rma_op->origin_type_info.elsize;
        acc_data_copy->address_of_pinned_buf = NULL;
        acc_data_copy->data_size = rma_op->origin_type_info.data_size;
        memcpy(acc_data_copy->data,	rma_op->origin_addr, rma_op->origin_type_info.data_size);

        mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
                JZ_RMA_WORK_SINGLE_STEP_ACC|(uintptr_t)rma_op,
                unknown_packet_info,
                rma_op->pinned_buf->dreg->memhandle[0]->lkey, 
                NULL,
                0,
                IBV_WR_SEND_WITH_IMM,
                0, //send_flags
                total_size, //size of data to send
                JZ_RMA_IMM_VALUE /*immediate data*/);
        JZ_FATAL_IF(mpi_errno, "ibv_post_send failed in JZ_Accumulate; mpi_errno = %d.\n", mpi_errno);
        --epoch_trait->nb_rma_ops;  //incremented only for the single-step acc post
		--epoch->nb_inter_node_rma_ops;
    }
    else
    {
		int64_t data[] = {rma_op->origin_type_info.data_size/rma_op->origin_type_info.elsize, //count
			rma_op->origin_type_info.eltype, //builtin type
			(int64_t)((uintptr_t)win_trait->base.addr + rma_op->target_disp*win_trait->disp_unit),
			rma_op->acc_op,
			epoch_trait->access_id};

		PMPI_Send(&data, MESSAGE_PUMP_BUF_SIZE, MPI_LONG_LONG, rma_op->target_rank, 
			rma_op->acc_op, win_ptr->comm_ptr->handle);

		MPI_Request req;
		PMPI_Isend(rma_op->origin_addr, rma_op->origin_type_info.data_size, 
			MPI_CHAR, rma_op->target_rank, rma_op->acc_op, win_ptr->large_acc_comm_ptr->handle, &req);
		MPID_Request_get_ptr(req, rma_op->request); 
		rma_op->request->JZ_data = rma_op;
		add_req(&win_ptr->origin_side_acc_req_head, &win_ptr->origin_side_acc_req_tail,
			rma_op->request);
    }
}

static inline JZ_lock_request_t* enqueue_lock_request(JZ_MPIDI_win_t *win_ptr, int requester_rank, JZ_lock_type_t lock_type)
{
    JZ_lock_request_t* lock_request;
    lock_request = (JZ_lock_request_t*)allocate_lock_request();
    lock_request->next = NULL;
    lock_request->requester_rank = requester_rank;
    lock_request->lock_type = lock_type;
    add_node((void**)(&win_ptr->lock_requests.head), (void**)(&win_ptr->lock_requests.tail), lock_request);				
    return lock_request;
}

static inline void grant_shm_lock(JZ_MPIDI_win_t *win_ptr, int rank, JZ_lock_type_t lock_type)
{
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[rank];
	++win_trait->exposure_epoch_counter;
	++win_trait->shm_remote_access_control[win_ptr->rank];
    if(lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
        win_ptr->exclusive_locking_rank = rank;
    else
        ++win_ptr->shared_lock_ref_count;
    win_ptr->granted_lock_type = lock_type;
    win_ptr->win_traits[rank].is_lock_holder = TRUE;

}

static inline void grant_remote_lock(JZ_MPIDI_win_t *win_ptr, int rank, JZ_lock_type_t lock_type)
{
    int error_status;
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[rank];
    free_ibv_send_q_slot(win_trait, 1);
    --win_trait->vc->mrail.rails[0].send_wqes_avail;
    JZ_AD_HOC_TRACE_SEND_WQES_AFTER_DEC(win_trait->vc->mrail.rails[0].send_wqes_avail, rank);
    ++win_trait->exposure_epoch_counter;
	JZ_pinned_buf_t* pinned_buf;
	get_pinned_buffer(sizeof(uint32_t), &pinned_buf);
	*((uint32_t*)pinned_buf->buf) = win_trait->exposure_epoch_counter;
	pinned_buf->metadata = (uintptr_t)win_trait;

	JZ_DEEP_TRACE_IN_FILE("About to grant lock to %d\n", rank);

	error_status = JZ_ibv_post_send(win_ptr, win_trait->qp,
			JZ_RMA_WORK_LOCK_GRANT_STATUS|((uintptr_t)pinned_buf),
			pinned_buf->buf,
			pinned_buf->dreg->memhandle[0]->lkey,
			((uint32_t*)win_trait->access_control_info.addr + win_ptr->rank),
			win_trait->access_control_info.rkey,
			IBV_WR_RDMA_WRITE,
			0, //send_flags
			sizeof(uint32_t), //size of data to send
			0 /*immediate data*/);
    JZ_FATAL_IF(error_status, "");

    if(lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
        win_ptr->exclusive_locking_rank = rank;
    else
        ++win_ptr->shared_lock_ref_count;
    win_ptr->granted_lock_type = lock_type;
    win_trait->is_lock_holder = TRUE;
}


static void process_lock_requests(JZ_MPIDI_win_t *win_ptr)
{
    int mpi_errno;
    JZ_lock_request_t *lock_request = win_ptr->lock_requests.head;
    uint32_t i;
    JZ_win_trait_t* win_trait = NULL;

    //check shm stuff
	if(win_ptr->has_shm_passive_target_backlog)
	{
		for(i=0; i<win_ptr->nb_shm_peers; i++)
		{
			win_trait = &win_ptr->win_traits[win_ptr->shm_peers[i]];
			if(win_trait->backlog_of_unlock)
			{
				--win_trait->backlog_of_unlock;
				--win_ptr->has_shm_passive_target_backlog;
				if(!win_trait->is_lock_holder)
				{
					JZ_WAIT_FOR_DEBUGGER();
					JZ_FATAL("Unlock request received from a peer that did not lock\n");
				}
				if(win_ptr->granted_lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
				{
					win_ptr->granted_lock_type = JZ_LOCK_REQUEST_NONE;
					win_ptr->exclusive_locking_rank = -1;				
				}
				else if(--win_ptr->shared_lock_ref_count == 0)
				{
					win_ptr->granted_lock_type = JZ_LOCK_REQUEST_NONE;
				}
				win_trait->is_lock_holder = FALSE;
			}
			if(win_trait->backlog_of_xlock)
			{
				--win_trait->backlog_of_xlock;
				--win_ptr->has_shm_passive_target_backlog;
				if(win_ptr->granted_lock_type != JZ_LOCK_REQUEST_NONE)
					(void)enqueue_lock_request(win_ptr, win_trait->rank, JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST);
				else
				{
					grant_shm_lock(win_ptr, win_trait->rank, JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST);
					return;
				}
			}
			if(win_trait->backlog_of_slock)
			{
				--win_trait->backlog_of_slock;
				--win_ptr->has_shm_passive_target_backlog;
				if(win_ptr->granted_lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
					(void)enqueue_lock_request(win_ptr, win_trait->rank, JZ_RMA_WORK_SHARED_LOCK_REQUEST);
				else
					grant_shm_lock(win_ptr, win_trait->rank, JZ_RMA_WORK_SHARED_LOCK_REQUEST);
			}
		}
	}

    if(win_ptr->granted_lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
        return; // we can't grant anything for now

    lock_request = win_ptr->lock_requests.head;  
	JZ_lock_request_t* next_lock_request = NULL;
    while(lock_request)
    {
		next_lock_request = lock_request->next;
        if(lock_request->lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
        {
            if(win_ptr->granted_lock_type != JZ_LOCK_REQUEST_NONE)
            {
                lock_request = next_lock_request;
                continue;
            }

            if(!win_ptr->win_traits[lock_request->requester_rank].is_intra_node)
                grant_remote_lock(win_ptr, lock_request->requester_rank, JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST);
            else
                grant_shm_lock(win_ptr, lock_request->requester_rank, JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST);
            remove_node((void**)(&win_ptr->lock_requests.head), (void**)(&win_ptr->lock_requests.tail), lock_request);
            free_lock_request(lock_request);
            break;
        }
        else
        {
            if(!win_ptr->win_traits[lock_request->requester_rank].is_intra_node)
                grant_remote_lock(win_ptr, lock_request->requester_rank, JZ_RMA_WORK_SHARED_LOCK_REQUEST);
            else
                grant_shm_lock(win_ptr, lock_request->requester_rank, JZ_RMA_WORK_SHARED_LOCK_REQUEST);

            remove_node((void**)(&win_ptr->lock_requests.head), (void**)(&win_ptr->lock_requests.tail), lock_request);
            free_lock_request(lock_request);
        }
        lock_request = next_lock_request;
    }
}

/*CAUTION: Accumulate handles buffer preparation in post_remote_accumulate; not here*/
static inline void prepare_origin_buffer_for_ibv(JZ_rma_op_t *rma_op)
{
    if(rma_op->origin_type_info.data_size < rdma_put_fallback_threshold)
    {
        get_pinned_buffer(rma_op->origin_type_info.data_size, &rma_op->pinned_buf);
        JZ_FATAL_IF(!rma_op->pinned_buf, "Couldn't get pinned buf of size %d from pool.\n",  
                rma_op->origin_type_info.data_size);
        if(rma_op->rma_comm_type == JZ_RMA_WORK_PUT)
            memcpy(rma_op->pinned_buf->buf, rma_op->origin_addr, rma_op->origin_type_info.data_size);
        rma_op->status = JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER;
    }
    else
    {
        rma_op->pinned_buf_data.dreg = dreg_register(rma_op->origin_addr, rma_op->origin_type_info.data_size);
        JZ_FATAL_IF(!rma_op->pinned_buf_data.dreg, "Memory registration failed.\n" );
        rma_op->pinned_buf_data.buf = rma_op->origin_addr;
        rma_op->status = JZ_RMA_STATUS_POSTED;
    }
}

static inline BOOL update_access(JZ_MPIDI_win_t* win_ptr, JZ_epoch_trait_t* epoch_trait);
static void post_inter_node_rma(JZ_MPIDI_win_t *win_ptr, JZ_win_trait_t* win_trait,
	JZ_rma_epoch_t* epoch, JZ_epoch_trait_t* epoch_trait, JZ_rma_op_t* rma_op)
{
	int mpi_errno;
	/*
	   JZ_epoch_trait_t* epoch_trait;
	   JZ_win_trait_t* win_trait;
	   JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	   */

	JZ_pinned_buf_t *pinned_buf;

	/*It is assumed that wqe availability is checked before this function is entered*/
	--win_trait->vc->mrail.rails[0].send_wqes_avail;
	JZ_AD_HOC_TRACE_SEND_WQES_AFTER_DEC(win_trait->vc->mrail.rails[0].send_wqes_avail, 
			rma_op->target_rank);

	switch(rma_op->rma_comm_type)
	{
		case JZ_RMA_WORK_PUT:
			prepare_origin_buffer_for_ibv(rma_op);
			pinned_buf = rma_op->status == JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER ?
				rma_op->pinned_buf : &rma_op->pinned_buf_data;
			mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
					JZ_RMA_WORK_PUT|(uintptr_t)rma_op,
					pinned_buf->buf,
					pinned_buf->dreg->memhandle[0]->lkey,
					(void*)((uint64_t)win_trait->base.addr + rma_op->target_disp*win_trait->disp_unit),
					win_trait->base.rkey,
					IBV_WR_RDMA_WRITE,
					0,  //send_flags. 
					rma_op->origin_type_info.data_size, //size of data to send
					0 /*immediate data*/);
			JZ_FATAL_IF(mpi_errno, "ibv_post_send failed; mpi_errno = %d.\n", mpi_errno);
			--epoch_trait->nb_rma_ops;
			--epoch->nb_inter_node_rma_ops;
			break;
		case JZ_RMA_WORK_GET:
			prepare_origin_buffer_for_ibv(rma_op);
			pinned_buf = rma_op->status == JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER ?
				rma_op->pinned_buf : &rma_op->pinned_buf_data;
			mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
					JZ_RMA_WORK_GET|(uintptr_t)rma_op,
					pinned_buf->buf,
					pinned_buf->dreg->memhandle[0]->lkey, 
					(void*)((uint64_t)win_trait->base.addr + rma_op->target_disp*win_trait->disp_unit),
					win_trait->base.rkey,
					IBV_WR_RDMA_READ,
					0,  //send_flags. 
					rma_op->origin_type_info.data_size, //size of data to send
					0 /*immediate data*/);
			JZ_FATAL_IF(mpi_errno, "ibv_post_send failed; mpi_errno = %d.\n", mpi_errno);
			--epoch_trait->nb_rma_ops;
			--epoch->nb_inter_node_rma_ops;
			break;
		case JZ_RMA_WORK_ACC:
			post_remote_accumulate(rma_op);
			break;
	}
	if(epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED && 
			epoch_trait->nb_rma_ops == 0 )
	{
		/*Let's free the remote target. Note that this is right after the last rma_op; so WE NEED A SEND_FENCE*/
		post_remote_done_packet(epoch, rma_op->target_rank);
	}
	if(epoch->ss_cur_rma_op)
		D_REMOVE(&epoch->ss_inter_node_rma_op_list.head, &epoch->ss_inter_node_rma_op_list.tail, rma_op);
	else
		D_REMOVE(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
}

#define IS_INTRA_NODE(rma_op)\
	(rma_op->status >= JZ_RMA_STATUS_LOCAL_COPY)


static void build_schedule_helpers(JZ_rma_epoch_t* epoch)
{
	JZ_rma_op_t* rma_op = epoch->rma_op_list.head;
	JZ_rma_op_t* next_rma_op;
	while(rma_op)
	{
		next_rma_op = rma_op->next;
		if(!IS_INTRA_NODE(rma_op))
		{
			D_REMOVE(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
			D_ADD(&epoch->ss_inter_node_rma_op_list.head, 
				&epoch->ss_inter_node_rma_op_list.tail, rma_op);
		}
		rma_op = next_rma_op;
	}
	epoch->ss_cur_rma_op = epoch->ss_inter_node_rma_op_list.head;
}


static inline void post_intra_node_rma(JZ_rma_op_t* rma_op);
static void post_epoch_rma_no_sched_helper(JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait;
	uint32_t scheduling_threshold = GET_SHM_DELAYED_COPY_THRESHOLD();
	if(epoch->close_request && !JZ_double_scheduling)
		scheduling_threshold = JZ_INFINITE;

	JZ_rma_op_t* rma_op = epoch->rma_op_list.head;
	JZ_rma_op_t* next_rma_op;
	JZ_epoch_trait_t *epoch_trait;

	while(rma_op)
	{
		next_rma_op = rma_op->next;
		epoch_trait = rma_op->epoch_trait;
		if(IS_INTRA_NODE(rma_op))
		{
			if(rma_op->origin_type_info.data_size > scheduling_threshold)
			{
				if(!(epoch->close_request || epoch->is_flushing_all) ||
					epoch->nb_inter_node_rma_ops) 
				{
					rma_op = next_rma_op;
					continue;
				}
			}
			
			if(epoch_trait->has_access || update_access(win_ptr, epoch_trait))
				post_intra_node_rma(rma_op);
		}
		else
		{
			if(epoch_trait->has_access || update_access(win_ptr, epoch_trait))
			{
				win_trait = &win_ptr->win_traits[rma_op->target_rank];
				if(win_trait->vc->mrail.rails[0].send_wqes_avail && 
						rma_op->status != JZ_RMA_POSTED_BUT_WAITING_FOR_REMOTE_BUF)
					post_inter_node_rma(win_ptr, win_trait, epoch, epoch_trait, rma_op);
			}
		}
		rma_op = next_rma_op;
	}

	if(epoch->just_closed_rma_batch) //SCARY_STUFF: The epoch could be conceptually deleted (i.e in epoch cache)
	{
		epoch->just_closed_rma_batch = FALSE;
		if(scheduling_threshold != JZ_INFINITE && epoch->nb_inter_node_rma_ops)
		{
			//JZ_DEEP_TRACE("About to build schedule_helpers\n");
			build_schedule_helpers(epoch);
		}
	}
}

static int try_post_open_inter_nodes(JZ_rma_epoch_t* epoch, 
	int nb_blocked_scans_to_withstand)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait;
	JZ_epoch_trait_t* epoch_trait;

	JZ_rma_op_t* next_rma_op;
	JZ_rma_op_t* rma_op = epoch->ss_cur_rma_op;
	while(rma_op && nb_blocked_scans_to_withstand)
	{
		next_rma_op = rma_op->next;
		epoch_trait = rma_op->epoch_trait;
		if(epoch_trait->has_access || update_access(win_ptr, epoch_trait))
		{
			win_trait = &win_ptr->win_traits[rma_op->target_rank];
			if(win_trait->vc->mrail.rails[0].send_wqes_avail)
			{
				if(rma_op->status != JZ_RMA_POSTED_BUT_WAITING_FOR_REMOTE_BUF)
					post_inter_node_rma(win_ptr, win_trait, epoch, epoch_trait, rma_op);
			}
		}
		else
			--nb_blocked_scans_to_withstand;
		rma_op = next_rma_op;
	}

	epoch->ss_cur_rma_op = rma_op ? rma_op : 
		epoch->ss_inter_node_rma_op_list.head; //loop back is required!;
}

static void post_epoch_rma_with_sched_helper(JZ_rma_epoch_t* epoch)
{
	int i;
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait;
	JZ_epoch_trait_t* epoch_trait;

	try_post_open_inter_nodes(epoch, UINT_MAX);

	if(!epoch->nb_intra_node_rma_ops)
		return;
	JZ_rma_op_t* rma_op = epoch->rma_op_list.head;
	JZ_rma_op_t *next_rma_op;
	uint32_t size_so_far = 0;
	int inter_node_scan_index = 0;
	while(rma_op)
	{
		next_rma_op = rma_op->next;
		epoch_trait = rma_op->epoch_trait;
		if(epoch_trait->has_access || update_access(win_ptr, epoch_trait))
		{
			size_so_far += rma_op->origin_type_info.data_size;
			post_intra_node_rma(rma_op);
			if(epoch->ss_cur_rma_op && 
				size_so_far >= JZ_ds_size_threshold_for_poll)
			{
				size_so_far = 0;
				try_post_open_inter_nodes(epoch, JZ_ds_failed_scan_size);
			}
		}
		rma_op = next_rma_op;
	}
}

static inline void post_win_rma(JZ_MPIDI_win_t *win_ptr)
{
	JZ_win_trait_t* win_trait;
	JZ_rma_epoch_t* epoch = win_ptr->active_epoch_head;
	JZ_rma_epoch_t* next_epoch;
    JZ_rma_op_t* next_rma_op = NULL;
	JZ_epoch_trait_t* epoch_trait;

	while(epoch)
	{
		if(!epoch->is_active || (!epoch->rma_op_list.head && !epoch->ss_cur_rma_op))
		{
			epoch = epoch->next;
			continue;
		}

		next_epoch = epoch->next;
		if(epoch->ss_cur_rma_op)
			post_epoch_rma_with_sched_helper(epoch);
		else
			post_epoch_rma_no_sched_helper(epoch);

		epoch = next_epoch;
	}
}

static inline BOOL can_close_epoch(JZ_rma_epoch_t* epoch)
{
	switch(epoch->epoch_side)
	{
		case JZ_EPOCH_SIDE_ORIGIN:
			return can_close_access_epoch(epoch);
		case JZ_EPOCH_SIDE_ORIGIN_AND_TARGET:
			return can_close_fence_epoch(epoch);
		case JZ_EPOCH_SIDE_TARGET:
			return can_close_exposure_epoch(epoch);
		default:
			JZ_WAIT_FOR_DEBUGGER();
			JZ_FATAL("Unknown epoch side\n");
			return FALSE;
	}
}

static void complete_win_epochs_if_possible(JZ_MPIDI_win_t* win_ptr)
{
	JZ_rma_epoch_t* epoch = win_ptr->active_epoch_head;
	JZ_rma_epoch_t* next_epoch = NULL;
	while(epoch)
	{
		next_epoch = epoch->next;
		if(epoch->rma_done == RMA_DONE_STATUS_ISSUED && epoch->nb_done_packets_to_send)
			notify_done_with_targets_right_away(epoch);
		if(can_close_epoch(epoch))
			close_epoch(epoch);
		epoch = next_epoch;
	}
	open_all_possible_epochs(win_ptr);
}


/*
   This routine actually processes communications for all win_ptr. Though, if
   until_completion is TRUE, it exists as soon as the specific win_ptr in argument
   is done for its communication.
   */

void  JZ_RMA_process_remote_communications( struct ibv_wc* pwc /*Could be NULL*/)
{
	if(remote_progress_nest_level && pwc == NULL)
		return;
    int mpi_errno = MPI_SUCCESS;

    if(!JZ_active_win_head)
       return;

	++remote_progress_nest_level;
	BOOL recursing = remote_progress_nest_level > 1;

    JZ_rma_op_t *rma_op = NULL, *temp_rma_op = NULL;
    struct ibv_wc wc;
    int nb_cqe = pwc ? 1 : 0;
    JZ_ibv_work_type_t completion_type;
    JZ_pinned_buf_t* pinned_buf = NULL;  
    BOOL is_send_completion;
    JZ_win_trait_t* win_trait = NULL;
	JZ_rma_epoch_t* epoch = NULL;
	JZ_epoch_trait_t* epoch_trait = NULL;

    JZ_MPIDI_win_t* temp_win_ptr = NULL;
    JZ_unknown_packet_info_t *unknown_packet_info_in = NULL;
	JZ_fast_notification_t* fast_notif;
	int some_rank;

    struct MPIDI_VC *vc = NULL;

    JZ_lock_request_t* lock_request;

	if(nb_cqe)
		memcpy(&wc, pwc, sizeof(struct ibv_wc));

	//deal with CQE
	for(;;)
	{
		if(nb_cqe == 0)
		{
			nb_cqe = ibv_poll_cq(JZ_cq, 1, &wc);
			JZ_DEEP_TRACE_IF(nb_cqe == 1 && wc.status != IBV_WC_SUCCESS, "DIAGNOSTIC:\n%s\n", JZ_get_str_wc(&wc));
			JZ_WAIT_FOR_DEBUGGER_IF(nb_cqe == 1 && wc.status != IBV_WC_SUCCESS);
			//JZ_DEEP_TRACE_ON_MASK_IF(JZ_AFTER_INIT_MASK, nb_cqe == 1, "CQE WITH wc.wr_id = 0x%016llX\n", wc.wr_id);
			//JZ_DEEP_TRACE_IN_FILE("Got wc %p from the device\n", &wc);
		}
		JZ_WAIT_FOR_DEBUGGER_ON_MASK(JZ_IBV_CQ_MASK);
		JZ_FATAL_IF(nb_cqe < 0, "ibv_poll_cq returns -1\n");
		if(nb_cqe == 0)
		{
			if(recursing)
			{
				--remote_progress_nest_level;
				return;
			}
			break;
		}
		nb_cqe = 0;
		JZ_FATAL_IF(wc.status != IBV_WC_SUCCESS, "DIAGNOSTIC:\n%s\n", JZ_get_str_wc(&wc));

		is_send_completion = (wc.opcode == IBV_WC_SEND
				|| wc.opcode == IBV_WC_RDMA_WRITE
				|| wc.opcode == IBV_WC_RDMA_READ);
		if(is_send_completion) 
		{
			completion_type = wc.wr_id & JZ_MASK_RMA_WORK;

			if(completion_type == JZ_NOT_AN_RMA_WORK) //it is a send related to the regular progress engine
			{
				/*JZ_DEEP_TRACE_ON_MASK(JZ_EXTENDED_CQ_MASK, "Enqueuing in JZ_regular_extended_wc_queue_head; wr_id = %p\n", 
				  wc.wr_id); */
				//JZ_DEEP_TRACE_IN_FILE("Enqueuing in JZ_regular_extended_wc_queue_head; wr_id = %p\n", wc.wr_id);
				JZ_enqueue_wc(JZ_regular_extended_wc_queue_head, &wc);  
				//In this case, the send_wqes_avail will be incremented when the regular progress engine will consume the CQE
				continue;
			}

			//JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_RMA_IBV_CQ_MASK, "Got RMA CQE; wr_id = %p\n", wc.wr_id);
			--mv2_MPIDI_CH3I_RDMA_Process.global_used_send_cq;
			switch(completion_type)  // These are RMA CQEs related to sent stuff ... from either side
			{
				case JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS:
				case JZ_RMA_WORK_LOCK_RELEASE:
					epoch_trait = (JZ_epoch_trait_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					epoch = epoch_trait->epoch;
					--epoch->nb_done_packet_completions_to_get;
					win_trait = &epoch->win_ptr->win_traits[epoch_trait->rank];
					--win_trait->is_target;
					++win_trait->vc->mrail.rails[0].send_wqes_avail;
					--win_trait->local_win_ptr->nb_consummed_wqes;
					break;
				case JZ_RMA_WORK_LOCK_GRANT_STATUS:
					pinned_buf = (JZ_pinned_buf_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					win_trait = (JZ_win_trait_t*)pinned_buf->metadata;
					++win_trait->vc->mrail.rails[0].send_wqes_avail;  
					--win_trait->local_win_ptr->nb_consummed_wqes;
					free_pinned_buffer(pinned_buf);
					break;
				case JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST:
				case JZ_RMA_WORK_SHARED_LOCK_REQUEST:
				case JZ_RMA_WORK_EPOCH_POST_STATUS:
					win_trait = (JZ_win_trait_t*)(wc.wr_id & JZ_MASK_ADDRESS); 
					++win_trait->vc->mrail.rails[0].send_wqes_avail;  
					--win_trait->local_win_ptr->nb_consummed_wqes;
					break;
				case JZ_RMA_WORK_BUF_REQUEST:
					rma_op = (JZ_rma_op_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					free_pinned_buffer(rma_op->pinned_buf);
					win_trait = &rma_op->epoch_trait->epoch->win_ptr->win_traits[rma_op->target_rank];
					++win_trait->vc->mrail.rails[0].send_wqes_avail;
					--win_trait->local_win_ptr->nb_consummed_wqes;
					break;
				case JZ_RMA_WORK_BUF_RESPONSE:
					pinned_buf = (JZ_pinned_buf_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					unknown_packet_info_in = (JZ_unknown_packet_info_t*)pinned_buf->buf;
					++unknown_packet_info_in->src_win_ptr->win_traits[unknown_packet_info_in->dest_rank].
						vc->mrail.rails[0].send_wqes_avail;
					--unknown_packet_info_in->src_win_ptr->nb_consummed_wqes;
					//can't free pinned_buf yet ...
					break;
				case JZ_RMA_WORK_1ST_OF_2_STEPS_ACC_DATA_ONLY:
					rma_op = (JZ_rma_op_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					free_pinned_buffer(rma_op->pinned_buf);
					win_trait = &rma_op->epoch_trait->epoch->win_ptr->win_traits[rma_op->target_rank];
					++win_trait->vc->mrail.rails[0].send_wqes_avail;
					--win_trait->local_win_ptr->nb_consummed_wqes;
					//Do nothing for now. Deregistration will happen after JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY fallthrough
					break;
				case JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY:  
				case JZ_RMA_WORK_PUT:
				case JZ_RMA_WORK_GET:
				case JZ_RMA_WORK_SINGLE_STEP_ACC:
					rma_op = (JZ_rma_op_t*)(wc.wr_id & JZ_MASK_ADDRESS);
					epoch_trait = rma_op->epoch_trait;
					epoch = epoch_trait->epoch;
					win_trait = &epoch->win_ptr->win_traits[rma_op->target_rank];
					++win_trait->vc->mrail.rails[0].send_wqes_avail;
					--win_trait->local_win_ptr->nb_consummed_wqes;

					--epoch_trait->nb_rma_ops_to_complete;

					if((!recursing || win_trait->vc->mrail.rails[0].send_wqes_avail > 1)&& 
						epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED && 
							epoch_trait->nb_rma_ops == 0)
					{
						post_remote_done_packet(epoch, rma_op->target_rank);
					}
					//unpin buf/release buf and free rma_op
					if(rma_op->status == JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER)
					{
						if(rma_op->rma_comm_type == JZ_RMA_WORK_GET)
						{
							memcpy(rma_op->origin_addr, rma_op->pinned_buf->buf, 
								rma_op->origin_type_info.data_size);
						}

						free_pinned_buffer(rma_op->pinned_buf);
					}
					else if(rma_op->status == JZ_RMA_STATUS_POSTED)
					{
						dreg_unregister(rma_op->pinned_buf_data.dreg);
						if(rma_op->rma_comm_type == JZ_RMA_WORK_ACC)
							free_pinned_buffer(rma_op->pinned_buf); //the acc metadata
					}
					--epoch->nb_expected_remote_completions;
					if(rma_op->rma_comm_type == JZ_RMA_WORK_GET)
						--epoch->nb_expected_remote_get_completions;
					if(epoch_trait->flush_request_head) 
						update_flush_status(epoch_trait, rma_op->id);
					free_rma_op(rma_op);
					break;
				default:
					JZ_WAIT_FOR_DEBUGGER();  //this should never happen!!!!
			}
		}
		else  //it is a receive completion
		{

			if((wc.wc_flags & IBV_WC_WITH_IMM) && 
				(wc.imm_data == JZ_RMA_FAST_NOTIF_IMM_VALUE || wc.imm_data == JZ_RMA_IMM_VALUE))//it is RMA-related
			{
				JZ_DEEP_TRACE_IN_FILE("Got RMA-related CQE\n");
				 if (mv2_MPIDI_CH3I_RDMA_Process.has_srq) 
				 {
					 pthread_spin_lock(&mv2_MPIDI_CH3I_RDMA_Process.srq_post_spin_lock);
					 --mv2_MPIDI_CH3I_RDMA_Process.posted_bufs[0];
					 if(mv2_MPIDI_CH3I_RDMA_Process.posted_bufs[0] <= rdma_credit_preserve) 
					 {
						 //JZ_DEEP_TRACE_IF(JZ_RANK == 0, "Filling SRQ\n");
						 /* Need to post more to the SRQ */
						 mv2_MPIDI_CH3I_RDMA_Process.posted_bufs[0] +=
							 viadev_post_srq_buffers(viadev_srq_fill_size - 
									 mv2_MPIDI_CH3I_RDMA_Process.posted_bufs[0], 0);

					 }    
					 pthread_spin_unlock(&mv2_MPIDI_CH3I_RDMA_Process.srq_post_spin_lock);

					 /* Check if we need to release the SRQ limit thread */
					 if (mv2_MPIDI_CH3I_RDMA_Process.srq_zero_post_counter[0] >= 1) 
					 { 
						 pthread_mutex_lock(&mv2_MPIDI_CH3I_RDMA_Process.srq_post_mutex_lock[0]);
						 mv2_MPIDI_CH3I_RDMA_Process.srq_zero_post_counter[0] = 0; 
						 pthread_cond_signal(&mv2_MPIDI_CH3I_RDMA_Process.srq_post_cond[0]);
						 pthread_mutex_unlock(&mv2_MPIDI_CH3I_RDMA_Process.srq_post_mutex_lock[0]);
					 }    
				 }

	
				JZ_unknown_packet_info_t *unknown_packet_info_out = NULL;
				vbuf* _vbuf = (vbuf *)((uintptr_t) wc.wr_id);

				JZ_buffer_request_packet_t* buffer_request_packet = NULL;
				JZ_buffer_response_packet_t* buffer_response_packet = NULL;
				JZ_acc_data_copy_t* acc_data_copy = NULL;
				JZ_lock_request_t *lock_request = NULL;
				char* temp_buffer = NULL;

				MPI_User_function *uop = NULL;
				int len;
				uint64_t notif_code;
				uint32_t exposure_id;
				uint32_t i;

				if(wc.imm_data == JZ_RMA_IMM_VALUE)
					goto UNKNOW_PACKET_INFO_LABEL;

FAST_NOTIFICATION_LABEL:
				fast_notif = (JZ_fast_notification_t*)_vbuf->buffer;
				temp_win_ptr = fast_notif->dest_win_ptr;
				notif_code = fast_notif->notification;
				some_rank = fast_notif->src_rank;
				//JZ_DEEP_TRACE_IN_FILE("Got fast notif %s \n", JZ_str_fast_notif(fast_notif));
				switch(notif_code)
				{
					case JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS:
						exposure_id = fast_notif->exposure_id;
						epoch = temp_win_ptr->active_epoch_head;
						while(epoch)
						{
							if(epoch->epoch_traits[some_rank].exposure_id == exposure_id)
							{
								--epoch->nb_expected_done;
								break;
							}
							epoch = epoch->next;
						}
						JZ_WAIT_FOR_DEBUGGER_IF(!epoch); 
						JZ_FATAL_IF(!epoch, "Couldn't find the exposure epoch a DONE packet is meant for\n");
						break;
					case JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST:
					case JZ_RMA_WORK_SHARED_LOCK_REQUEST:
						JZ_DEEP_TRACE_IN_FILE("Got lock request from %d\n", some_rank);
						/*send_wqes_avail > 1 is necessary if this function is recursing*/
						if((!recursing || temp_win_ptr->win_traits[some_rank].vc->mrail.rails[0].send_wqes_avail > 1)
							&& (((notif_code == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST) 
							&& temp_win_ptr->granted_lock_type == JZ_LOCK_REQUEST_NONE) ||
								((notif_code == JZ_RMA_WORK_SHARED_LOCK_REQUEST) &&
								 temp_win_ptr->granted_lock_type != JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST))) 
						{
							grant_remote_lock(temp_win_ptr, some_rank, notif_code);
						}
						else
						{
							JZ_DEEP_TRACE_IN_FILE("Enqueueing lock request coming from %d\n", some_rank);
							lock_request = (JZ_lock_request_t*)allocate_lock_request();
							lock_request->next = NULL;
							lock_request->requester_rank = some_rank;
							lock_request->lock_type = notif_code;
							add_node((void**)(&temp_win_ptr->lock_requests.head), 
								(void**)(&temp_win_ptr->lock_requests.tail), lock_request);
						}
						break;
					case JZ_RMA_WORK_LOCK_RELEASE:
						JZ_DEEP_TRACE_IN_FILE("Got lock release from %d\n", some_rank);
						if(!temp_win_ptr->win_traits[some_rank].is_lock_holder)
						{
							JZ_WAIT_FOR_DEBUGGER();
							JZ_FATAL("Unlock request received from a peer that did not lock\n");
						}
						temp_win_ptr->win_traits[some_rank].is_lock_holder = FALSE;
						if(temp_win_ptr->granted_lock_type == JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST)
						{
							temp_win_ptr->exclusive_locking_rank = -1;
							temp_win_ptr->granted_lock_type = JZ_LOCK_REQUEST_NONE;
						}
						else if(--temp_win_ptr->shared_lock_ref_count == 0)
						{
							temp_win_ptr->granted_lock_type = JZ_LOCK_REQUEST_NONE;
						}
						if(!recursing) //TODO: if epoch->nb_large_acc_to_complete != 0, the logic would be wrong. Fix later
							process_lock_requests(temp_win_ptr);
						break;
					default:
						JZ_WAIT_FOR_DEBUGGER();
						JZ_FATAL("Unknown fast notif code is received\n");
				} 
				goto FREE_VBUF_LABEL;
				
UNKNOW_PACKET_INFO_LABEL: //TODO enqueue the wc for itself if recursing is TRUE
				unknown_packet_info_in = (JZ_unknown_packet_info_t*)_vbuf->buffer; 
				switch(unknown_packet_info_in->temp_buf_type)
				{
					case JZ_RMA_WORK_BUF_REQUEST:  
						//The remote origin has asked us RDMA info for accumulate operand transfer
						buffer_request_packet = (JZ_buffer_request_packet_t*)unknown_packet_info_in->content;	

						temp_win_ptr = unknown_packet_info_in->dest_win_ptr;
						win_trait = &temp_win_ptr->win_traits[unknown_packet_info_in->src_rank];

						//prepare to reply
						get_pinned_buffer(
								sizeof(JZ_unknown_packet_info_t) + 
								sizeof(JZ_buffer_response_packet_t) +
								buffer_request_packet->size,
								&pinned_buf);
						unknown_packet_info_out = (JZ_unknown_packet_info_t*)pinned_buf->buf;
						unknown_packet_info_out->temp_buf_type = JZ_RMA_WORK_BUF_RESPONSE;

						unknown_packet_info_out->src_win_ptr = unknown_packet_info_in->dest_win_ptr;
						unknown_packet_info_out->dest_win_ptr = unknown_packet_info_in->src_win_ptr;
						unknown_packet_info_out->src_rank = unknown_packet_info_in->dest_rank;
						unknown_packet_info_out->dest_rank = unknown_packet_info_in->src_rank;

						buffer_response_packet = (JZ_buffer_response_packet_t*)unknown_packet_info_out->content;
						buffer_response_packet->remote_addr = 
							((uint64_t)pinned_buf->buf + sizeof(JZ_unknown_packet_info_t)+ 
								sizeof(JZ_buffer_response_packet_t));

						buffer_response_packet->rkey = pinned_buf->dreg->memhandle[0]->rkey;
						buffer_response_packet->address_of_requesting_rma_op = 
							buffer_request_packet->address_of_requesting_rma_op;
						buffer_response_packet->address_of_pinned_buf = pinned_buf;

						free_ibv_send_q_slot(win_trait, 1);
						--win_trait->vc->mrail.rails[0].send_wqes_avail;
						mpi_errno = JZ_ibv_post_send(temp_win_ptr, 
								win_trait->qp,	
								JZ_RMA_WORK_BUF_RESPONSE|(uintptr_t)pinned_buf,
								pinned_buf->buf,
								pinned_buf->dreg->memhandle[0]->lkey, 
								NULL,
								0,
								IBV_WR_SEND_WITH_IMM,
								0, //send_flags
								sizeof(JZ_unknown_packet_info_t) + sizeof(JZ_buffer_response_packet_t),
								JZ_RMA_IMM_VALUE /*immediate data*/);

						JZ_FATAL_IF(mpi_errno!=MPI_SUCCESS, 
							"JZ_ibv_post_send failed when sending unsolicited buffer response");
						break;
					case JZ_RMA_WORK_BUF_RESPONSE:
						//The remote target has sent us RDMA info for accumulate operand transfer
						buffer_response_packet = (JZ_buffer_response_packet_t*)unknown_packet_info_in->content;
						temp_win_ptr = unknown_packet_info_in->dest_win_ptr;
						win_trait = &temp_win_ptr->win_traits[unknown_packet_info_in->src_rank];
						rma_op = buffer_response_packet->address_of_requesting_rma_op;
						epoch_trait = rma_op->epoch_trait;
						epoch = epoch_trait->epoch;

						free_ibv_send_q_slot(win_trait, 2);

						//Prepare and send the data. 
						rma_op->pinned_buf_data.dreg = dreg_register(rma_op->origin_addr, rma_op->origin_type_info.data_size);
						JZ_FATAL_IF(!rma_op->pinned_buf_data.dreg, 
								"Memory registration failed in JZ_RMA_process_remote_communications.\n" );
						rma_op->pinned_buf_data.buf = rma_op->origin_addr;
						rma_op->status = JZ_RMA_STATUS_POSTED;

						--win_trait->vc->mrail.rails[0].send_wqes_avail;						
						mpi_errno = JZ_ibv_post_send(temp_win_ptr, win_trait->qp,
								JZ_RMA_WORK_1ST_OF_2_STEPS_ACC_DATA_ONLY|(uintptr_t)rma_op,
								rma_op->pinned_buf_data.buf,
								rma_op->pinned_buf_data.dreg->memhandle[0]->lkey, 
								(void*)buffer_response_packet->remote_addr,
								buffer_response_packet->rkey,
								IBV_WR_RDMA_WRITE,
								0,  //send_flags. 
								rma_op->origin_type_info.data_size, //size of data to send
								0 /*immediate data*/);
						JZ_FATAL_IF(mpi_errno, "JZ_ibv_post_send failed; mpi_errno = %d.\n", mpi_errno);

						//prepare and send the JZ_acc_data_copy_t
						{
							uint32_t total_size = sizeof(JZ_acc_data_copy_t) + sizeof(JZ_unknown_packet_info_t);
							get_pinned_buffer(total_size, &rma_op->pinned_buf);
							unknown_packet_info_out  = (JZ_unknown_packet_info_t*)rma_op->pinned_buf->buf;
							unknown_packet_info_out->temp_buf_type = JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY;

							unknown_packet_info_out->src_win_ptr = unknown_packet_info_in->dest_win_ptr;
							unknown_packet_info_out->dest_win_ptr = unknown_packet_info_in->src_win_ptr;
							unknown_packet_info_out->src_rank = unknown_packet_info_in->dest_rank;
							unknown_packet_info_out->dest_rank = unknown_packet_info_in->src_rank;

							acc_data_copy = (JZ_acc_data_copy_t*)unknown_packet_info_out->content;
							acc_data_copy->data_destination_address = (void*)((uint64_t)win_trait->base.addr + 
									rma_op->target_disp*win_trait->disp_unit);
							acc_data_copy->op = rma_op->acc_op;
							acc_data_copy->builtin_type = rma_op->origin_type_info.eltype;
							acc_data_copy->builtin_type_size = rma_op->origin_type_info.elsize;
							acc_data_copy->address_of_pinned_buf = buffer_response_packet->address_of_pinned_buf;
							acc_data_copy->data_size = rma_op->origin_type_info.data_size;
							acc_data_copy->data[0] = '\0';

							--win_trait->vc->mrail.rails[0].send_wqes_avail;
							mpi_errno = JZ_ibv_post_send(temp_win_ptr, win_trait->qp,
									JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY|(uintptr_t)rma_op,
									unknown_packet_info_out,	
									rma_op->pinned_buf->dreg->memhandle[0]->lkey, 
									NULL,
									0,
									IBV_WR_SEND_WITH_IMM,
									0, //send_flags
									total_size,
									JZ_RMA_IMM_VALUE /*immediate data*/);
							--epoch_trait->nb_rma_ops;  //accs in 2 steps are considered posted at this point
							--epoch->nb_inter_node_rma_ops;
						}
						if(epoch->ss_cur_rma_op)
							D_REMOVE(&epoch->ss_inter_node_rma_op_list.head, &epoch->ss_inter_node_rma_op_list.tail, rma_op);
						else
							D_REMOVE(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
						if(epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED &&
								epoch_trait->nb_rma_ops == 0)
						{
							post_remote_done_packet(epoch,  rma_op->target_rank);
						}
						break;
					case JZ_RMA_WORK_SINGLE_STEP_ACC:
					case JZ_RMA_WORK_2ND_OF_2_STEPS_ACC_CTRL_ONLY:
						acc_data_copy = (JZ_acc_data_copy_t*)unknown_packet_info_in->content;
						pinned_buf = acc_data_copy->address_of_pinned_buf;
						if(pinned_buf) //large data
							temp_buffer = (void*)((uintptr_t)pinned_buf->buf + sizeof(JZ_unknown_packet_info_t) 
									+ sizeof(JZ_buffer_response_packet_t));
						else  //small data
							temp_buffer = acc_data_copy->data;

						uop = MPIR_Op_table[acc_data_copy->op%16 - 1];
						len = acc_data_copy->data_size/acc_data_copy->builtin_type_size;
						(*uop)(temp_buffer,
								acc_data_copy->data_destination_address,
								&len,
								&acc_data_copy->builtin_type);

						if(pinned_buf)
						{
							free_pinned_buffer(pinned_buf);
						}
						break;
					default:
						;//JZ_WAIT_FOR_DEBUGGER();
				}
FREE_VBUF_LABEL:
				//memset(_vbuf->buffer, 0, sizeof(JZ_unknown_packet_info_t));
				MRAILI_Release_vbuf(_vbuf);
			}
			else
			{
				//it is something for the regular progress engine
				//JZ_DEEP_TRACE_IN_FILE("Enqueuing in JZ_regular_extended_wc_queue_head; wr_id = %p\n", wc.wr_id);
				JZ_WAIT_FOR_DEBUGGER_IF(wc.status != IBV_WC_SUCCESS);
				JZ_enqueue_wc(JZ_regular_extended_wc_queue_head, &wc);
			}
		}
	}

	--remote_progress_nest_level;
}


static void post_all_win_rma()
{
	JZ_MPIDI_win_t* temp_win_ptr = JZ_active_win_head;
	while(temp_win_ptr)
	{
		if(temp_win_ptr->active_epoch_head)
		{
			post_win_rma(temp_win_ptr);
			complete_win_epochs_if_possible(temp_win_ptr);
		}
		temp_win_ptr = temp_win_ptr->next;
	}
}

#ifdef REGION_INTRA_NODE_STUFF
static inline void grab_shm_update_notifications(JZ_win_trait_t* win_trait)
{
	JZ_MPIDI_win_t* win_ptr = win_trait->local_win_ptr;
	JZ_pair_of_shm_control_queue_t* shm_queue = &win_trait->shm_queues;
	uint64_t code;
	uintptr_t control;
	uint32_t exposure_id;
	JZ_rma_epoch_t* epoch;
	int rank = win_trait->rank;
	while(try_get_shm_control(shm_queue, &control))
	{
		code = (control & JZ_MASK_RMA_WORK);
		switch(code)
		{
			case JZ_RMA_WORK_EPOCH_ORIGIN_IS_DONE_STATUS:
				exposure_id = (uint32_t)(control & JZ_MASK_ADDRESS);
				epoch = win_ptr->active_epoch_head;
				while(epoch)
				{
					if(epoch->epoch_traits[rank].exposure_id == exposure_id)
					{
						--epoch->nb_expected_done;
						break;
					}
					epoch = epoch->next;
				}
				JZ_WAIT_FOR_DEBUGGER_IF(!epoch);
				JZ_FATAL_IF(!epoch, "Couldn't find the exposure epoch a DONE packet is meant for\n");
				break;
			case JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST:
				++win_trait->backlog_of_xlock;
				++win_ptr->has_shm_passive_target_backlog;
				break;
			case JZ_RMA_WORK_SHARED_LOCK_REQUEST:
				++win_trait->backlog_of_slock;
				++win_ptr->has_shm_passive_target_backlog;
				break;
			case JZ_RMA_WORK_LOCK_RELEASE:
				++win_trait->backlog_of_unlock;
				++win_ptr->has_shm_passive_target_backlog;
				break;
			default:
				JZ_WAIT_FOR_DEBUGGER(); //shouldn't happen
				JZ_FATAL("Unknown code\n");
		}
	}
}

/*Should be called only if win_ptr has no more active epoch*/
static void grab_win_shm_update_notifications(JZ_MPIDI_win_t* win_ptr)
{
	uint32_t i;
	JZ_win_trait_t* win_trait;
	uintptr_t control; 
	for(i=0; i<win_ptr->nb_shm_peers; i++)
	{
		win_trait = &win_ptr->win_traits[win_ptr->shm_peers[i]];
		grab_shm_update_notifications(win_trait);
	}
}

static void direct_rma(JZ_rma_op_t* rma_op)
{
	JZ_epoch_trait_t* epoch_trait = rma_op->epoch_trait;
	JZ_rma_epoch_t* epoch = epoch_trait->epoch;
	JZ_win_trait_t* win_trait = &epoch->win_ptr->win_traits[rma_op->target_rank];
	
	//Type contiguity was checked outside
    //if(rma_op->origin_type_info.is_contig && rma_op->target_type_info.is_contig)	
    {
        switch(rma_op->rma_comm_type)
        {
            case JZ_RMA_WORK_ACC:
                if(rma_op->acc_op != MPI_REPLACE)
                {
                    int count = rma_op->target_type_info.data_size/rma_op->target_type_info.elsize;
					//int count = rma_op->target_count;
                    MPI_User_function *uop = MPIR_Op_table[rma_op->acc_op%16 - 1];
                    (*uop)(rma_op->origin_addr, win_trait->base.addr+rma_op->target_disp*win_trait->disp_unit,
                            &count, &rma_op->target_type_info.eltype);
                    break;
                }
                //else, fallthrough to simply PUT
            case JZ_RMA_WORK_PUT:
                memcpy(win_trait->base.addr+rma_op->target_disp*win_trait->disp_unit, 
                        rma_op->origin_addr, rma_op->origin_type_info.data_size);
                break;
            case JZ_RMA_WORK_GET:
                memcpy(rma_op->origin_addr, 
					win_trait->base.addr+rma_op->target_disp*win_trait->disp_unit, 
                        rma_op->origin_type_info.data_size);
                break;

            default:
                JZ_FATAL("Got rma_op->rma_comm_type = %d", rma_op->rma_comm_type);
        }
		--epoch_trait->nb_rma_ops;
		--epoch_trait->nb_rma_ops_to_complete;
    }
}

static inline void local_copy(JZ_rma_op_t* rma_op)
{
	JZ_epoch_trait_t* epoch_trait = rma_op->epoch_trait;
	JZ_rma_epoch_t* epoch = epoch_trait->epoch;
    int mpi_errno = MPI_SUCCESS;
	int rank = rma_op->target_rank;
	direct_rma(rma_op);

    if(epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED && 
            epoch_trait->nb_rma_ops == 0)
    {
		post_shm_done_packet(epoch, rank);
    }
	if(rma_op->status != JZ_RMA_STATUS_SHM_LOCAL_IMMEDIATE_COPY)
	{
		--epoch->nb_intra_node_rma_ops;
		D_REMOVE(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
		if(epoch_trait->flush_request_head) 
			update_flush_status(epoch_trait, rma_op->id);
	}
    free_rma_op(rma_op);
}

static inline void shm_rdma(JZ_rma_op_t* rma_op)
{
	local_copy(rma_op);
}

static inline void post_intra_node_rma(JZ_rma_op_t* rma_op)
{
	if(rma_op->status == JZ_RMA_STATUS_LOCAL_COPY)
		local_copy(rma_op);
	else 
		shm_rdma(rma_op); 
}

static MPID_Request* process_single_cas(const void *origin_addr, const void *compare_addr,
                           void *result_addr, MPI_Datatype datatype, int target_rank,
                           MPI_Aint target_disp, JZ_rma_epoch_t* epoch);
static void post_epoch_cas(JZ_rma_epoch_t* epoch)
{
	JZ_FATAL("This implementation of post_epoch_cas is faulty! Do not use CAS!\n");
    JZ_rma_op_t* rma_op = epoch->rma_op_list.head;
    JZ_rma_op_t* temp_rma_op = NULL;
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_epoch_trait_t* epoch_trait;
	MPID_Request* cas_request;
    while(rma_op)
	{
		epoch_trait = rma_op->epoch_trait;

		if(!epoch_trait->has_access && !update_access(win_ptr, epoch_trait))
		{
			rma_op = rma_op->next;
			continue;
		}

		cas_request = process_single_cas(rma_op->origin_addr, rma_op->compare_addr, 
				rma_op->result_addr, rma_op->origin_datatype,
				rma_op->target_rank, rma_op->target_disp, rma_op->epoch_trait->epoch);

		--epoch_trait->nb_rma_ops;
		--epoch_trait->nb_cas;
		--epoch->nb_cas;

		if(!cas_request && epoch_trait->flush_request_head) 
			update_flush_status(epoch_trait, rma_op->id);

		temp_rma_op = rma_op;
		rma_op = rma_op->next;
		D_REMOVE(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, temp_rma_op);
		free_rma_op(temp_rma_op);
	}
}

static inline void process_other_communications()
{
    JZ_MPIDI_win_t *win_ptr = JZ_active_win_head;
	JZ_rma_epoch_t* epoch;
	JZ_rma_epoch_t* next;
    while(win_ptr)
    {
		epoch = win_ptr->active_epoch_head;
		while(epoch)
		{
			next = epoch->next;
			if(epoch->nb_cas)
				post_epoch_cas(epoch);
#if 0
			if(epoch->nb_intra_node_rma_ops)
				post_intra_node_epoch_rma(epoch);
#endif
			epoch = next;
		}

		grab_win_shm_update_notifications(win_ptr);
		if(win_ptr->has_shm_passive_target_backlog || win_ptr->lock_requests.head)
			process_lock_requests(win_ptr);

		if(win_ptr->active_epoch_head)
			complete_win_epochs_if_possible(win_ptr);
        win_ptr = win_ptr->next;
    }
}

static void handle_fetch_and_op(JZ_MPIDI_win_t *win_ptr)
{
	//Yet to be implemented
}

static void handle_cas(JZ_MPIDI_win_t *win_ptr)
{
	int64_t origin = win_ptr->message_pump_buffer[0];
	int64_t compare = win_ptr->message_pump_buffer[1];
	int64_t data_size = win_ptr->message_pump_buffer[2];
	uintptr_t address = 
		((uintptr_t)win_ptr->win_traits[win_ptr->rank].base.addr + 
		 (uintptr_t)win_ptr->message_pump_buffer[3]);	
	if(data_size == sizeof(int64_t))
	{
		int64_t data_to_send = *((int64_t*)address);
		if(*((int64_t*)address) == (int64_t)compare)
			*((int64_t*)address) = (int64_t)origin; //swap
		PMPI_Send(&data_to_send, 1, MPI_LONG_LONG, 
				win_ptr->message_pump_status.MPI_SOURCE,
				CAS_REPLY_TAG, win_ptr->cas_and_fao_comm_ptr->handle);
	}
	else if(data_size == sizeof(int32_t))
	{
		int32_t data_to_send = *((int32_t*)address);
		if(*((int32_t*)address) == (int32_t)compare)
			*((int32_t*)address) = (int32_t)origin; //swap
		PMPI_Send(&data_to_send, 1, MPI_INT, 
				win_ptr->message_pump_status.MPI_SOURCE,
				CAS_REPLY_TAG, win_ptr->cas_and_fao_comm_ptr->handle);
	}
	else if(data_size == sizeof(int16_t))
	{
		int16_t data_to_send = *((int16_t*)address);
		if(*((int16_t*)address) == (int16_t)compare)
			*((int16_t*)address) = (int16_t)origin; //swap
		PMPI_Send(&data_to_send, 1, MPI_SHORT, 
				win_ptr->message_pump_status.MPI_SOURCE,
				CAS_REPLY_TAG, win_ptr->cas_and_fao_comm_ptr->handle);
	}
	else
	{
		int8_t data_to_send = *((int8_t*)address);
		if(*((int8_t*)address) == (int8_t)compare)
			*((int8_t*)address) = (int8_t)origin; //swap
		PMPI_Send(&data_to_send, 1, MPI_CHAR, 
				win_ptr->message_pump_status.MPI_SOURCE,
				CAS_REPLY_TAG, win_ptr->cas_and_fao_comm_ptr->handle);
	}
}

static void handle_large_acc(JZ_MPIDI_win_t *win_ptr)
{
	size_t data_size = MPID_Datatype_get_basic_size((MPI_Datatype)win_ptr->message_pump_buffer[1]);
	data_size *= win_ptr->message_pump_buffer[0];
	JZ_pinned_buf_t* pinned_buf;
	get_pinned_buffer(4*sizeof(int64_t) + data_size, &pinned_buf); 
	memcpy(pinned_buf->buf, win_ptr->message_pump_buffer, MESSAGE_PUMP_BUF_SIZE*sizeof(int64_t));

	void* data_addr = (void*)((uintptr_t)pinned_buf->buf + MESSAGE_PUMP_BUF_SIZE*sizeof(int64_t));

	MPI_Request req;
	PMPI_Irecv(data_addr, data_size, MPI_CHAR, win_ptr->message_pump_status.MPI_SOURCE,
			win_ptr->message_pump_buffer[3], win_ptr->large_acc_comm_ptr->handle, &req);

	MPID_Request* request_ptr;
	MPID_Request_get_ptr(req, request_ptr); 
	request_ptr->JZ_data = pinned_buf;

	int rank = win_ptr->message_pump_status.MPI_SOURCE;
	int64_t exposure_id = win_ptr->message_pump_buffer[4];
	JZ_rma_epoch_t* epoch;
	epoch = win_ptr->active_epoch_head;
	while(epoch)
	{
		if(epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL)
		{
			if(epoch->epoch_traits->exposure_id == exposure_id)
			{
				assert(epoch->epoch_traits->rank == rank);
				++epoch->nb_large_acc_to_complete;
				request_ptr->JZ_data_1 = (uintptr_t)epoch;
				break;
			}
		}
		else
		{
			if(epoch->epoch_traits[rank].exposure_id == exposure_id)
			{
				++epoch->nb_large_acc_to_complete;
				request_ptr->JZ_data_1 = (uintptr_t)epoch;
				break;
			}
		}
		epoch = epoch->next;
	}
	JZ_WAIT_FOR_DEBUGGER_IF(!epoch); 
	JZ_FATAL_IF(!epoch, "Couldn't find the exposure epoch \n");

	add_req(&win_ptr->target_side_acc_req_head, &win_ptr->target_side_acc_req_tail,
			request_ptr);
}

static void pump_message()
{
	JZ_MPIDI_win_t *win_ptr = JZ_active_win_head;
	int flag;
	MPI_Request req;
	while(win_ptr) 
	{
		MPIR_Test_impl(&win_ptr->message_pump_req, &flag, &win_ptr->message_pump_status);
		if(flag)
		{
			switch(win_ptr->message_pump_status.MPI_TAG)
			{
				case CAS_LISTENER_TAG: handle_cas(win_ptr); break;
				case FETCH_AND_OP_LISTENER_TAG: handle_fetch_and_op(win_ptr); break;
				default:
					if(win_ptr->message_pump_status.MPI_TAG < MPI_ACC_MAX_OP)
						handle_large_acc(win_ptr);
					else
					{
						JZ_WAIT_FOR_DEBUGGER();
						JZ_FATAL("Unknown message pump request\n");
					}
			}
			/*getting ready anew for pump*/
			PMPI_Irecv(win_ptr->message_pump_buffer, MESSAGE_PUMP_BUF_SIZE, MPI_LONG_LONG, 
				MPI_ANY_SOURCE, MPI_ANY_TAG, 
				win_ptr->comm_ptr->handle, &win_ptr->message_pump_req);
		}
		win_ptr = win_ptr->next;
	}
}


static void scan_large_acc_requests()
{
	MPID_Request* req, *temp_req;
	JZ_rma_op_t* rma_op;
	JZ_rma_epoch_t* epoch;
	JZ_epoch_trait_t* epoch_trait;
	JZ_win_trait_t* win_trait;

	JZ_pinned_buf_t* pinned_buf;
	MPI_User_function *uop;
	int64_t *acc_metadata;
	int len;
	MPI_Datatype datatype;

	JZ_MPIDI_win_t *win_ptr = JZ_active_win_head;
	while(win_ptr) 
	{
		req = win_ptr->origin_side_acc_req_head;
		while(req)
		{
			if(req->cc == 0)
			{
				remove_req(&win_ptr->origin_side_acc_req_head,
					&win_ptr->origin_side_acc_req_tail,
					req);
				rma_op = (JZ_rma_op_t*)req->JZ_data;
				epoch_trait = rma_op->epoch_trait;
				win_trait = &epoch_trait->epoch->win_ptr->win_traits[rma_op->target_rank];
				--epoch_trait->nb_rma_ops_to_complete;

				if(win_trait->vc->mrail.rails[0].send_wqes_avail > 1 &&
					epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED &&
					epoch_trait->nb_rma_ops == 0)
				{
					post_remote_done_packet(epoch_trait->epoch, rma_op->target_rank);
				}

				temp_req = req;
				req = req->JZ_next;
				MPID_Request_release(temp_req);
				free_rma_op(rma_op);
			}
			else
				req = req->JZ_next;
		}

		req = win_ptr->target_side_acc_req_head;
		while(req)
		{
			if(req->cc == 0)
			{
				remove_req(&win_ptr->target_side_acc_req_head,
					&win_ptr->target_side_acc_req_tail,
					req);

				pinned_buf = (JZ_pinned_buf_t*)req->JZ_data; 
				acc_metadata = (int64_t*)pinned_buf->buf;

				len = acc_metadata[0];
				datatype = acc_metadata[1];

				uop = MPIR_Op_table[acc_metadata[3]%16 - 1];
				(*uop)((void*)((uintptr_t)pinned_buf->buf + MESSAGE_PUMP_BUF_SIZE*sizeof(int64_t)),
						(void*)(uintptr_t)acc_metadata[2],
						&len,
						&datatype);

				free_pinned_buffer(pinned_buf);
				--((JZ_rma_epoch_t*)req->JZ_data_1)->nb_large_acc_to_complete;
				MPID_Request_release(temp_req);
			}
			else
				req = req->JZ_next;
		}

		win_ptr = win_ptr->next;
	}
}


void JZ_RMA_progress_test()
{
	if(progress_test_recursing)
		return;
	progress_test_recursing = TRUE;
    if(JZ_cq)
	{
        JZ_RMA_process_remote_communications(NULL);
#if 0
		pump_message();
#endif
		scan_large_acc_requests();
	}

	post_all_win_rma();

	process_other_communications();
	progress_test_recursing = FALSE;
}

#endif //REGION_INTRA_NODE_STUFF


static inline void* get_shm_win_base_addr(JZ_MPIDI_win_t *win_ptr, int shm_position)
{
	return (void*)((uintptr_t)(win_ptr->shm_for_win_allocate.buffer)+
		win_ptr->buf_size*shm_position);
}

int get_number_of_combinations(int set_size, int combinzation_size)
{
	assert(set_size >= combinzation_size);
	int factorial_comb_size = 1;
	int i;
	int nb_comb = 1;
	int limit = set_size - combinzation_size;
	while(set_size > limit)
	{
		nb_comb *= set_size;
		--set_size;
	}
	while(combinzation_size > 1)
	{
		factorial_comb_size *= combinzation_size;
		--combinzation_size;
	}
	return nb_comb/factorial_comb_size;
}

static void* allocate_shared(JZ_MPIDI_win_t *win_ptr, size_t root_win_addr,
	JZ_shm_control_queue_t** shm_control_queue_bufs, uint32_t** all_access_controls)
{
	int comm_size = win_ptr->comm_ptr->local_size;
	uint32_t rank = (uint32_t)win_ptr->comm_ptr->rank;
	char shm_name[256];
	uint32_t i;
	int min_shm_rank = INT_MAX;
	int max_shm_rank = -1;
	int nb_shm_peers = 0;
	int my_shm_position = 0;


	struct MPIDI_VC* vc;
	for(i=0; i<(uint32_t)comm_size; i++)
	{
		MPIDI_Comm_get_vc(win_ptr->comm_ptr, i, &vc);
		if(vc->smp.local_rank >= 0)
		{
			min_shm_rank = MIN(min_shm_rank, i);
			max_shm_rank = MAX(max_shm_rank, i);
			if(rank == i)
				my_shm_position = nb_shm_peers;
			++nb_shm_peers;
		}
	}
	uint32_t access_control_size = comm_size*sizeof(uint32_t)*nb_shm_peers;

	*shm_control_queue_bufs = NULL;
	win_ptr->nb_shm_peers = nb_shm_peers;

	if(nb_shm_peers < 2) //no shm peer but the proc itself
	{
		//JZ_DEEP_TRACE("Performing vanilla allocation; nb_shm_peers = %d\n", nb_shm_peers);
		 win_ptr->access_controls = calloc(comm_size, sizeof(uint32_t));
		 *all_access_controls = win_ptr->access_controls;
		return malloc(win_ptr->buf_size);
	}

	//make sure win_ptr->buf_size is a multiple of word size
	win_ptr->buf_size += (win_ptr->buf_size%sizeof(void*));

	sprintf(win_ptr->shm_for_win_allocate.name, 
		"/%s_%p_%d_%d", SHM_NAME_ROOT, root_win_addr, min_shm_rank, max_shm_rank);

	win_ptr->shm_for_win_allocate.size = win_ptr->buf_size*win_ptr->nb_shm_peers; 

	uintptr_t data_size = (uintptr_t)win_ptr->shm_for_win_allocate.size;

	win_ptr->shm_for_win_allocate.size += 
		(sizeof(JZ_shm_control_queue_t) * 2 * get_number_of_combinations(nb_shm_peers, 2)
		+ access_control_size); 
	JZ_create_shared_mem(&win_ptr->shm_for_win_allocate);

	*all_access_controls = 
		(uint32_t*)((uintptr_t)win_ptr->shm_for_win_allocate.buffer + data_size);

	*shm_control_queue_bufs = (JZ_shm_control_queue_t*)
		((uintptr_t)(*all_access_controls) + access_control_size);
	
	win_ptr->access_controls = (*all_access_controls + my_shm_position*comm_size);

	return get_shm_win_base_addr(win_ptr, my_shm_position);
}

typedef struct JZ_combination_t
{
	int lower;
	int higher;
}JZ_combination_t;

JZ_combination_t* enumerate_combinations(int* ranks, int nb_ranks, int nb_combinations)
{
	JZ_combination_t* combinations = malloc(nb_combinations*sizeof(JZ_combination_t));
	uint32_t i;
	uint32_t j;
	uint32_t comb_index = 0;
	for(i=0; i<nb_ranks; i++)
	{
		for(j=i+1; j<nb_ranks; j++)
		{
			combinations[comb_index].lower = ranks[i];
			combinations[comb_index].higher = ranks[j];
			++comb_index;
		}
	}
	return combinations;
}

static inline uint32_t get_shm_control_fifo_index(int nb_combinations, 
	JZ_combination_t* all_combinations, JZ_combination_t two_peer_combination)
{
	uint32_t i; 
	for(i=0; i<nb_combinations; i++)
	{
		if(all_combinations[i].lower == two_peer_combination.lower &&
			all_combinations[i].higher == two_peer_combination.higher)
			return i;
	}
	JZ_FATAL("Couldn't find two_peer_combination\n");
	return UINT_MAX;
}

#define STR_TRUE	"TRUE"
#define ST_FALSE	"FALSE"

static inline const char* sbool(BOOL val)
{
	return val ? STR_TRUE : ST_FALSE;
}


static print_win_info(JZ_MPIDI_win_t *win_ptr)
{
	JZ_PRINT("Info of win (%p|%d):\n"
		"\t{win_ptr->overlap_disjoint_targets = %s, \n"
		"\twin_ptr->access_after_access_reorder = %s, \n"
		"\twin_ptr->exposure_after_exposure_reorder = %s, \n"
		"\twin_ptr->exposure_after_access_reorder = %s\n," 
		"\twin_ptr->access_after_exposure_reorder = %s\n}",
		win_ptr, win_ptr->unique_win_id,
		sbool(win_ptr->overlap_disjoint_targets), 
		sbool(win_ptr->access_after_access_reorder), 
		sbool(win_ptr->exposure_after_exposure_reorder), 
		sbool(win_ptr->exposure_after_access_reorder), 
		sbool(win_ptr->access_after_exposure_reorder));
}

static inline int _win_set_info(JZ_MPIDI_win_t *win_ptr, MPI_Info info)
{
	char value[2];
	int flag;
	if(info == MPI_INFO_NULL)
		goto EXIT_LABEL;
	MPI_Info_get(info, "MPI_WIN_OVERLAP_DISJOINT_TARGETS", 2, value, &flag);
	if(flag)
		win_ptr->overlap_disjoint_targets = strcmp(value, "1") == 0;
	MPI_Info_get(info, "MPI_WIN_ACCESS_AFTER_ACCESS_REORDER", 2, value, &flag);
	if(flag)
		win_ptr->access_after_access_reorder = strcmp(value, "1") == 0;
	MPI_Info_get(info, "MPI_WIN_EXPOSURE_AFTER_EXPOSURE_REORDER", 2, value, &flag);
	if(flag)
		win_ptr->exposure_after_exposure_reorder = strcmp(value, "1") == 0;
	MPI_Info_get(info, "MPI_WIN_EXPOSURE_AFTER_ACCESS_REORDER", 2, value, &flag);
	if(flag)
		win_ptr->exposure_after_access_reorder = strcmp(value, "1") == 0;
	MPI_Info_get(info, "MPI_WIN_ACCESS_AFTER_EXPOSURE_REORDER", 2, value, &flag);
	if(flag)
		win_ptr->access_after_exposure_reorder = strcmp(value, "1") == 0;
	win_ptr->info = info;

EXIT_LABEL:
	//print_win_info(win_ptr);
	return MPI_SUCCESS;
}

static int win_create_common(void *base, MPI_Aint size, int disp_unit, MPI_Info info,
        MPID_Comm *comm_ptr, MPI_Win *win )
{
    int mpi_errno = MPI_SUCCESS;
	uint32_t i, k, j, comm_size, rank;
    int errflag = MPI_SUCCESS;
    JZ_win_trait_t* my_trait = NULL;
    size_t root_win_addr;  //this is used as a unique shm filename root
    int* temp_local_node_ranks;
	JZ_shm_control_queue_t* shm_control_queue_bufs = NULL;

	 uint32_t* all_access_controls = NULL;

    comm_size = comm_ptr->local_size;
    rank = comm_ptr->rank;

    JZ_MPIDI_win_t *win_ptr = create_win_object();
    mpi_errno = MPIR_Comm_dup_impl(comm_ptr, &win_ptr->comm_ptr);
    JZ_FATAL_IF(mpi_errno, "MPIR_Comm_dup_impl failed in win_create_common.");
    mpi_errno = MPIR_Comm_dup_impl(comm_ptr, &win_ptr->large_acc_comm_ptr);
    JZ_FATAL_IF(mpi_errno, "MPIR_Comm_dup_impl failed in win_create_common.");
    mpi_errno = MPIR_Comm_dup_impl(comm_ptr, &win_ptr->cas_and_fao_comm_ptr);
    JZ_FATAL_IF(mpi_errno, "MPIR_Comm_dup_impl failed in win_create_common.");

    *win = win_ptr->unique_win_id;

	win_ptr->mem_allocation_type = JZ_RMA_MEM_ALLOC_FROM_WIN_SAME_SIZE; //for now
    win_ptr->rank = rank;
    win_ptr->comm_size = comm_size;
	win_ptr->buf_size = size;

	_win_set_info(win_ptr, info);

    root_win_addr = (size_t)win_ptr;
    MPIR_Bcast_impl(&root_win_addr, 1, JZ_MPI_PTR, 0, comm_ptr, &errflag);

	base = allocate_shared(win_ptr, root_win_addr, &shm_control_queue_bufs,
				&all_access_controls);

    mpi_errno = MPIR_Comm_group_impl(win_ptr->comm_ptr, &win_ptr->grp_ptr);
    JZ_FATAL_IF(mpi_errno, "MPIR_Comm_group_impl failed in win_create_common.");

	uint32_t access_control_size = comm_size*sizeof(uint32_t);

    //PREPARE TRAITS AND EXCHANGE THEM
    win_ptr->win_traits = calloc(comm_size, sizeof(JZ_win_trait_t));
    my_trait = &win_ptr->win_traits[rank];
    my_trait->rank = rank;

    my_trait->base.addr = base;
	my_trait->local_numa_node = local_numa_node;

	my_trait->access_control_info.addr = (void*)win_ptr->access_controls;

    my_trait->size = size;
    my_trait->disp_unit = disp_unit;
    my_trait->remote_win_ptr = win_ptr;

	if(JZ_cq)
	{
		win_ptr->access_control_dreg = dreg_register(win_ptr->access_controls, access_control_size);
		JZ_FATAL_IF(!win_ptr->access_control_dreg, 
				"Memory registration failed in win_create_common. win_ptr->access_control_dreg = %p\n",
				win_ptr->access_control_dreg);
		my_trait->access_control_info.rkey = win_ptr->access_control_dreg->memhandle[0]->rkey;
		if(size > 0)  //JZ_cq not being NULL means that there are potentially inter-node peers
		{
			win_ptr->data_dreg = dreg_register(base, size);
			JZ_FATAL_IF(!win_ptr->data_dreg, "Memory registration failed in win_create_common. win_ptr->data_dreg = %p\n",
					win_ptr->data_dreg);
			my_trait->base.rkey = win_ptr->data_dreg->memhandle[0]->rkey;
		}
	}
	memset(win_ptr->access_controls, 0, access_control_size);

    mpi_errno = MPIR_Allgather_impl(my_trait, sizeof(JZ_win_trait_t), MPI_CHAR, win_ptr->win_traits,
            sizeof(JZ_win_trait_t), MPI_CHAR, comm_ptr, &errflag);
	JZ_FATAL_IF(mpi_errno != MPI_SUCCESS, "MPIR_Allgather_impl failed\n");

    struct MPIDI_VC* vc;
    temp_local_node_ranks=malloc(sizeof(int)*comm_size);

	k = 0;
	BOOL has_shm_peers = win_ptr->nb_shm_peers > 1;
	for(i=0; i<comm_size; i++)
    {
        MPIDI_Comm_get_vc(win_ptr->comm_ptr, i, &vc);
			
        win_ptr->win_traits[i].vc = vc;
        win_ptr->win_traits[i].qp = (vc->mrail.rails? vc->mrail.rails[0].qp_hndl : NULL); //we're using the first rail only
        win_ptr->win_traits[i].local_win_ptr = win_ptr;
		win_ptr->win_traits[i].is_intra_node = win_ptr->win_traits[i].vc->smp.local_rank >= 0;

        if(win_ptr->win_traits[i].is_intra_node)
        {
			if(has_shm_peers)
			{
				win_ptr->win_traits[i].base.addr = get_shm_win_base_addr(win_ptr, k);
			}
			win_ptr->win_traits[i].shm_remote_access_control = all_access_controls + comm_size*k;
            temp_local_node_ranks[k++] = i;
        }
    }
	win_ptr->nb_shm_peers = k;

    /*Create shared memory control fifos*/
    uint32_t remote_rank, lower_rank, higher_rank, temp_rank;
    win_ptr->shm_peers = malloc(sizeof(int)*k);

	if(k == 1)
	{
		win_ptr->shm_peers[0] = rank;
		my_trait->shm_queues.in = my_trait->shm_queues.out =
			(JZ_shm_control_queue_t*)calloc(sizeof(JZ_shm_control_queue_t), 1);
		reset_shm_control(&my_trait->shm_queues);
	}
	else
	{
		int nb_combinations = get_number_of_combinations(win_ptr->nb_shm_peers, 2);

		JZ_combination_t* all_combinations = enumerate_combinations(
			temp_local_node_ranks, k, nb_combinations);

		JZ_combination_t two_peer_combination;
		uint32_t epoch_control_index;

		for(i=0; i<k; i++)
		{
			remote_rank = temp_local_node_ranks[i];
			win_ptr->shm_peers[i] = remote_rank;
			if(remote_rank == rank)
			{
				win_ptr->win_traits[rank].shm_queues.in = 
					win_ptr->win_traits[rank].shm_queues.out =
					(JZ_shm_control_queue_t*)calloc(sizeof(JZ_shm_control_queue_t), 1);
				continue;
			}
			if(rank<remote_rank)
			{
				lower_rank = rank;
				higher_rank = remote_rank;
			}
			else
			{
				lower_rank = remote_rank;
				higher_rank = rank;
			}
			two_peer_combination.lower = lower_rank;
			two_peer_combination.higher = higher_rank;
			epoch_control_index = get_shm_control_fifo_index(nb_combinations, all_combinations, 
				two_peer_combination);
			epoch_control_index *= 2;

			if(rank == lower_rank) //the first producer buffer is always the one of lower rank
			{
				win_ptr->win_traits[remote_rank].shm_queues.in = 
					&shm_control_queue_bufs[epoch_control_index];
				win_ptr->win_traits[remote_rank].shm_queues.out = 
					&shm_control_queue_bufs[epoch_control_index+1];

			}
			else
			{
				win_ptr->win_traits[remote_rank].shm_queues.out = 
					&shm_control_queue_bufs[epoch_control_index];
				win_ptr->win_traits[remote_rank].shm_queues.in = 
					&shm_control_queue_bufs[epoch_control_index+1];
			}
			reset_shm_control(&win_ptr->win_traits[remote_rank].shm_queues);
		}
		free(all_combinations);
	}
	free(temp_local_node_ranks);

    //statuses for self

    win_ptr->exclusive_locking_rank = -1;
	
	/*Create message pump */
	PMPI_Irecv(win_ptr->message_pump_buffer, MESSAGE_PUMP_BUF_SIZE, 
		MPI_LONG_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, 
		win_ptr->comm_ptr->handle, &win_ptr->message_pump_req);

    MPIR_Barrier_impl(win_ptr->comm_ptr, &errflag);  //TODO: Is this barrier really useful?
    add_node((void**)(&JZ_active_win_head), (void**)(&JZ_active_win_tail), win_ptr);

fn_exit:
    return mpi_errno;

fn_fail:
    goto fn_exit;
}


int JZ_Win_create(void *base, MPI_Aint size, int disp_unit, MPI_Info info,
        MPID_Comm *comm_ptr, MPI_Win *win )
{
	JZ_FATAL("Not supported for now. Use MPI_Win_allocate with \"same_size\" set to \"true\"\n");
	return win_create_common(base, size, disp_unit, info, comm_ptr, win);
}

int JZ_Win_allocate(MPI_Aint size, int disp_unit, MPI_Info info, 
		 MPID_Comm *comm_ptr, void* baseptr,  MPI_Win* win)
{
	JZ_rma_mem_allocation_type_t alloc_type = JZ_RMA_MEM_ALLOC_FROM_WIN_SAME_SIZE;
	int ret = MPI_SUCCESS;
	void* mem = NULL;
	char value[11];
	int flag;
	MPI_Info_get(info, "same_size", 10, value, &flag);
	if(!flag || strcmp(value, "true") != 0)
	{
		JZ_FATAL("Not supported for now. Use MPI_Win_allocate with \"same_size\" set to \"true\"\n");
#if 0
		void* mem = malloc(size);
		alloc_type = JZ_RMA_MEM_ALLOC_FROM_WIN;
#endif
	}

	ret = win_create_common(mem, size, disp_unit, info, comm_ptr, win);
	JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(*win);
	assert(win_ptr);
	(*(char**)baseptr) = win_ptr->win_traits[win_ptr->rank].base.addr;
	return ret;
}

int JZ_Win_free(MPI_Win *win)
{
    int errflag = MPI_SUCCESS, k;
    JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(*win);

	//JZ_DEEP_TRACE_ON_ALL_CHANNELS("About to enter MPIR_Barrier_impl in %s\n", __FUNCTION__);
    MPIR_Barrier_impl(win_ptr->comm_ptr, &errflag);  //Make sure that all processes called MPI_Win_free.
    //JZ_DEEP_TRACE_IN_FILE_ON_MASK(JZ_WIN_FREE_MASK, "After the barrier in Win_free\n");

    JZ_WAIT_FOR_DEBUGGER_IF(win_ptr->nb_consummed_wqes);
    JZ_FATAL_IF(win_ptr->nb_consummed_wqes, "A window about to be freed still has some consummed wqes\n");

	//free cas resources
	int64_t dummy[4];
	PMPI_Send(dummy, MESSAGE_PUMP_BUF_SIZE, MPI_LONG_LONG, win_ptr->rank, 
		CAS_LISTENER_TAG, win_ptr->comm_ptr->handle);
	MPIR_Wait_impl(&win_ptr->message_pump_req, MPI_STATUS_IGNORE);

	if(JZ_cq)
	{
		dreg_unregister(win_ptr->access_control_dreg);
		if(win_ptr->data_dreg)
			dreg_unregister(win_ptr->data_dreg);
	}
    free((void*)win_ptr->win_traits[win_ptr->rank].shm_queues.in);

	if(win_ptr->nb_shm_peers > 1)
		JZ_delete_shared_mem(&win_ptr->shm_for_win_allocate);
	else
	{
		free(win_ptr->win_traits[win_ptr->rank].base.addr); //because the buf was created from within
		free(win_ptr->access_controls);
	}

    free(win_ptr->win_traits);
	free(win_ptr->shm_peers);

    MPIR_Group_free_impl(win_ptr->grp_ptr);
    MPIR_Comm_free_impl(win_ptr->comm_ptr);
    MPIR_Comm_free_impl(win_ptr->large_acc_comm_ptr);
    MPIR_Comm_free_impl(win_ptr->cas_and_fao_comm_ptr);

    delete_win_object(win_ptr);
    win_ptr = NULL;
    *win = MPI_WIN_NULL;
    return MPI_SUCCESS;
}


static inline BOOL update_access(JZ_MPIDI_win_t* win_ptr, JZ_epoch_trait_t* epoch_trait)
{
	epoch_trait->has_access = 
		epoch_trait->access_id <= win_ptr->access_controls[epoch_trait->rank];
	return epoch_trait->has_access;
}


static int process_put_get(void *origin_addr, int origin_count, MPI_Datatype
        origin_datatype, int target_rank, MPI_Aint target_disp,
        int target_count, MPI_Datatype target_datatype, JZ_MPIDI_win_t *win_ptr,
		JZ_rma_work_type_t work_type, enum ibv_wr_opcode work_code)
{
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;

    int mpi_errno = MPI_SUCCESS;
    if(origin_count == 0)
        goto fn_exit;
    int rank = win_ptr->rank;
    int origin_type_size, target_type_size, target_size;
    MPIDI_msg_sz_t origin_data_sz, target_data_sz;

	JZ_win_trait_t* win_trait = &win_ptr->win_traits[target_rank];
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[target_rank];

    //Create an RMA op object and enqueue it
    JZ_rma_op_t* rma_op = allocate_rma_op();
    rma_op->next = NULL;
	rma_op->id = epoch->rma_id++;
    rma_op->rma_comm_type = work_type;
    rma_op->epoch_trait = epoch_trait;
    rma_op->origin_addr = origin_addr;
    rma_op->target_rank = target_rank;
    rma_op->target_disp = target_disp;
    rma_op->target_count = target_count;
    rma_op->target_datatype = target_datatype;
	rma_op->request = NULL;

	++epoch_trait->nb_rma_ops;
	++epoch_trait->nb_rma_ops_to_complete;

    build_type_info(origin_datatype, origin_count, &rma_op->origin_type_info);
    build_type_info(target_datatype, target_count, &rma_op->target_type_info);
    if(!rma_op->origin_type_info.is_contig || !rma_op->target_type_info.is_contig)
    {
		JZ_FATAL("Non-contiguous types not supported for now\n");
    }

	if(epoch->is_active && !epoch_trait->has_access)
		update_access(win_ptr, epoch_trait);

    if(win_trait->is_intra_node)
    {
		if(epoch_trait->has_access && 
			rma_op->origin_type_info.data_size <= GET_SHM_DELAYED_COPY_THRESHOLD())
		{
			rma_op->status = JZ_RMA_STATUS_SHM_LOCAL_IMMEDIATE_COPY;
			if(target_rank != rank)
				shm_rdma(rma_op);
			else
				local_copy(rma_op);
		}
		else
		{
			_UP_DELAYED_SHM();
			/*JZ_DEEP_TRACE_IF(epoch_trait->has_access,
				"Delaying message of size %d due to scheduling \n",  rma_op->origin_type_info.data_size);*/
			++epoch->nb_intra_node_rma_ops; //only here; but not in the if above
			rma_op->status = target_rank != rank ? 
				JZ_RMA_STATUS_SHM_COPY : JZ_RMA_STATUS_LOCAL_COPY ;  
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
		}
        goto fn_exit;
    }
    else
    {
		++epoch->nb_expected_remote_completions;
		if(work_type == JZ_RMA_WORK_GET)
			++epoch->nb_expected_remote_get_completions;
		if(!epoch_trait->has_access)
        {		
			_UP_LATE_REMOTE_TARGET();
			++epoch->nb_inter_node_rma_ops;
            rma_op->status = JZ_RMA_STATUS_WAITING_FOR_REMOTE_EPOCH;
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
            goto fn_exit;
        }
        if(!win_trait->vc->mrail.rails[0].send_wqes_avail)
        {
			_UP_NO_WQE();
			++epoch->nb_inter_node_rma_ops;
            rma_op->status = JZ_RMA_STATUS_WAITING_FOR_WQE_AVAILABILITY;
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
        }
        else
        {
            prepare_origin_buffer_for_ibv(rma_op);
            --win_trait->vc->mrail.rails[0].send_wqes_avail;
            JZ_AD_HOC_TRACE_SEND_WQES_AFTER_DEC(win_trait->vc->mrail.rails[0].send_wqes_avail, target_rank);

			JZ_pinned_buf_t* pinned_buf = rma_op->status == JZ_RMA_STATUS_POSTED_WITH_POOL_BUFFER ?
				rma_op->pinned_buf : &rma_op->pinned_buf_data;

            mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
                    work_type|(uintptr_t)rma_op,
                    pinned_buf->buf,
                   	pinned_buf->dreg->memhandle[0]->lkey, 
                    (void*)((uintptr_t)win_trait->base.addr + rma_op->target_disp*win_trait->disp_unit),
                    win_trait->base.rkey,
                    work_code,
                    0,  //send_flags. 
                    rma_op->origin_type_info.data_size, //size of data to send
                    0 /*immediate data*/);

            JZ_FATAL_IF(mpi_errno, "ibv_post_send failed in put_get; mpi_errno = %d.\n", mpi_errno);
            --epoch_trait->nb_rma_ops;			
        }
    }

fn_exit:
	JZ_RMA_progress_test();
#if 0
    if(epoch->nb_inter_node_rma_ops)
        JZ_RMA_process_remote_communications(NULL); 
#endif
    return mpi_errno;
fn_fail:
    goto fn_exit;
}


int JZ_Put(void *origin_addr, int origin_count, MPI_Datatype
        origin_datatype, int target_rank, MPI_Aint target_disp,
        int target_count, MPI_Datatype target_datatype, JZ_MPIDI_win_t *win_ptr)
{
	return process_put_get(origin_addr, origin_count, origin_datatype, target_rank,
		target_disp, target_count, target_datatype, win_ptr, JZ_RMA_WORK_PUT,
		IBV_WR_RDMA_WRITE);
}

int JZ_Get(void *origin_addr, int origin_count, MPI_Datatype
        origin_datatype, int target_rank, MPI_Aint target_disp,
        int target_count, MPI_Datatype target_datatype, JZ_MPIDI_win_t *win_ptr)
{
	return process_put_get(origin_addr, origin_count, origin_datatype, target_rank,
		target_disp, target_count, target_datatype, win_ptr, JZ_RMA_WORK_GET,
		IBV_WR_RDMA_READ);
}

int JZ_Accumulate(void *origin_addr, int origin_count, MPI_Datatype
        origin_datatype, int target_rank, MPI_Aint target_disp,
        int target_count, MPI_Datatype target_datatype, MPI_Op op,
        JZ_MPIDI_win_t *win_ptr)
{
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;

    int mpi_errno = MPI_SUCCESS;
    if(origin_count == 0)
        goto fn_exit;
    int rank = win_ptr->rank;
    //int origin_predefined, target_predefined;
    int origin_type_size, target_type_size, target_size;
    MPIDI_msg_sz_t origin_data_sz, target_data_sz;

	JZ_win_trait_t* win_trait = &win_ptr->win_traits[target_rank];
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[target_rank];

    //Create an RMA op object and enqueue it
    JZ_rma_op_t* rma_op = allocate_rma_op();
    rma_op->next = NULL;
	rma_op->id = epoch->rma_id++;
    rma_op->rma_comm_type = JZ_RMA_WORK_ACC;
    rma_op->epoch_trait = epoch_trait;
    rma_op->origin_addr = origin_addr;
    rma_op->target_rank = target_rank;
    rma_op->target_disp = target_disp;
    rma_op->target_count = target_count;
    rma_op->target_datatype = target_datatype;
	rma_op->acc_op = op;
	rma_op->request = NULL;

	++epoch_trait->nb_rma_ops;
	++epoch_trait->nb_rma_ops_to_complete;

    build_type_info(origin_datatype, origin_count, &rma_op->origin_type_info);
    build_type_info(target_datatype, target_count, &rma_op->target_type_info);
    if(!rma_op->origin_type_info.is_contig || !rma_op->target_type_info.is_contig)
    {
		JZ_FATAL("Non-contiguous types not supported for now\n");
    }

	if(epoch->is_active && !epoch_trait->has_access)
		update_access(win_ptr, epoch_trait);

    if(win_trait->is_intra_node)
    {
		if(epoch_trait->has_access && 
			rma_op->origin_type_info.data_size <= GET_SHM_DELAYED_COPY_THRESHOLD())
		{
			rma_op->status = JZ_RMA_STATUS_SHM_LOCAL_IMMEDIATE_COPY;
			if(target_rank != rank)
				shm_rdma(rma_op);
			else
				local_copy(rma_op);
		}
		else
		{
			++epoch->nb_intra_node_rma_ops;
			rma_op->status = target_rank != rank ? JZ_RMA_STATUS_SHM_COPY : JZ_RMA_STATUS_LOCAL_COPY ;  
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
		}
        goto fn_exit;
    }
    else
    {
		++epoch->nb_expected_remote_completions;
		++epoch->nb_inter_node_rma_ops; //for ACC, this must always be incremented here
		if(!epoch_trait->has_access)
        {		
            rma_op->status = JZ_RMA_STATUS_WAITING_FOR_REMOTE_EPOCH;
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
            goto fn_exit;
        }
        if(!win_trait->vc->mrail.rails[0].send_wqes_avail)
        {
            rma_op->status = JZ_RMA_STATUS_WAITING_FOR_WQE_AVAILABILITY;
			D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
        }
        else
        {
            --win_trait->vc->mrail.rails[0].send_wqes_avail;
			post_remote_accumulate(rma_op);
        }
    }

fn_exit:
	JZ_RMA_progress_test();
#if 0
    if(epoch->nb_inter_node_rma_ops)
        JZ_RMA_process_remote_communications(NULL); 
#endif
    return mpi_errno;
fn_fail:
    goto fn_exit;
}

static MPID_Request* process_single_cas(const void *origin_addr, const void *compare_addr,
                           void *result_addr, MPI_Datatype datatype, int target_rank,
                           MPI_Aint target_disp, JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	int64_t buf[4];
	int size;
	MPID_Datatype_get_size_macro(datatype, size);
	buf[2] = size;
	buf[3] = target_disp*win_ptr->win_traits[target_rank].disp_unit;
	MPI_Request req;
	MPID_Request* request_ptr;
	if(size == sizeof(int64_t))
	{
		buf[0] = *(int64_t*)origin_addr;
		buf[1] = *(int64_t*)compare_addr;

		//This send should be eager in most cases. No need to make it nonblocking
		PMPI_Send(buf, 4, MPI_LONG_LONG, target_rank, CAS_LISTENER_TAG, 
			win_ptr->comm_ptr->handle); 
		PMPI_Irecv(result_addr, 1, MPI_LONG_LONG, target_rank, CAS_REPLY_TAG, 
			win_ptr->cas_and_fao_comm_ptr->handle, &req);
	}
	else if(size == sizeof(int32_t))
	{
		buf[0] = *(int32_t*)origin_addr;
		buf[1] = *(int32_t*)compare_addr;

		//This send should be eager in most cases. No need to make it nonblocking
		PMPI_Send(buf, 4, MPI_INT, target_rank, CAS_LISTENER_TAG, 
			win_ptr->comm_ptr->handle); 
		PMPI_Irecv(result_addr, 1, MPI_INT, target_rank, CAS_REPLY_TAG, 
			win_ptr->cas_and_fao_comm_ptr->handle, &req);
	}
	else if(size == sizeof(int16_t))
	{
		buf[0] = *(int16_t*)origin_addr;
		buf[1] = *(int16_t*)compare_addr;

		//This send should be eager in most cases. No need to make it nonblocking
		PMPI_Send(buf, 4, MPI_SHORT, target_rank, CAS_LISTENER_TAG, 
			win_ptr->comm_ptr->handle); 
		PMPI_Irecv(result_addr, 1, MPI_SHORT, target_rank, CAS_REPLY_TAG, 
			win_ptr->cas_and_fao_comm_ptr->handle, &req);
	}
	else
	{
		buf[0] = *(int8_t*)origin_addr;
		buf[1] = *(int8_t*)compare_addr;

		//This send should be eager in most cases. No need to make it nonblocking
		PMPI_Send(buf, 4, MPI_CHAR, target_rank, CAS_LISTENER_TAG,
			 win_ptr->comm_ptr->handle); 
		PMPI_Irecv(result_addr, 1, MPI_CHAR, target_rank, CAS_REPLY_TAG, 
			win_ptr->cas_and_fao_comm_ptr->handle, &req);
	}

	int flag;
	PMPI_Test(&req, &flag, MPI_STATUS_IGNORE);
	if(flag)
	{
		JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
			epoch->epoch_traits : &epoch->epoch_traits[target_rank];
		--epoch_trait->nb_rma_ops_to_complete;

		return NULL;
	}

	MPID_Request_get_ptr(req, request_ptr);
	request_ptr->JZ_data = (void*)(uintptr_t)(epoch->rma_id++);
	request_ptr->JZ_next = NULL;
	request_ptr->JZ_data_1 = target_rank;

	if(epoch->cas_request_tail)
	{
		epoch->cas_request_tail->JZ_next = request_ptr;
		epoch->cas_request_tail = request_ptr;
	}
	else
	{
		epoch->cas_request_head = epoch->cas_request_tail = request_ptr;
	}

	return request_ptr;
}

int JZ_Compare_and_swap(const void *origin_addr, const void *compare_addr,
                           void *result_addr, MPI_Datatype datatype, int target_rank,
                           MPI_Aint target_disp, JZ_MPIDI_win_t *win_ptr)
{
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[target_rank];

	++epoch_trait->nb_rma_ops_to_complete;

	if(epoch->is_active && !epoch_trait->has_access)
		update_access(win_ptr, epoch_trait);
	if(!epoch_trait->has_access)
		goto __QUEUE_CAS;

	process_single_cas(origin_addr, compare_addr, result_addr, datatype,
				target_rank, target_disp, epoch);
	return MPI_SUCCESS;


__QUEUE_CAS:
	++epoch->nb_cas;
	++epoch_trait->nb_cas;
    //Create an RMA op object and enqueue it
    JZ_rma_op_t* rma_op = allocate_rma_op();
    rma_op->next = NULL;
	rma_op->id = epoch->rma_id++;
    rma_op->rma_comm_type = JZ_RMA_WORK_CAS;
    rma_op->epoch_trait = epoch_trait;
    rma_op->origin_addr = (void*)origin_addr;
	rma_op->compare_addr = (void*)compare_addr;
	rma_op->result_addr = result_addr;
    rma_op->origin_datatype = datatype;
    rma_op->target_rank = target_rank;
    rma_op->target_disp = target_disp;
	rma_op->status = JZ_RMA_STATUS_WAITING_FOR_REMOTE_EPOCH;
	rma_op->request = NULL;

	D_ADD(&epoch->rma_op_list.head, &epoch->rma_op_list.tail, rma_op);
	++epoch_trait->nb_rma_ops;
}


static int internal_win_post_impl(MPID_Group *post_grp_ptr, int assert, JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
    uint32_t i, post_grp_size = post_grp_ptr->size;
	int *ranks_in_post_grp;
    int mpi_errno, remote_rank;
	JZ_win_trait_t* win_trait = NULL;;
	JZ_epoch_trait_t* epoch_trait = NULL;
    JZ_UNUSED(assert);

	epoch->is_active = TRUE;

	int* origin_group_ranks = malloc(post_grp_size*sizeof(int)*2);
	ranks_in_post_grp = &origin_group_ranks[post_grp_size];  //temporarily
	for(i=0; i<post_grp_size; i++)
		ranks_in_post_grp[i] = i;
	mpi_errno = MPIR_Group_translate_ranks_impl(post_grp_ptr, post_grp_size, ranks_in_post_grp,
			win_ptr->grp_ptr, origin_group_ranks);
	JZ_FATAL_IF(mpi_errno, "MPIR_Group_translate_ranks_impl failed in JZ_Win_post");

	
	uint32_t* u32_ar = NULL;
	if(JZ_cq)
		u32_ar = (uint32_t*)epoch->access_open_pinned_buf->buf;

	for(i=0; i<post_grp_size; i++)
    {
        remote_rank = origin_group_ranks[i];
		win_trait = &win_ptr->win_traits[remote_rank];
		epoch_trait = &epoch->epoch_traits[remote_rank];
        epoch_trait->exposure_id = ++win_trait->exposure_epoch_counter;
		epoch_trait->epoch = epoch;
		epoch_trait->rank = remote_rank;
        if(!win_trait->is_intra_node)
        {
            /*Make sure we can send stuff before proceeding. The post routine can only exit when all the epochs are opened*/
            free_ibv_send_q_slot(win_trait, 1);

            --win_trait->vc->mrail.rails[0].send_wqes_avail;
            JZ_AD_HOC_TRACE_SEND_WQES_AFTER_DEC(win_trait->vc->mrail.rails[0].send_wqes_avail, remote_rank);

			u32_ar[remote_rank] = win_trait->exposure_epoch_counter;

			mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
					JZ_RMA_WORK_EPOCH_POST_STATUS|((uintptr_t)win_trait),
					&u32_ar[remote_rank],
					epoch->access_open_pinned_buf->dreg->memhandle[0]->lkey,
					((uint32_t*)win_trait->access_control_info.addr + win_ptr->rank),
					win_trait->access_control_info.rkey,
					IBV_WR_RDMA_WRITE,
					0, //send_flags
					sizeof(uint32_t), //size of data to send
					0 /*immediate data*/);
            JZ_FATAL_IF(mpi_errno, "ibv_post_send failed in JZ_Win_post for rank %d with error 0x%X", i, mpi_errno);
        }
        else
        {
			++win_trait->shm_remote_access_control[win_ptr->rank];
        }
    }
	free(origin_group_ranks);
    epoch->epoch_side = JZ_EPOCH_SIDE_TARGET;
	epoch->nb_expected_done = post_grp_size;
    ++JZ_is_rma_in_progress;
fn_exit:
    return mpi_errno;

fn_fail:
    goto fn_exit;
}

static inline int internal_win_post(MPID_Group *post_grp_ptr, int assert, JZ_MPIDI_win_t *win_ptr)
{
	if(can_activate_new_epoch(win_ptr, JZ_EPOCH_TYPE_POST, -1))
	{
		int ret;
		JZ_rma_epoch_t* epoch = create_epoch(win_ptr, JZ_EPOCH_TYPE_POST);
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t)*win_ptr->comm_size);
		ret = internal_win_post_impl(post_grp_ptr, assert, epoch);
		add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), epoch);
		JZ_RMA_progress_test();
		return ret;
	}
	MPIR_Group_add_ref(post_grp_ptr);
	defer_epoch(win_ptr, JZ_EPOCH_TYPE_POST, post_grp_ptr, (void*)(uintptr_t)assert, NULL);
	JZ_RMA_progress_test();
	return MPI_SUCCESS;	
}


int JZ_Win_post(MPID_Group *post_grp_ptr, int assert, JZ_MPIDI_win_t *win_ptr)
{
#if 0
    process_unfinished_passive_target_stuff(win_ptr);
#endif    
    return internal_win_post(post_grp_ptr, assert, win_ptr);
}

int JZ_Win_ipost(MPI_Group group, int assert, MPI_Win win, MPID_Request** request)
{
    JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(win);
    assert(win_ptr);

    MPID_Group *group_ptr = NULL;
    MPID_Group_get_ptr(group, group_ptr);
    *request = make_rma_epoch_opening_request();

    return internal_win_post(group_ptr, assert, win_ptr);  //this function is non-blocking
}


static int internal_win_start_impl(MPID_Group *group_ptr, int assert, JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait;
	JZ_epoch_trait_t* epoch_trait;
    int mpi_errno = MPI_SUCCESS;
    uint32_t i, size = (uint32_t)group_ptr->size;
	int *ranks_in_start_grp, *ranks_in_win_grp;
    JZ_UNUSED(assert);

	epoch->is_active = TRUE;

	ranks_in_start_grp = malloc(size*sizeof(int)); 
	epoch->target_group_ranks = malloc(size*sizeof(int)*3); //quite liberal
	epoch->inter_node_targets = &epoch->target_group_ranks[size];
	epoch->intra_node_targets = &epoch->inter_node_targets[size];
	ranks_in_start_grp = epoch->intra_node_targets;
	for(i=0; i<size; i++)
		ranks_in_start_grp[i] = i;
	mpi_errno = MPIR_Group_translate_ranks_impl(group_ptr, size, ranks_in_start_grp,
			win_ptr->grp_ptr, epoch->target_group_ranks);
	epoch->target_group_size = size;

	for(i=0; i<size; i++)
    {
		int remote_rank = epoch->target_group_ranks[i];

        win_trait = &win_ptr->win_traits[remote_rank];
		epoch_trait = &epoch->epoch_traits[remote_rank];
		epoch_trait->rank = remote_rank;
        epoch_trait->access_id = ++win_trait->access_epoch_counter;
		epoch_trait->epoch = epoch;

        ++win_trait->is_target;
        if(win_trait->is_intra_node)
		{
            epoch->intra_node_targets[epoch->nb_intra_node_targets++] = remote_rank;
		}
		else
		{
			epoch->inter_node_targets[epoch->nb_inter_node_targets++] = remote_rank;
		}
	}
    epoch->epoch_side = JZ_EPOCH_SIDE_ORIGIN;
    epoch->nb_done_packet_completions_to_get = size;
	epoch->nb_done_packets_to_send = size;
    ++JZ_is_rma_in_progress;
    return mpi_errno;
}

static inline int internal_win_start(MPID_Group *group_ptr, int assert, JZ_MPIDI_win_t *win_ptr)
{
	if(can_activate_new_epoch(win_ptr, JZ_EPOCH_TYPE_START, -1))
	{
		int ret;
		JZ_rma_epoch_t* epoch = create_epoch(win_ptr, JZ_EPOCH_TYPE_START);
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t)*win_ptr->comm_size);
		ret = internal_win_start_impl(group_ptr, assert, epoch);
		add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), epoch);
		JZ_RMA_progress_test();
		return ret;
	}
	MPIR_Group_add_ref(group_ptr);
	defer_epoch(win_ptr, JZ_EPOCH_TYPE_START, group_ptr, (void*)(uintptr_t)assert, NULL);
	JZ_RMA_progress_test();
	return MPI_SUCCESS;
}

int JZ_Win_start(MPID_Group *group_ptr, int assert, JZ_MPIDI_win_t *win_ptr)
{
    return internal_win_start(group_ptr, assert, win_ptr);
}


int JZ_Win_istart(MPI_Group group, int assert, MPI_Win win, MPID_Request** request)
{
    JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(win);
    assert(win_ptr);

    MPID_Group *group_ptr = NULL;
    MPID_Group_get_ptr(group, group_ptr);

    int mpi_errno = internal_win_start(group_ptr, assert, win_ptr); 
    *request = make_rma_epoch_opening_request();
    return mpi_errno;
}

static void notify_done_with_targets_right_away(JZ_rma_epoch_t* epoch)
{
	JZ_MPIDI_win_t* win_ptr = epoch->win_ptr;
    uint32_t i;
    JZ_win_trait_t *win_trait = NULL;
	JZ_epoch_trait_t *epoch_trait = NULL;
    int nb_targets = 0;
    int *targets = NULL;

    targets = epoch->target_group_ranks;
    nb_targets = epoch->target_group_size;

	for(i=0; i<nb_targets; i++)
    {
		int t = targets[i];
        win_trait = &win_ptr->win_traits[t];
		epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
			epoch->epoch_traits : &epoch->epoch_traits[t];

        if(epoch_trait->rma_done_status == RMA_DONE_STATUS_REQUESTED && 
                epoch_trait->nb_rma_ops == 0) 
        {
			if(!epoch_trait->has_access && !update_access(win_ptr, epoch_trait))
				continue;
			if(win_trait->is_intra_node)
				post_shm_done_packet(epoch, t);
			else
            {
                post_remote_done_packet(epoch, t);
            }
        }
    }
}

static inline MPID_Request* close_app_level_epoch(JZ_MPIDI_win_t *win_ptr /*=NULL*/, MPI_Win win,
	JZ_rma_epoch_t* epoch, BOOL set_done)
{
	if(!win_ptr)
	{
		win_ptr = JZ_get_win_object(win);
		assert(win_ptr);
	}
    MPID_Request *request = make_rma_epoch_closing_request(win_ptr, epoch);
	if(set_done)
		set_done_flags(epoch);
    JZ_RMA_progress_test();
	return request;
}

int JZ_Win_icomplete(JZ_MPIDI_win_t *win_ptr, MPID_Request** request)
{
	*request = close_app_level_epoch(win_ptr, -1, win_ptr->most_recent_origin_epoch, TRUE);
    return MPI_SUCCESS;
}

int JZ_Win_complete(JZ_MPIDI_win_t *win_ptr)
{
    MPID_Request* request_ptr = NULL;
    int mpi_errno = JZ_Win_icomplete(win_ptr, &request_ptr); 
    MPI_Request request = request_ptr->handle;
    return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
}

int JZ_Win_iwait(JZ_MPIDI_win_t *win_ptr, MPID_Request** request)
{
	*request = close_app_level_epoch(win_ptr, -1, win_ptr->most_recent_target_epoch, FALSE);
    return MPI_SUCCESS;
}


int JZ_Win_wait(JZ_MPIDI_win_t *win_ptr)
{
    MPID_Request* request_ptr = NULL;
    int mpi_errno = JZ_Win_iwait(win_ptr, &request_ptr); 
    MPI_Request request = request_ptr->handle;
    return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
}

int JZ_Win_test(JZ_MPIDI_win_t *win_ptr, int *flag)
{
    JZ_RMA_progress_test();
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_target_epoch;
	if(!epoch)
	{
		*flag = TRUE;
		return;
	}
    *flag = (epoch->nb_expected_done == 0);

	if (*flag && epoch->close_request)
	{
		MPI_Request request = epoch->close_request->handle;
		return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
	}
    return MPI_SUCCESS;
}

static int internal_win_ifence(int assert, JZ_MPIDI_win_t* win_ptr, MPID_Request** request)
{
	JZ_WAIT_ONCE_FOR_DEBUGGER_ON_MASK(JZ_MISC_MASK);

	*request = NULL;
	if(win_ptr->is_fence_epoch_active)
	{
		JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;
		win_ptr->is_fence_epoch_active = FALSE;
		*request = make_rma_epoch_closing_request(win_ptr, epoch);
		set_done_flags(epoch); 
		JZ_RMA_progress_test();
		if((assert & MPI_MODE_NOSUCCEED))
		{
			return MPI_SUCCESS;
		}
	}
	win_ptr->is_fence_epoch_active = TRUE;

	if( *request == NULL)
		*request = make_rma_epoch_opening_request();
	else if((*request)->cc == 0)
	{
		MPID_Request_release(*request);
		*request = make_rma_epoch_opening_request();
	}
	

	if(can_activate_new_epoch(win_ptr, JZ_EPOCH_TYPE_FENCE, -1))
	{
		int ret;
		JZ_rma_epoch_t* epoch = create_epoch(win_ptr, JZ_EPOCH_TYPE_FENCE);
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t)*win_ptr->comm_size);
		ret = internal_win_post_impl(win_ptr->grp_ptr, assert, epoch);
		ret = internal_win_start_impl(win_ptr->grp_ptr, assert, epoch);
		epoch->epoch_side = JZ_EPOCH_SIDE_ORIGIN_AND_TARGET;
		add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), epoch);
		JZ_RMA_progress_test();
		return ret;
	}

	defer_epoch(win_ptr, JZ_EPOCH_TYPE_FENCE, (void*)(uintptr_t)assert, NULL, NULL);
	*request = make_rma_epoch_opening_request();
	JZ_RMA_progress_test();
	return MPI_SUCCESS;
}

int JZ_Win_ifence(int assert, JZ_MPIDI_win_t* win_ptr, MPID_Request** request)
{
	return internal_win_ifence(assert, win_ptr, request);
}

int JZ_Win_fence(int assert, JZ_MPIDI_win_t *win_ptr)
{
    MPID_Request* request_ptr = NULL;
    int mpi_errno = internal_win_ifence(assert, win_ptr, &request_ptr);
    MPI_Request request = request_ptr->handle;
	JZ_WAIT_FOR_DEBUGGER_ON_MASK(JZ_FENCE_MASK);
    return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
}

static int issue_win_lock(int lock_type, int dest, int assert, JZ_rma_epoch_t* epoch)
{
    int mpi_errno = MPI_SUCCESS;
	JZ_MPIDI_win_t *win_ptr = epoch->win_ptr;
	JZ_win_trait_t* win_trait = &win_ptr->win_traits[dest];
    ++win_trait->is_locked;
	JZ_epoch_trait_t* epoch_trait = epoch->epoch_type > JZ_EPOCH_TYPE_LOCK_ALL ? 
		epoch->epoch_traits : &epoch->epoch_traits[dest];

	JZ_WAIT_FOR_DEBUGGER_IF(epoch_trait->has_access);
    if(!win_trait->is_intra_node)
    {
        free_ibv_send_q_slot(win_trait, 1);

        uint64_t req_type = 
            lock_type == MPI_LOCK_EXCLUSIVE ? 
				JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST : JZ_RMA_WORK_SHARED_LOCK_REQUEST;

        --win_trait->vc->mrail.rails[0].send_wqes_avail;						
        JZ_AD_HOC_TRACE_SEND_WQES_AFTER_DEC(win_trait->vc->mrail.rails[0].send_wqes_avail, dest);

		
		JZ_fast_notification_t *fast_notif = epoch->epoch_type == JZ_EPOCH_TYPE_LOCK_ALL?
			&(((JZ_fast_notification_t*)epoch->access_open_pinned_buf->buf)[dest]) : 
			&(((JZ_fast_notification_t*)epoch->access_open_pinned_buf->buf)[0]) ;
		fast_notif->dest_win_ptr = win_trait->remote_win_ptr;
		fast_notif->notification = req_type; 
		fast_notif->exposure_id = 0;
		fast_notif->src_rank = win_ptr->rank;

        mpi_errno = JZ_ibv_post_send(win_ptr, win_trait->qp,
                req_type|(uintptr_t)win_trait,
                fast_notif,
                epoch->access_open_pinned_buf->dreg->memhandle[0]->lkey,	
                NULL,
                0,
                IBV_WR_SEND_WITH_IMM,
                0, //IBV_SEND_FENCE, //send_flags
                sizeof(JZ_fast_notification_t), //size of data to send
       			JZ_RMA_FAST_NOTIF_IMM_VALUE /*immediate data*/);
        JZ_FATAL_IF(mpi_errno, "ibv_post_send failed ...; mpi_errno = %d.\n", mpi_errno);

		epoch->inter_node_targets[epoch->nb_inter_node_targets++] = dest;
    }
    else 
    {
        if(lock_type == MPI_LOCK_EXCLUSIVE)
			set_shm_control(&win_trait->shm_queues, JZ_RMA_WORK_EXCLUSIVE_LOCK_REQUEST);
        else
			set_shm_control(&win_trait->shm_queues, JZ_RMA_WORK_SHARED_LOCK_REQUEST);
        
        epoch->intra_node_targets[epoch->nb_intra_node_targets++] = dest;
        if(dest == win_ptr->rank)
            process_lock_requests(win_ptr);
    }

    ++JZ_is_rma_in_progress;
    epoch_trait->access_id = ++win_trait->access_epoch_counter;
	epoch_trait->epoch = epoch;
	epoch_trait->rank = dest;
    ++win_trait->is_target;

    return MPI_SUCCESS;
}

static int internal_win_lock_impl(int lock_type, int dest, int assert, JZ_rma_epoch_t* epoch)
{ 
	epoch->is_active = TRUE;
	JZ_MPIDI_win_t *win_ptr = epoch->win_ptr;
    epoch->target_group_ranks = malloc(sizeof(int)*2);
    epoch->target_group_ranks[0] = dest;
    epoch->target_group_size = 1;
	if(!win_ptr->win_traits[dest].is_intra_node)
	{
		epoch->inter_node_targets = &epoch->target_group_ranks[1];
	}
	else
	{
		epoch->intra_node_targets = &epoch->target_group_ranks[1];
	}
	epoch->epoch_side = JZ_EPOCH_SIDE_ORIGIN;
    epoch->nb_done_packet_completions_to_get = 1;
	epoch->nb_done_packets_to_send = 1;

    return issue_win_lock(lock_type, dest, assert, epoch);
}


static inline int internal_win_lock(int lock_type, int dest, int assert, JZ_MPIDI_win_t *win_ptr)
{ 
	JZ_epoch_type_t epoch_type = lock_type == MPI_LOCK_EXCLUSIVE ? 
		JZ_EPOCH_TYPE_LOCK_EXCLUSIVE : JZ_EPOCH_TYPE_LOCK_SHARED;
	if(can_activate_new_epoch(win_ptr, epoch_type, dest))
	{
		int ret;
		JZ_rma_epoch_t* epoch = create_epoch(win_ptr, epoch_type);
		memset(&epoch->epoch_traits[0], 0, sizeof(JZ_epoch_trait_t));
		ret = internal_win_lock_impl(lock_type, dest, assert, epoch);
		add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), 
			epoch);
		win_ptr->win_traits[dest].unclosed_lock_epoch = win_ptr->most_recent_origin_epoch;
		JZ_RMA_progress_test();
		return ret;
	}
	defer_epoch(win_ptr, epoch_type, (void*)(uintptr_t)lock_type, 
		(void*)(uintptr_t)dest, (void*)(uintptr_t)assert);
	win_ptr->win_traits[dest].unclosed_lock_epoch = win_ptr->most_recent_origin_epoch;
	JZ_RMA_progress_test();
    return MPI_SUCCESS;
}

int JZ_Win_lock(int lock_type, int dest, int assert, JZ_MPIDI_win_t *win_ptr)
{
	return internal_win_lock(lock_type, dest, assert, win_ptr);
}

int JZ_Win_ilock(int lock_type, int dest, int assert, MPI_Win win, MPID_Request** request)
{
    JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(win);
    assert(win_ptr);

    *request = make_rma_epoch_opening_request();
	return internal_win_lock(lock_type, dest, assert, win_ptr);
}

static int internal_win_lock_all_impl(int assert, JZ_rma_epoch_t* epoch)
{
	epoch->is_active = TRUE;
	JZ_MPIDI_win_t *win_ptr = epoch->win_ptr;
    epoch->target_group_size = (uint32_t)win_ptr->comm_size;
    epoch->target_group_ranks = malloc(sizeof(int)*3*win_ptr->comm_size);
	epoch->inter_node_targets = &epoch->target_group_ranks[win_ptr->comm_size];
	epoch->intra_node_targets = &epoch->inter_node_targets[win_ptr->comm_size];
    uint32_t i;
	int ret;
    for(i=0; i<epoch->target_group_size; i++)
    {
        epoch->target_group_ranks[i] = i;
        ret = issue_win_lock(MPI_LOCK_SHARED, i, 0, epoch);
        assert(ret == MPI_SUCCESS); //... I should normally undo all the other locks and return failed status ... but later
    }
    epoch->nb_done_packet_completions_to_get = epoch->target_group_size;
	epoch->nb_done_packets_to_send = epoch->target_group_size;
    epoch->epoch_side = JZ_EPOCH_SIDE_ORIGIN;
    epoch->epoch_type = JZ_EPOCH_TYPE_LOCK_ALL;
    return MPI_SUCCESS;
}


static int internal_win_lock_all(int assert, JZ_MPIDI_win_t *win_ptr)
{
	if(can_activate_new_epoch(win_ptr, JZ_EPOCH_TYPE_LOCK_ALL, -1))
	{
		int ret;
		JZ_rma_epoch_t* epoch = create_epoch(win_ptr, JZ_EPOCH_TYPE_LOCK_ALL);
		memset(epoch->epoch_traits, 0, sizeof(JZ_epoch_trait_t)*win_ptr->comm_size);
		ret = internal_win_lock_all_impl(assert, epoch);
		add_node((void**)(&win_ptr->active_epoch_head), (void**)(&win_ptr->active_epoch_tail), 
			epoch);
		JZ_RMA_progress_test();
		return ret;
	}
	defer_epoch(win_ptr, JZ_EPOCH_TYPE_LOCK_ALL, (void*)(uintptr_t)assert, NULL, NULL);
	JZ_RMA_progress_test();
    return MPI_SUCCESS;
}

int JZ_Win_lock_all(int assert, JZ_MPIDI_win_t *win_ptr)
{
	return internal_win_lock_all(assert, win_ptr);
}

int JZ_Win_ilock_all(int assert, MPI_Win win, MPID_Request** request)
{
    JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(win);
    assert(win_ptr);

    *request = make_rma_epoch_opening_request();
	return internal_win_lock_all(assert, win_ptr);
}

int JZ_Win_iflush(int rank, MPI_Win win, MPID_Request** request)
{
    int mpi_errno = MPI_SUCCESS;
    JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
    assert(win_ptr);
	JZ_rma_epoch_t* epoch = win_ptr->win_traits[rank].unclosed_lock_epoch;
	if(!epoch)
		epoch = win_ptr->most_recent_origin_epoch;
    *request = make_rma_win_flush_request(epoch, rank);

	JZ_RMA_progress_test();
    return mpi_errno;
}

int JZ_Win_iflush_local(int rank, MPI_Win win, MPID_Request** request)
{
    return JZ_Win_iflush(rank, win, request);
}

int JZ_Win_flush(int rank, MPI_Win win)
{
    int i;
    JZ_win_trait_t* win_trait = NULL;
    JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
    assert(win_ptr);
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;
	while(!done_flushing(epoch))
	{
		JZ_RMA_progress_test();
	}
    return MPI_SUCCESS;
}

int JZ_Win_flush_local(int rank, MPI_Win win)
{
    return JZ_Win_flush(rank, win);
}

int JZ_Win_iflush_all(MPI_Win win, MPID_Request** request)
{
    int mpi_errno = MPI_SUCCESS;
    JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
    assert(win_ptr);
    *request = make_rma_win_flush_all_request(win_ptr->most_recent_origin_epoch);
    JZ_RMA_progress_test();
    return mpi_errno;
}

int JZ_Win_iflush_local_all(MPI_Win win, MPID_Request** request)
{
    return JZ_Win_iflush_all(win, request);
}

int internal_win_flush_all(JZ_MPIDI_win_t *win_ptr, BOOL post_done)
{
    int i;
    JZ_win_trait_t* win_trait = NULL;
	JZ_rma_epoch_t* epoch = win_ptr->most_recent_origin_epoch;
	epoch->is_flushing_all = TRUE;
	epoch->just_closed_rma_batch = TRUE;
	while(!done_flushing(epoch))
	{
		JZ_RMA_progress_test();
	}
	epoch->is_flushing_all = FALSE;
    return MPI_SUCCESS;
}

int JZ_Win_flush_all(MPI_Win win)
{
    JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
    assert(win_ptr);
    return internal_win_flush_all(win_ptr, FALSE);
}

int JZ_Win_flush_local_all(MPI_Win win)
{
    return JZ_Win_flush_all(win);
}

int JZ_Win_iunlock(int rank, MPI_Win win, MPID_Request** request)
{
	JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
	assert(win_ptr);
	JZ_rma_epoch_t* epoch = win_ptr->win_traits[rank].unclosed_lock_epoch;
	win_ptr->win_traits[rank].unclosed_lock_epoch = NULL;
	assert(epoch);
    *request  = make_rma_epoch_closing_request(win_ptr, epoch);
	set_done_flags(epoch);
    JZ_RMA_progress_test();
    return MPI_SUCCESS;
}

int JZ_Win_unlock(int dest, JZ_MPIDI_win_t *win_ptr)
{
	MPID_Request *request_ptr = NULL;
	JZ_Win_iunlock(dest, win_ptr->unique_win_id, &request_ptr);
    MPI_Request request = request_ptr->handle;
    return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
}


int JZ_Win_iunlock_all(MPI_Win win, MPID_Request** request)
{
	JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
	assert(win_ptr);
	*request = close_app_level_epoch(win_ptr, -1, win_ptr->most_recent_origin_epoch, TRUE);
    return MPI_SUCCESS;
}

int JZ_Win_unlock_all(MPI_Win win)
{
	JZ_MPIDI_win_t* win_ptr = JZ_get_win_object(win);
	assert(win_ptr);
	MPID_Request *request_ptr = close_app_level_epoch(win_ptr, -1, 
		win_ptr->most_recent_origin_epoch, TRUE);
    MPI_Request request = request_ptr->handle;
    return MPIR_Wait_impl(&request, MPI_STATUS_IGNORE);
}

void JZ_init_rma_stuff()
{
    JZ_cq = mv2_MPIDI_CH3I_RDMA_Process.cq_hndl[0];

	local_numa_node = getnumanode();	
    if(!JZ_cq)
        return;

    JZ_allocate_extended_wc_queues_if_required();
    JZ_pinned_buf_t* pinned_buf;
    get_pinned_buffer(1, &pinned_buf);
    free_pinned_buffer(pinned_buf);  //mitigate registration cost for small stuff

	//JZ_WAIT_FOR_DEBUGGER();
#if 0
	if(errno != 0)
	{
		local_numa_node = 0;
		JZ_DEEP_PRINT_IF(errno != 0, "getnumanode failed! strerror(errno) is %s\n",
				strerror(errno)); 
	}
#endif
}

void JZ_release_rma_stuff()
{
	JZ_rma_op_t *rma_op, *next_rma_op = rma_op_cache_head;
	while(next_rma_op)
	{
		rma_op = next_rma_op;
		next_rma_op = next_rma_op->next;
		free(rma_op);
	}
	JZ_lock_request_t *lock_request, *next_lock_request = lock_request_cache_head;
	while(next_lock_request)
	{
		lock_request = next_lock_request;
		next_lock_request = next_lock_request->next;
		free(lock_request);
	}
	JZ_rma_epoch_t *epoch, *next_epoch = epoch_cache_head;
	while(next_epoch)
	{
		epoch = next_epoch;
		next_epoch = next_epoch->next;
		if(FALSE && JZ_cq && epoch->access_open_pinned_buf)
		{
			/*Don't have to free them. releasing the pinned_buf_pool takes care of everything*/
			free_pinned_buffer(epoch->access_open_pinned_buf);
			free_pinned_buffer(epoch->access_close_pinned_buf);
		}
		free(epoch);
	}
	next_epoch = single_lock_epoch_cache_head;
	while(next_epoch)
	{
		epoch = next_epoch;
		next_epoch = next_epoch->next;
		if(FALSE && JZ_cq && epoch->access_open_pinned_buf)
		{
			/*Don't have to free them. releasing the pinned_buf_pool takes care of everything*/
			free_pinned_buffer(epoch->access_open_pinned_buf);
			free_pinned_buffer(epoch->access_close_pinned_buf);
		}
		free(epoch);
	}
	return;
	if(JZ_regular_extended_wc_queue_head)
		free(JZ_regular_extended_wc_queue_head);
	if(JZ_RMA_extended_wc_queue_head)
		free(JZ_RMA_extended_wc_queue_head);
}

int JZ_Win_set_info(MPI_Win win, MPI_Info info)
{
	JZ_MPIDI_win_t *win_ptr = JZ_get_win_object(win);
	return _win_set_info(win_ptr, info);
}

#ifdef JZ_FULL_DEBUG
JZ_MPIDI_win_t* JZ_get_win_object(MPI_Win win) 
{    
	if(JZ_active_win_head->unique_win_id == win) 
		return JZ_active_win_head;
	JZ_MPIDI_win_t* win_ptr = JZ_active_win_head;
	while(win_ptr)
	{    
		if(win_ptr->unique_win_id == win) 
			return win_ptr;
		win_ptr = win_ptr->next;
	}    
	return NULL;
}   
#endif
