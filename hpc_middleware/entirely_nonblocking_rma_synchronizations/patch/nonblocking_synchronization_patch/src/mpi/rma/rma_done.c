#if 0

/* -*- Mode: C; c-basic-offset:4 ; -*- */
/*
 *
 *	Inspired from work in MPICH
 *  Judicael ZOUNMEVO from the PPRL of Dr. Ahmad Afsahi, Queen's University Canada
 *  (C) 20 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include "mpiimpl.h"
#include "rma.h"

/* -- Begin Profiling Symbol Block for routine MPI_Rma_done */
#if defined(HAVE_PRAGMA_WEAK)
#pragma weak MPI_Rma_done = PMPI_Rma_done
#elif defined(HAVE_PRAGMA_HP_SEC_DEF)
#pragma _HP_SECONDARY_DEF PMPI_Rma_done  MPI_Rma_done
#elif defined(HAVE_PRAGMA_CRI_DUP)
#pragma _CRI duplicate MPI_Rma_done as PMPI_Rma_done
#endif
/* -- End Profiling Symbol Block */

/* Define MPICH_MPI_FROM_PMPI if weak symbols are not supported to build
   the MPI routines */
#ifndef MPICH_MPI_FROM_PMPI
#undef MPI_Rma_done
#define MPI_Rma_done PMPI_Rma_done

#endif

#undef FUNCNAME
#define FUNCNAME MPI_Rma_done

/*@
   MPI_Rma_done - 

   Input Parameter:
. win - window object (handle) 

.N ThreadSafe

.N Fortran

.N Errors
.N MPI_SUCCESS
.N MPI_ERR_WIN
.N MPI_ERR_OTHER
@*/
int MPI_Rma_done(MPI_Win win, int target)
{
	JZ_MPIDI_win_t* jz_win_ptr = JZ_get_win_object(win);
	assert(jz_win_ptr);
	return JZ_Rma_done(jz_win_ptr, target);

}


/* -- Begin Profiling Symbol Block for routine MPI_Rma_done_all */
#if defined(HAVE_PRAGMA_WEAK)
#pragma weak MPI_Rma_done_all = PMPI_Rma_done_all
#elif defined(HAVE_PRAGMA_HP_SEC_DEF)
#pragma _HP_SECONDARY_DEF PMPI_Rma_done_all  MPI_Rma_done_all
#elif defined(HAVE_PRAGMA_CRI_DUP)
#pragma _CRI duplicate MPI_Rma_done_all as PMPI_Rma_done_all
#endif
/* -- End Profiling Symbol Block */

/* Define MPICH_MPI_FROM_PMPI if weak symbols are not supported to build
   the MPI routines */
#ifndef MPICH_MPI_FROM_PMPI
#undef MPI_Rma_done_all
#define MPI_Rma_done_all PMPI_Rma_done_all

#endif

#undef FUNCNAME
#define FUNCNAME MPI_Rma_done_all

/*@
   MPI_Rma_done_all - 

   Input Parameter:
. win - window object (handle) 

.N ThreadSafe

.N Fortran

.N Errors
.N MPI_SUCCESS
.N MPI_ERR_WIN
.N MPI_ERR_OTHER
@*/
int MPI_Rma_done_all(MPI_Win win)
{
	JZ_MPIDI_win_t* jz_win_ptr = JZ_get_win_object(win);
	assert(jz_win_ptr);
	return JZ_Rma_done_all(jz_win_ptr);

}
#endif
