# Prelude
Radix sort is a non-comparative sorting approach suitable for keys made of 
strings of finitely-enumerable values (digits, alphabets, etc.)   It can be
extremely fast; even faster than the (rather universal) well known Nlog(N)
methods. ... but its space complexity is higher!
 
This benchmark implements a distributed LSD radix-sort for 64-bit integers. 


# Prerequisites
MPI-3 is required!


# Building, running and misc info

The benchmark provides multiple data generation types. Two of them (Uniform and
Gaussian) require C++11. If C++11 is not present, the benchmark will be
automatically built without those two data generation distributions.

The benchmark is written for standard-compliant MPI-3.x RMA. However, it also 
has a version of the sort that uses entirely nonblocking MPI one-sided epoch
synchronizations. The entirely nonblocking synchronizations are explained in
[this paper](http://post.queensu.ca/~pprl/papers/SC-2014.pdf); but I'm yet to
cleanse their (huge) source code before making it public. In the meantime, the
command-line switch required to run this benchmark with the nonblocking
synchronizations is disabled.

The Makefile is hard-coded for gcc. Please update it if you have a different
compiler.

To run it, use your favorite MPI distro and first run in help mode (with the -h
switch) to read the usage info (.e.g mpiexec -n 1 ./radix -h). 
	
Enjoy!
