/*Author: Judicael Zounmevo (zounm@linux.com)
 * */
#ifndef __DATA__GEN_H__
#define __DATA__GEN_H__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void generate_uniform(uint64_t* distribution, uint64_t count, uint64_t min, 
		uint64_t max);
void generate_gaussian(uint64_t* distribution, uint64_t count, double mean, 
		double std_dev);

#ifdef __cplusplus
}
#endif
#endif //__DATA__GEN_H__
