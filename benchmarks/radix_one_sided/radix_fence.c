/*Author: Judicael Zounmevo (zounm@linux.com)
 * */

#include "zounm_utils.h"
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <limits.h>

#ifdef DISTRO_DATA_GEN
#include "distro_data_gen.h"
#endif

#if 0
#ifndef __JZ_RMA
inline int MPI_Win_icomplete(MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_iwait(MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_ifence(int assert, MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_iunlock(int rank, MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_iunlock_all(MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_iflush(int rank, MPI_Win win, MPI_Request* request);                                                    
inline int MPI_Win_iflush_local(int rank, MPI_Win win, MPI_Request* request);                                                        
inline int MPI_Win_iflush_all(MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_iflush_local_all(MPI_Win win, MPI_Request* request){return 0;}
inline int MPI_Win_ipost(MPI_Group group, int assert, MPI_Win win, 
MPI_Request* request){return 0;}
inline int MPI_Win_istart(MPI_Group group, int assert, MPI_Win win, 
MPI_Request* request){return 0;}
inline int MPI_Win_ilock(int lock_type, int rank, int assert, MPI_Win win, 
MPI_Request* request){return 0;}
inline int MPI_Win_ilock_all(int assert, MPI_Win win, MPI_Request* request){return 0;}
#endif
#endif

#define CHECK(cond,failure_message,failure_message_cond,failure_return_val)	\
do																			\
{																			\
	if(!(cond))																\
	{																		\
		if((failure_message_cond))											\
		{																	\
			Z_PRINT_ERROR((failure_message)); 								\
		}																	\
		return (failure_return_val);										\
	}																		\
}while(0)

typedef enum data_generation_type_t
{
	DATA_GEN_NO_DISTRO, //The data correspond to no particular distribution
	DATA_GEN_FORWARD_SORTED,
	DATA_GEN_REVERSE_SORTED,
#ifdef DISTRO_DATA_GEN
	DATA_GEN_GAUSSIAN,
	DATA_GEN_UNIFORM,
#endif
}data_generation_type_t;
/*DATA_GEN_NO_DISTRO and DATA_GEN_UNIFORM come in two flavours each.
 * They can be capped if the user specifies a max_key. By default, they are not,
 * in which case their max_key is ULONG_MAX*/

data_generation_type_t data_generation_type;

#ifdef DISTRO_DATA_GEN
uint64_t gaussian_mean = ULLONG_MAX/2;
uint64_t gaussian_std_dev = ULLONG_MAX/2;
#endif

uint32_t nb_iters = 1;
uint32_t nb_omit_iters = 1;
BOOL validate = FALSE;
BOOL nonblocking = FALSE;
BOOL print = FALSE;
BOOL trace = FALSE;
BOOL cache_data_generation = TRUE;
int simulated_job_size = -1;
BOOL verbose_result_display = FALSE;

#define DEFAULT_DATA_GEN_CAP	ULLONG_MAX
uint64_t data_gen_cap = DEFAULT_DATA_GEN_CAP;

#define DEFAULT_APP_DATA_CAP	0x7FFFFFFFFFFFFFFF
uint64_t app_data_cap = DEFAULT_APP_DATA_CAP;  //in bytes

uint32_t radix = 4;
uint32_t log2_radix = 2;
uint64_t radix_mask = 0;
uint32_t log2_nb_keys = 0;
uint64_t nb_keys = 0; //total
uint64_t nb_keys_per_proc = 0;

uint64_t current_mask;

uint64_t *local_keys_0 = NULL;
uint64_t *local_keys_1 = NULL;
uint64_t *cached_generated_data = NULL;

uint32_t* bucket_count_distro = NULL; //host bucket distro
uint32_t* local_bucket_counts = NULL;

char app_name[251];
uint32_t nb_procs = 0;
int rank = -1;

MPI_Win key_win;
MPI_Win bucket_win;
MPI_Info info;

uint64_t current_shift = 0;
uint32_t *local_prefix_sums = NULL;
uint32_t *global_prefix_sums = NULL;
uint32_t *global_bucket_counts = NULL;
uint32_t *global_offsets = NULL;

typedef struct data_relocation_info_t
{
	int rank_0;
	uint32_t offset_in_rank_0;
	uint32_t amount_in_rank_0;
	int rank_1; //if -1 then, the whole data can host in a single rank
	uint32_t offset_in_rank_1;
	uint32_t amount_in_rank_1;
}data_relocation_info_t;

data_relocation_info_t* data_relocation_info = NULL;


void print_usage(const char* app_name)
{
	Z_PRINT_IF(rank == 0,
		"\n\tNOTE: Convention for the help message below is:\n"
		"\t\t[-option_swith option_argument {default_value}] --comments and explanations\n\n"
		"Usage is %s -n log_2_of_nb_keys [options [options] .... ]\n\n"
		"\t[-r log2_radix {2}] -- Must be between 1 and 16 inclusively\n"
		"\t[-v] -- Run validation after radix sort\n"
		"\t[-p] -- Print values after radix sort. Not recommended for large key sets\n"
		"\t[-c nb_iterations {1}] \n"
		"\t[-m MEM_FOOTPRINT_CAP_PER_PROCESS {INFINITE}] -- Cap on app-level overall data\n"
	   "\t\t in GB (floating point). This includes internal data used by the algorithm.\n"
		"\t\tThis will force early termination if the data set per process will be \n"
		"\t\thigher than MEM_FOOTPRINT_CAP_PER_PROCESS\n"
#ifdef __JZ_RMA
		"\t[-i] -- Use nonblocking epochs. This assumes that the MPI implementation provides\n"
		"\t\t full nonblocking epochs. See my 2014 SC paper\n"
#endif
#ifndef DISTRO_DATA_GEN
		"\t[-g data_generation_type {ND}] -- ND=No particular distribution; \n"
		"\t\tFS=Forward sorted; RS=Reverse sorted;\n"
		"\t[-M max_key] -- Max key (Valid only for ND)\n"
#else
		"\t[-g data_generation_type {ND}] -- ND=No particular distribution; \n"
		"\t\tFS=Forward sorted; RS=Reverse sorted; G=Gaussian distribution; \n"
		"\t\tU=Uniform distribution\n"
		"\t[-M max_key] -- Max key (Valid only for ND and U)\n"
		"\t[-u mean_for_G_distro {ULLONG/2}] -- per-process mean parameter for G distro\n"
		"\t[-s std-dev_for_G_distro {ULLONG/2}] -- per-process standard-deviation \n"
		"\t\tparameter for G distro\n"
#endif
		"\t[-C] -- Do not cache data set accross iterations (slower for several iterations)\n"
		"\t[-S job_size] -- Simulate load; display \"expected\" per-process footprint and exit\n"
		"\t[-V] -- Verbose result display (not graph-friendly)\n"
		"\t[-t] -- tracing. Allows local bucket info to be displayed. This is for diagnoses\n"
		"\t[-h] -- Help\n\n", 
		app_name);
}

typedef enum cmd_parse_status_t
{
	CPS_CONTINUE,
	CPS_EXIT_ON_HELP,
	CPS_ERRONEOUS
}cmd_parse_status_t;

extern int opterr;
cmd_parse_status_t parse_cmd_line(int argc, char** argv)
{
	int c;
	double data_cap;
	char param_switches[100];
	strcpy(param_switches, "n:r:vc:m:pg:tM:GS:C:Vh");
#ifdef __JZ_RMA
	strcat(param_switches, "i");
#endif
#ifdef DISTRO_DATA_GEN
	strcat(param_switches, "u:s:");
#endif
	opterr = 0;
	while((c = getopt(argc, argv, param_switches)) != -1)
	{
		switch(c)
		{
			case 'n': log2_nb_keys = atoi(optarg); break;
			case 'r': log2_radix = atoi(optarg); break;
			case 'v': validate = TRUE; break;
			case 'c': nb_iters = atoi(optarg); break;
			case 'm': 
				if(strcmp(optarg, "INIFINITE") == 0)
					break;
				data_cap = atof(optarg);
				app_data_cap = (uint64_t)(data_cap*GB);
				break;
			case 'i': nonblocking = TRUE; break;
			case 'p': print = TRUE; break;
			case 'g':
					  if(strcmp(optarg, "ND") == 0)
						  data_generation_type = DATA_GEN_NO_DISTRO;
					  else if(strcmp(optarg, "FS") == 0)
						  data_generation_type = DATA_GEN_FORWARD_SORTED;
					  else if(strcmp(optarg, "RS") == 0)
						  data_generation_type = DATA_GEN_REVERSE_SORTED;
#ifdef DISTRO_DATA_GEN
					  else if(strcmp(optarg, "U") == 0)
						  data_generation_type = DATA_GEN_UNIFORM;
					  else if(strcmp(optarg, "G") == 0)
						  data_generation_type = DATA_GEN_GAUSSIAN;
#endif
					  else
					  {
						  Z_PRINT_ERROR_IF(rank == 0, "\nUnsupported data generation type\n"); 
						  return CPS_ERRONEOUS;
					  }
					  break;
			case 't': trace = TRUE; break;
			case 'M': data_gen_cap = (uint64_t)atoll(optarg); break;
			case 'C': cache_data_generation = FALSE; break;
			case 'S': simulated_job_size = atoi(optarg); break;
#ifdef DISTRO_DATA_GEN
			case 'u': gaussian_mean = atoll(optarg); break;
			case 's': gaussian_std_dev = atoll(optarg); break;
#endif
			case 'V': verbose_result_display = TRUE; break;
			case 'h': return CPS_EXIT_ON_HELP;
			default: 
					  Z_PRINT_ERROR_IF(rank == 0, "\nUnsupported option detected\n"); 
					  return CPS_ERRONEOUS;
		}
	}
	return CPS_CONTINUE;
}

inline BOOL is_power_of_two(uint64_t val)
{
	return val && !(val & (val -1));
}

BOOL check_parameter_sanity()
{
	nb_keys = 1ULL<<log2_nb_keys;
	CHECK(nb_keys>=nb_procs,
		"log2(nb_keys) (i.e., the parameter n) must be provided and \n"
		"be such that nb_keys is larger than the number of processes",
		rank==0, FALSE);
	CHECK(log2_radix >= 1 &&  log2_radix <= 16,
		"log2_radix must be between 1 and 16 (inclusively)",
		rank==0, FALSE);
	return TRUE;
}

void prepare()
{
	nb_keys_per_proc = nb_keys/nb_procs;
	radix = 1 << log2_radix;
	radix_mask = radix - 1;

	MPI_Info_create(&info);
	MPI_Info_set(info, "same_size", "true");
	MPI_Win_allocate(nb_keys_per_proc*sizeof(uint64_t), sizeof(uint64_t),
		info, MPI_COMM_WORLD, &local_keys_0, &key_win); 

	local_keys_1 = malloc(nb_keys_per_proc*sizeof(uint64_t));

	MPI_Win_allocate(radix*nb_procs*sizeof(uint32_t), sizeof(uint32_t), 
		info, MPI_COMM_WORLD, &bucket_count_distro, &bucket_win);

	local_bucket_counts = &bucket_count_distro[radix*rank];

	local_prefix_sums = malloc(sizeof(uint32_t)*radix);
	global_prefix_sums = malloc(sizeof(uint32_t)*radix);
	global_bucket_counts = malloc(sizeof(uint32_t)*radix);
	global_offsets = malloc(sizeof(uint32_t)*radix);
	data_relocation_info = malloc(sizeof(data_relocation_info_t)*radix);
}

uint64_t data_load = 0;
void compute_data_load()
{
	radix = 1 << log2_radix;
	nb_keys_per_proc = nb_keys/nb_procs;
	data_load = nb_keys_per_proc*sizeof(uint64_t) //local_keys_0
				+ nb_keys_per_proc*sizeof(uint64_t) //local_keys_1
				+ radix*nb_procs*sizeof(uint32_t) // bucket_count_distro
				+ sizeof(uint32_t)*radix //local_prefix_sums
				+ sizeof(uint32_t)*radix //global_prefix_sums
				+ sizeof(uint32_t)*radix //global_bucket_counts
				+ sizeof(uint32_t)*radix //global_offsets
				+ sizeof(uint32_t)*radix //local_positions
				+sizeof(data_relocation_info_t)*radix; //data_relocation_info

	if(cache_data_generation)
		data_load += (nb_keys_per_proc*sizeof(uint64_t));
}

void simulate_data_load()
{
	nb_procs = simulated_job_size;
	compute_data_load();
}



void release()
{
	MPI_Win_free(&key_win);
	MPI_Win_free(&bucket_win);
	MPI_Info_free(&info);

	free(local_keys_1);
	if(cached_generated_data)
		free(cached_generated_data);

	free(local_prefix_sums);
	free(global_prefix_sums);
	free(global_bucket_counts);
	free(global_offsets);
	free(data_relocation_info);
}


/*---DATA GENERATION---*/
void generate_no_distro()
{
	/*The seeds are chosen on purpose for reproducibility*/
	int seed0 = 23 + 1001*rank;
	int seed1 = 'J'*1000 + 'u'*100 + 'd'*10 +'i';
	seed1 += rank;	
	uint64_t i;
	srand(seed0);
	for(i=0; i<nb_keys_per_proc; i++)
		local_keys_0[i] = ((uint64_t)rand())<<31;

	srand(seed1);

	if(data_gen_cap < DEFAULT_DATA_GEN_CAP)
	{
		for(i=0; i<nb_keys_per_proc; i++)
			local_keys_0[i] = (local_keys_0[i] | (uint64_t)rand())%data_gen_cap;
	}
	else
	{
		for(i=0; i<nb_keys_per_proc; i++)
			local_keys_0[i] |= (uint64_t)rand();
	}
}

void generate_forward_sorted()
{
	uint64_t i;
	uint64_t offset = nb_keys_per_proc*rank; 
	for(i=0; i<nb_keys_per_proc; i++)
		local_keys_0[i] = offset + i;
}

void generate_reverse_sorted()
{
	uint64_t i;
	uint64_t offset = nb_keys_per_proc*(nb_procs - rank) - 1; 
	for(i=0; i<nb_keys_per_proc; i++)
		local_keys_0[i] = offset - i;
}

BOOL generate()
{
	if(cached_generated_data)
	{
		memcpy(local_keys_0, cached_generated_data, sizeof(uint64_t)*nb_keys_per_proc);
		return TRUE;
	}
	switch(data_generation_type)
	{
		case DATA_GEN_NO_DISTRO: generate_no_distro(); break;
		case DATA_GEN_FORWARD_SORTED: generate_forward_sorted(); break;
		case DATA_GEN_REVERSE_SORTED: generate_reverse_sorted(); break;
#ifdef DISTRO_DATA_GEN
		case DATA_GEN_UNIFORM: 
			generate_uniform(local_keys_0, nb_keys_per_proc, 0, data_gen_cap);
			break;
		case DATA_GEN_GAUSSIAN:
			generate_gaussian(local_keys_0, nb_keys_per_proc, gaussian_mean,
					gaussian_std_dev);
			break;
#endif
		default:
			Z_PRINT_IF(rank == 0, "Invalid data generation type\n");
			return FALSE;
	}
	if(cache_data_generation)
	{
		cached_generated_data = malloc(sizeof(uint64_t)*nb_keys_per_proc);
		memcpy(cached_generated_data, local_keys_0, sizeof(uint64_t)*nb_keys_per_proc);
	}
	return TRUE;
}

void reset()
{
	//NOOP
}
/*---END DATA GENERATION---*/

void init(int* argc, char*** argv)
{
	MPI_Init(argc, argv);
	strcpy(app_name, (*argv)[0]);
	MPI_Comm_size(MPI_COMM_WORLD, (int*)(&nb_procs));
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
}



#define KEY(u64_val) (((u64_val)&current_mask) >> current_shift)

void build_data_relocation_info(uint32_t amount_to_move,
		uint32_t radix_index, data_relocation_info_t* dri)
{
	uint32_t offset = global_prefix_sums[radix_index] + global_offsets[radix_index];
	dri->rank_0 = offset/nb_keys_per_proc;
	dri->offset_in_rank_0 = offset - dri->rank_0*nb_keys_per_proc;
	uint32_t space_left_in_rank_0 = nb_keys_per_proc - dri->offset_in_rank_0;
	if(space_left_in_rank_0 >= amount_to_move)
	{
		dri->amount_in_rank_0 = amount_to_move;
		dri->rank_1 = -1;
		return;
	}
	dri->amount_in_rank_0 = space_left_in_rank_0;
	dri->amount_in_rank_1 = amount_to_move - space_left_in_rank_0;
	dri->rank_1 = dri->rank_0 + 1;
	dri->offset_in_rank_1 = 0; //always 0 because it starts right after the previous rank
}

void perform_key_based_local_validation()
{
	uint32_t i;
	for(i=1; i<nb_keys_per_proc; i++)
	{
		if(KEY(local_keys_0[i]) < KEY(local_keys_0[i-1]))
		{
			Z_PRINT("[%d], Local validation failed!\n", rank);
			break;
		}
	}
}

void perform_validation()
{
	uint32_t i;
	uint64_t max_of_previous;
	uint32_t valid_count = 1;
	if(rank > 0)
	{
		MPI_Win_lock(MPI_LOCK_SHARED, rank-1, 0, key_win);
		MPI_Get(&max_of_previous, 1, MPI_UNSIGNED_LONG_LONG, rank-1,
			nb_keys_per_proc-1, 1, MPI_UNSIGNED_LONG_LONG, key_win);
		MPI_Win_unlock(rank-1, key_win);
		valid_count = max_of_previous <= local_keys_0[0];
	}
	
	if(valid_count)
	{
		for(i=1; i<nb_keys_per_proc; i++)
		{
			if(local_keys_0[i] < local_keys_0[i-1])
			{
				valid_count = 0;
				Z_PRINT("[%d], Local validation failed\n", rank);
				break;
			}
		}
	}
	else
		Z_PRINT("[%d], Cross-process validation failed\n", rank);

	MPI_Win_fence(MPI_MODE_NOPRECEDE, bucket_win);
	/*Use slot 0 of each bucket to send the validation status*/
	MPI_Put(&valid_count, 1, MPI_UNSIGNED, 0, rank*radix, 1, 
		MPI_UNSIGNED, bucket_win);
	MPI_Win_fence(MPI_MODE_NOSUCCEED, bucket_win);

	if(rank == 0)
	{
		for(i=0; i<nb_procs; i++)
		{
			if(!bucket_count_distro[i*radix])
			{
				Z_PRINT("Validation failed!\n");
				return;
			}
		}
		Z_PRINT("Validation succeeded!\n");
	}
}

FILE* stream_out = NULL;
void init_stream()
{
	if(!trace)
		return;
	char filename[21];
	sprintf(filename, "out_%d", rank);
	stream_out = fopen(filename, "w");
	assert(stream_out);
}

void release_stream()
{
	if(!trace)
		return;
	fclose(stream_out);
}


/*Trace algorithm data at intermediate states.
 * This is for debugging*/
void trace_to_stream(uint32_t round)
{
	if(!trace)
		return;
	fprintf(stream_out, "-------------Tracing round %u ------------\n", round);
	fprintf(stream_out, "current_shift = 0x%lX, current_mask = 0x%lX\n", current_shift, current_mask);
	fprintf(stream_out, "Local buckets:\n");
	uint32_t i, j, k=0;
	for(i=0; i<radix; i++)
	{
		fprintf(stream_out, "Bucket[0x%X] = {nb_items=%u, keys={", i, 
			local_bucket_counts[i]);
		for(j=0; j<local_bucket_counts[i]; j++)
			fprintf(stream_out, "0x%016lX ", local_keys_1[k++]);
		fprintf(stream_out, "}}\n");
	}

	fprintf(stream_out, "global_bucket_counts = {");
	for(i=0; i<radix; i++)
		fprintf(stream_out, "%u ", global_bucket_counts[i]);
	fprintf(stream_out, "}\n");

	fprintf(stream_out, "global_prefix_sums = {");
	for(i=0; i<radix; i++)
		fprintf(stream_out, "%u ", global_prefix_sums[i]);
	fprintf(stream_out, "}\n");

	fprintf(stream_out, "global_offsets = {");
	for(i=0; i<radix; i++)
		fprintf(stream_out, "%u ", global_offsets[i]);
	fprintf(stream_out, "}\n");
	fprintf(stream_out, "\n\n");
}

typedef enum display_base_t
{
	DB_OCTAL = 0xFF00000000000000,
	DB_DECIMAL,
	DB_HEX
}display_base_t;

/*Display function for serialized_stream. See Z_serialized_stream in
 * zounm_utils.h*/
void display_func(void* data, uint32_t data_size, int rank, void* additional_arg)
{
	uint64_t* d = (uint64_t*)data;
	display_base_t display_base = DB_HEX;
	if(additional_arg)
	{
		if((uintptr_t)additional_arg < (uintptr_t)DB_OCTAL)
		{
			uint32_t round = (uint32_t)(uintptr_t)additional_arg;
			Z_PRINT_IF(rank == 0, "- - - - - - - -Round %u - - - - - - - -\n", round);
		}
		else
			display_base = (display_base_t)(uintptr_t)additional_arg;
	}
	
	Z_PRINT("[%d]: ", rank);
	int i;
	int count = data_size/sizeof(uint64_t);
	switch(display_base)
	{
		case DB_OCTAL:
		{
			for(i=0; i<count; i++)
				Z_PRINT("0o%lo ", d[i]);
			break;
		}
		case DB_DECIMAL:
		{
			for(i=0; i<count; i++)
				Z_PRINT("%lu ", d[i]);
			break;
		}
		case DB_HEX:
		{
			for(i=0; i<count; i++)
				Z_PRINT("0x%lX ", d[i]);
			break;
		}
		default:
			assert(FALSE);
	}
	Z_PRINT("\n---------------------------------------------------\n");
}

/*Another diaply func for Z_serialized_stream. See the readme of zounm_utils.h*/
void display_double_func(void* data, uint32_t data_size, int rank, void* additional_arg)
{
	Z_PRINT("[%d]: %0.4lf\n", rank, *(double*)data);
}


/*Sort using standard-compliant MPI-3 RMA*/
void sort()
{
	uint32_t nb_rounds = 64/log2_radix;
	if(nb_rounds*log2_radix < 64)
		++nb_rounds;
	uint32_t round;
	uint32_t *local_positions = malloc(sizeof(uint32_t)*radix);

	uint64_t max_key = 0;
	uint32_t max_round = nb_rounds;

	memset(local_bucket_counts, 0, sizeof(uint32_t)*radix);
	memset(local_prefix_sums, 0, sizeof(uint32_t)*radix);
	memset(global_bucket_counts, 0, sizeof(uint32_t)*radix);
	memset(global_offsets, 0, sizeof(uint32_t)*radix);
	memset(global_prefix_sums, 0, sizeof(uint32_t)*radix);
	for(round = 0; round <= max_round; round++)
	{
		MPI_Win_fence(MPI_MODE_NOPRECEDE, bucket_win);

		current_shift = log2_radix*(round);
		current_mask = (radix_mask << current_shift);

		/*Compute local bucket sizes for current round*/
		uint32_t i;

		if(round == 0)
		{
			for(i=0; i<nb_keys_per_proc; i++)
			{
				max_key = Z_MAX(max_key, local_keys_0[i]);
				local_bucket_counts[KEY(local_keys_0[i])]++;
			}
			MPI_Allreduce(MPI_IN_PLACE, &max_key, 1, MPI_UNSIGNED_LONG_LONG, MPI_MAX, 
				MPI_COMM_WORLD);

			int32_t r;
			uint64_t mask;
			uint64_t shift;
			for(r=nb_rounds-1; r>=0; r--)
			{
				shift = log2_radix*(uint32_t)r;
				mask = (radix_mask << shift);
				if((max_key&mask))
				{
					max_round = (uint32_t)r;
					break;
				}
			}
		}
		else
		{
			for(i=0; i<nb_keys_per_proc; i++)
				local_bucket_counts[KEY(local_keys_0[i])]++;
		}
		
		/*Exchange bucket distribution*/
		for(i=0; i<nb_procs; i++)
		{
			if(i == rank)
				continue;
			MPI_Put(local_bucket_counts, radix, MPI_UNSIGNED, i, rank*radix,
				 radix, MPI_UNSIGNED, bucket_win);
		}

		/*Overlap prefix sum computation*/
		local_positions[0] = 0;
		for(i=1; i<radix; i++)
		{
			local_prefix_sums[i] = local_prefix_sums[i-1] + local_bucket_counts[i-1];
			local_positions[i] = local_prefix_sums[i];
		}
		assert(local_prefix_sums[0] == 0);

		/*Local sort ...*/
		uint64_t val;
		for(i=0; i<nb_keys_per_proc; i++)
		{
			val = local_keys_0[i];
			local_keys_1[local_positions[KEY(val)]++] = val;
		}

		MPI_Win_fence(MPI_MODE_NOSUCCEED, bucket_win);

#ifndef __JZ_RMA //seems like the vanilla MVAPICH does not have a barier semantic for closing fence?
		MPI_Barrier(MPI_COMM_WORLD);
#endif

		MPI_Win_fence(MPI_MODE_NOPRECEDE, key_win);

		/*Compute global bucket distro and offset*/
		uint32_t j;

		/*Move data to right place*/
		uint32_t amount_to_move;
		for(i=0; i<radix; i++)
		{
			for(j=0; j< nb_procs; j++)
			{
				if(j < rank)
				{
					global_offsets[i] += bucket_count_distro[j*radix + i];
				}
				global_bucket_counts[i] += bucket_count_distro[j*radix + i];
			}
			if(i>0)
				global_prefix_sums[i] = global_prefix_sums[i-1] + global_bucket_counts[i-1]; 

			data_relocation_info[i].rank_0 = data_relocation_info[i].rank_1 = -1;

			amount_to_move = local_bucket_counts[i];
			if(!amount_to_move)
				continue;

			build_data_relocation_info(amount_to_move, i, &data_relocation_info[i]);

			assert(data_relocation_info[i].offset_in_rank_0 + 
				data_relocation_info[i].amount_in_rank_0 <= nb_keys_per_proc);

			{
				MPI_Put(&local_keys_1[local_prefix_sums[i]], data_relocation_info[i].amount_in_rank_0,
						MPI_UNSIGNED_LONG_LONG, data_relocation_info[i].rank_0, 
						data_relocation_info[i].offset_in_rank_0,
						data_relocation_info[i].amount_in_rank_0, MPI_UNSIGNED_LONG_LONG, key_win);
			}

			if(data_relocation_info[i].rank_1 == -1)
				continue;

			assert(data_relocation_info[i].amount_in_rank_1 <= nb_keys_per_proc);
				
			{
				MPI_Put(&local_keys_1[local_prefix_sums[i]+data_relocation_info[i].amount_in_rank_0], 
						data_relocation_info[i].amount_in_rank_1,
						MPI_UNSIGNED_LONG_LONG, data_relocation_info[i].rank_1, 
						data_relocation_info[i].offset_in_rank_1,
						data_relocation_info[i].amount_in_rank_1, MPI_UNSIGNED_LONG_LONG, key_win);
			}
		}

		if(round < nb_rounds)
		{
			memset(local_bucket_counts, 0, sizeof(uint32_t)*radix);
			memset(local_prefix_sums, 0, sizeof(uint32_t)*radix);
			memset(global_bucket_counts, 0, sizeof(uint32_t)*radix);
			memset(global_offsets, 0, sizeof(uint32_t)*radix);
			memset(global_prefix_sums, 0, sizeof(uint32_t)*radix);
		}
		MPI_Win_fence(MPI_MODE_NOSUCCEED, key_win);
	
		trace_to_stream(round);
		if(trace)
		{
			Z_serialized_stream((void*)local_keys_0, nb_keys_per_proc*sizeof(uint64_t),
					(void*)(uintptr_t)(round), MPI_COMM_WORLD, display_func);
		}
#ifndef __JZ_RMA //seems like the vanilla MVAPICH does not have a barier semantic for fence?
		MPI_Barrier(MPI_COMM_WORLD);
#endif
	}
	free(local_positions);
}


#ifdef __JZ_RMA
/*Sort using nonblocking epochs*/
void isort()
{
	MPI_Request requests[10];
	uint32_t nb_rounds = 64/log2_radix;
	if(nb_rounds*log2_radix < 64)
		++nb_rounds;
	uint32_t round;
	uint32_t *local_positions = malloc(sizeof(uint32_t)*radix);

	uint64_t max_key = 0;
	uint32_t max_round = nb_rounds;

	memset(local_bucket_counts, 0, sizeof(uint32_t)*radix);
	memset(local_prefix_sums, 0, sizeof(uint32_t)*radix);
	memset(global_bucket_counts, 0, sizeof(uint32_t)*radix);
	memset(global_offsets, 0, sizeof(uint32_t)*radix);
	memset(global_prefix_sums, 0, sizeof(uint32_t)*radix);
	for(round = 0; round <= max_round; round++)
	{
		MPI_Win_fence(MPI_MODE_NOPRECEDE, bucket_win);

		current_shift = log2_radix*(round);
		current_mask = (radix_mask << current_shift);

		/*Compute local bucket sizes for current round*/
		uint32_t i;

		if(round == 0)
		{
			for(i=0; i<nb_keys_per_proc; i++)
			{
				max_key = Z_MAX(max_key, local_keys_0[i]);
				local_bucket_counts[KEY(local_keys_0[i])]++;
			}
			MPI_Allreduce(MPI_IN_PLACE, &max_key, 1, MPI_UNSIGNED_LONG_LONG, MPI_MAX, 
				MPI_COMM_WORLD);

			int32_t r;
			uint64_t mask;
			uint64_t shift;
			for(r=nb_rounds-1; r>=0; r--)
			{
				shift = log2_radix*(uint32_t)r;
				mask = (radix_mask << shift);
				if((max_key&mask))
				{
					max_round = (uint32_t)r;
					break;
				}
			}
		}
		else
		{
			for(i=0; i<nb_keys_per_proc; i++)
				local_bucket_counts[KEY(local_keys_0[i])]++;
		}

		/*Exchange bucket distribution*/
		
		for(i=0; i<nb_procs; i++)
		{
			if(i == rank)
				continue;
			MPI_Put(local_bucket_counts, radix, MPI_UNSIGNED, i, rank*radix,
				 radix, MPI_UNSIGNED, bucket_win);
		}
		MPI_Win_ifence(MPI_MODE_NOSUCCEED, bucket_win, requests);

		/*Overlap prefix sum computation*/
		local_positions[0] = 0;
		for(i=1; i<radix; i++)
		{
			local_prefix_sums[i] = local_prefix_sums[i-1] + local_bucket_counts[i-1];
			local_positions[i] = local_prefix_sums[i];
		}
		assert(local_prefix_sums[0] == 0);

		/*Local sort ...*/
		uint64_t val;
		for(i=0; i<nb_keys_per_proc; i++)
		{
			val = local_keys_0[i];
			local_keys_1[local_positions[KEY(val)]++] = val;
		}

		MPI_Wait(requests, MPI_STATUS_IGNORE);

		MPI_Win_fence(MPI_MODE_NOPRECEDE, key_win);

		uint32_t j;

		/*Move data to right place*/
		uint32_t amount_to_move;
		for(i=0; i<radix; i++)
		{
			for(j=0; j< nb_procs; j++)
			{
				if(j < rank)
				{
					global_offsets[i] += bucket_count_distro[j*radix + i];
				}
				global_bucket_counts[i] += bucket_count_distro[j*radix + i];
			}
			if(i>0)
				global_prefix_sums[i] = global_prefix_sums[i-1] + global_bucket_counts[i-1]; 

			data_relocation_info[i].rank_0 = data_relocation_info[i].rank_1 = -1;

			amount_to_move = local_bucket_counts[i];
			if(!amount_to_move)
				continue;

			build_data_relocation_info(amount_to_move, i, &data_relocation_info[i]);

			assert(data_relocation_info[i].offset_in_rank_0 + 
				data_relocation_info[i].amount_in_rank_0 <= nb_keys_per_proc);

			if(data_relocation_info[i].rank_0 != rank)
			{
				MPI_Put(&local_keys_1[local_prefix_sums[i]], 
						data_relocation_info[i].amount_in_rank_0,
						MPI_UNSIGNED_LONG_LONG, data_relocation_info[i].rank_0, 
						data_relocation_info[i].offset_in_rank_0,
						data_relocation_info[i].amount_in_rank_0, 
						MPI_UNSIGNED_LONG_LONG, key_win);
			}

			if(data_relocation_info[i].rank_1 == -1)
				continue;

			assert(data_relocation_info[i].amount_in_rank_1 <= nb_keys_per_proc);
				
			if(data_relocation_info[i].rank_1 != rank)
			{
				MPI_Put(&local_keys_1[local_prefix_sums[i]+
						data_relocation_info[i].amount_in_rank_0], 
						data_relocation_info[i].amount_in_rank_1,
						MPI_UNSIGNED_LONG_LONG, data_relocation_info[i].rank_1, 
						data_relocation_info[i].offset_in_rank_1,
						data_relocation_info[i].amount_in_rank_1, 
						MPI_UNSIGNED_LONG_LONG, key_win);
			}
		}
		MPI_Win_ifence(MPI_MODE_NOSUCCEED, key_win, requests);

		for(i=0; i<radix; i++)
		{
			if(data_relocation_info[i].rank_0 == rank)
			{
				memcpy(&local_keys_0[data_relocation_info[i].offset_in_rank_0],
					&local_keys_1[local_prefix_sums[i]], 
					data_relocation_info[i].amount_in_rank_0*sizeof(uint64_t));
			}
			if(data_relocation_info[i].rank_1 == rank)
			{
				memcpy(&local_keys_0[data_relocation_info[i].offset_in_rank_1],
					&local_keys_1[local_prefix_sums[i]+data_relocation_info[i].amount_in_rank_0], 
					data_relocation_info[i].amount_in_rank_1*sizeof(uint64_t));
			}
		}

		if(round < nb_rounds)
		{
			memset(local_bucket_counts, 0, sizeof(uint32_t)*radix);
			memset(local_prefix_sums, 0, sizeof(uint32_t)*radix);
			memset(global_bucket_counts, 0, sizeof(uint32_t)*radix);
			memset(global_offsets, 0, sizeof(uint32_t)*radix);
			memset(global_prefix_sums, 0, sizeof(uint32_t)*radix);
		}

		MPI_Wait(requests, MPI_STATUS_IGNORE);

		trace_to_stream(round);
		if(trace)
		{
			Z_serialized_stream((void*)local_keys_0, nb_keys_per_proc*sizeof(uint64_t),
					(void*)(uintptr_t)(round), MPI_COMM_WORLD, display_func);
		}
	}
	free(local_positions);
}
#endif //__JZ_RMA

void print_sorted_keys()
{
	Z_serialized_stream((void*)local_keys_0, nb_keys_per_proc*sizeof(uint64_t),
			(void*)DB_DECIMAL, MPI_COMM_WORLD, display_func);
}

void print_sorted_key_of_rank(int _rank)
{
	if(_rank != rank)
		return;
	Z_PRINT("Printing sorted keys of %d\n", rank);
	uint32_t i;
	for(i=0; i<nb_keys_per_proc; i++)
	{
		Z_PRINT("%lu\n", local_keys_0[i]);
	}
}

int main(int argc, char** argv)
{
	double avg_sort_duration=0.0;

	double d_sum = 0.0, d_sum_square = 0.0; /*durations. These are used to compute
											  online standard deviations*/

	double validation_duration=0.0, avg_validation_duration=0.0;
	init(&argc, &argv);

	cmd_parse_status_t cps = parse_cmd_line(argc, argv);
	if(cps != CPS_CONTINUE)
	{
		print_usage(argv[0]);
		goto _FINALIZE;
	}
	if(!check_parameter_sanity())
	{
		print_usage(argv[0]);
		goto _FINALIZE;
	}
	if(simulated_job_size > 0)
	{
		simulate_data_load();
		Z_PRINT_IF(rank == 0, "Simulated app data =%0.6fGB\n", 1.0*data_load/GB);
		goto _FINALIZE;
	}
	init_stream();
	compute_data_load();

	if(app_data_cap == DEFAULT_APP_DATA_CAP)
		Z_PRINT_IF(rank == 0, "App data per process = %0.6lfGB. Cap = INFINITE\n", 
				1.0*data_load/GB);
	else if(data_load > app_data_cap)
	{
		Z_PRINT_IF(rank == 0, 
				"App data per process (%0.6lfGB) exceeded cap (%0.6lfGB). Job will exit\n", 
				1.0*data_load/GB, 1.0*app_data_cap/GB);
		goto _PREMATURE_EXIT;
	}
	else
		Z_PRINT_IF(rank == 0, 
			"App data = %0.6lfGB. Cap = %0.6lf GB\n", 1.0*data_load/GB, 1.0*app_data_cap/GB);

	prepare();
	int i;
	for(i=0; i<nb_iters+nb_omit_iters; i++)
	{
		MPI_Barrier(MPI_COMM_WORLD);
		if(!generate())
			goto _EXIT;
		reset();

		Z_STAMP_START_TIME();
#ifdef __JZ_RMA
		if(nonblocking)
			isort();
		else
#endif
			sort();
		Z_STAMP_END_TIME();

		if(i >= nb_omit_iters)
		{
			double d = Z_GET_DURATION_US()/MILLION;
			d_sum += d;
			d_sum_square += (d*d);

			Z_PRINT_IF(rank == 0, "\nDone sorting iter %d!\n", i - nb_omit_iters);
		}

		if(validate && i >= nb_omit_iters)
		{
			MPI_Barrier(MPI_COMM_WORLD);
			Z_PRINT_IF(rank == 0, "Starting validation iter %d...\n", i - nb_omit_iters);
			Z_STAMP_START_TIME();
			perform_validation();
			Z_STAMP_END_TIME();
			validation_duration += Z_GET_DURATION_US();
		}
		if(print)
			print_sorted_keys();
	}

	/*No need to reduce the timing metrics; the algo is fence-based.*/

	Z_PRINT_IF(rank==0, "\n");
	if(rank == 0)
	{
		double std = nb_iters < 2 ? 0.0 :
			sqrt((d_sum_square - (d_sum*d_sum)/nb_iters)/(nb_iters-1));
		avg_sort_duration = d_sum / nb_iters;
		if(verbose_result_display)
		{
			Z_PRINT_IF(rank == 0, 
					"nb_keys = %lu (2^%u), duration = %.3lf us, duration = %.3lf s\n", 
					nb_keys, log2_nb_keys, avg_sort_duration*MILLION, avg_sort_duration);
		}
		else
			Z_PRINT("%0.4lf\t%0.4lf\n", avg_sort_duration, std);
	}
	if(validate)
	{
		/*No need to reduce here as well because process 0 is always the last to
		 * finish the validation procedure*/

		avg_validation_duration = validation_duration / nb_iters;
		if(rank == 0)
		{
			if(verbose_result_display)
			{
				Z_PRINT_IF(rank == 0, 
						"validation = %.4lf us, validation = %.4lf s\n", 
						avg_validation_duration, avg_validation_duration/MILLION);
			}
			else
				Z_PRINT("%0.4lf\n", avg_validation_duration/MILLION);
		}
	}

_EXIT:
	release();
_PREMATURE_EXIT:
	release_stream();
_FINALIZE:
	MPI_Finalize();
	return 0; /*Returning 0 in any case because of te dreaded bad termination 
				message of MPI lunchers. That message is displayed indiscrimitely
				for a bugs or for planned early termination upon bad command options*/
}

