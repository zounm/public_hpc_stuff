/*Author: Judicael Zounmevo (zounm@linux.com)
 * */

#include <random>
#include <cmath>
#include <string>
#include "distro_data_gen.h"

void generate_uniform(uint64_t* distribution, uint64_t count, uint64_t min, 
		uint64_t max)
{
	std::random_device rd;
	std::mt19937_64 gen(rd()); //Mersenne twister engine for 64-bit integers
	std::uniform_int_distribution<uint64_t> distro(min, max);
	for(uint64_t i=0; i<count; i++)
		distribution[i] = distro(gen);
}

void generate_gaussian(uint64_t* distribution, uint64_t count, double mean,
		double std_dev)
{
	std::random_device rd;
	std::mt19937_64 gen(rd()); 
	std::normal_distribution<> distro(mean, std_dev);
	for(uint64_t i=0; i<count; i++)
		distribution[i] = std::lround(distro(gen));
}
