# Synopsis
A few tools and work that I built or did over the years for distributed 
computing and HPC-related research, development, diagnoses or benchmarking. 
I shared various bits of them with 
colleagues and labmates for ad-hoc needs at various occasions; but I should
have just sanitized the whole thing ages ago and make it public somewhere; 
... doing that now. 

Sanitization requires a bit of time; so the addition of new content over the
weeks might be kind of slow.


# Copyright
Everything here is free for research, learning or personal use. Please give
credits when appropriate if anything is used for work that is also made
public.

# Content

## utils 

* Diagnosis tools
* Some other "goodies"

Please see the README in utils/ for the doc of what is offered.

## benchmarks

* MPI one-sided distributed radix-sort
	* Please read the README in benchmarks/radix\_one\_sided

## HPC middleware
New concepts, novel stuff, "glorious" and "trivial" "hacks" for MPI and MPI middlewares.

* entirely\_nonblocking\_rma\_synchronizations
	*  Please read the README in hpc\_middleware/entirely\_nonblocking\_rma\_synchronizations.
